//var root = location.protocol + '//' + location.host;
//var base_url = root + '/scm/';
$(document).ready(function () {

    $('#add_module_form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            short_name: {
                validators: {
                    stringLength: {
                        min: 2,
                        message: 'Minimum 2 character required'
                    }
                }
            },
            order_no: {
                validators: {
                    integer: {
                        message: 'The value is not an integer'
                    }
                }
            },
            remark: {
                validators: {
                    stringLength: {
                        min: 5,
                        message: 'Minimum 5 character required'
                    }
                }
            },
            status: {
                validators: {
                    notEmpty: {
                        message: 'Status is required'
                    },
                    integer: {
                        message: 'The value is not an integer'
                    }
                }
            },
        }
    });

});



