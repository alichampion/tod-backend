var TableDatatablesManaged = function () {
    

    var initCaptainTable = function () {

        var table = $('#captain-table');
        table.dataTable({
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            "bPaginate": true,
            "bInfo": true,
            "processing": true,
            "serverSide": true,
             ajax: BASE_URL+'/admin/get-captains-ajax',
             "columns": [
             {"data": "id"},
             {"data": "fname"},
             {"data": "email"},
             {"data": "phone"},
             {"data": "status"},
             {"data": "id"},
             {"data": "id"},

             ],

            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
            ],

            "columnDefs": [
                {  // set default column settings
                    'orderable': false,
                    'targets': [0]
                }, 
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "className": "dt-right", 
                    //"targets": [2]
                }
            ],
            "order": [
                [1, "asc"]
            ] // set first column as a default sort by asc
            
        });


       



    }

 


   return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initCaptainTable();
        }

    };

}();

if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        TableDatatablesManaged.init();
    });
}