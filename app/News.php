<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = ['news_image' , 'news_content'];
 	protected $hidden = [ 'status' ];
}