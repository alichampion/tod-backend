<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
     protected $fillable = ['images' , 'phone' , 'email' , 'address' , 'description'];
 	protected $hidden = [ 'status' ];
}
