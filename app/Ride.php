<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ride extends Model
{
    protected $fillable = ['user_id','captain_id' , 'booking_id' ,'loading_waiting_time' , 'loading_image' ,'loading_time' ,'rejected_by', 'status' ,'arrival_time' , 'ride_start_time' , 'ride_end_time' , 'total_ride_time'];

    protected $hidden = ['complete_at' ];
    

    public function user()
    {
        return $this->belongsTo(User::class ,'user_id');
    }

    public function advancebookings()
    {
        return $this->belongsTo(AdvanceBooking::class);
    }

    public function booking(){

    	return $this->belongsTo(Booking::class);
    }

    public function good(){

        return $this->belongsTo(Booking::class , 'good_id');
    }

    public function BookingDestination(){

        return $this->belongsTo(Booking::class);
    }

    public function captain(){

    	return $this->belongsTo(Captain::class);
    }

    public function vehical(){

        return $this->belongsTo(Vehical::class , 'captain_id' );
    }

    public function vehicals(){

        return $this->belongsTo(Booking::class , 'vehical_id' );
    }

    public function RideCompleted(){
        
        return $this->hasMany(RidesCompleted::class ,'ride_id');
    }

     public function RideCompleteds(){
        
        return $this->belongsTo(RidesCompleted::class ,'ride_id');
    }
    public function captain_rate(){
        return $this->hasOne(Rate::class);
    }
    public function user_rate(){
        return $this->hasOne(UserRate::class);
    }
    public function transaction(){
        return $this->hasOne(Transaction::class);
    }
    
}

