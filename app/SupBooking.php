<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupBooking extends Model
{
     protected $fillable = [
    	 'pickup_longitude', 'pickup_latitude','good_id','vehical_sub_id','user_id','amount','distance','booking_date','status','pickup_place', 'PaymentMethod','labour_loader','labour_unloader' ,'estimated_time' , 'rejected_by' ,'toll_plaza' , 'booking_type' , 'old_booking_id' , 'captain_id' , 'emergencies_reasons', 'vehical_cat_id' , 'serge' ,'serge_charge' ,'loading_charge' , 'unloading_charge'
    ];

    public function BookingDestination(){
         
        return $this->hasMany(SupBookingDestination::class ,'sup_booking_id');
    }
}
