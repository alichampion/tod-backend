<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Help extends Model
{
  protected $fillable = ['complaint_type','complaint_description','additional_detail','captain_refuse_desc' ,'destination_desc','behavior_detail','vehical_detail','license_detail' ,'person_detail','passanger_detail','profile_detail','mention_captain' ,'addition_amount','parking_location','parking_amount','toll_charge' ,'date_accident','time_accident' ,'location_accident'];
}
