<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class booking extends Model
{
    protected $fillable = [
    	 'pickup_longitude', 'pickup_latitude','good_id','vehical_sub_id','user_id','amount','distance','booking_date','status','pickup_place', 'PaymentMethod','labour_loader','labour_unloader' ,'estimated_time' , 'rejected_by' ,'toll_plaza' , 'booking_type' , 'old_booking_id' , 'emergencies_reasons' , 'captain_id' ,'vehical_cat_id','total_captain' , 'serge' ,'serge_charge' ,'loading_charge' , 'unloading_charge' , 'distance_charge','reject_booking_image'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function vehicleSubCategorie()
    {
        return $this->belongsTo(VehicleSubCategorie::class, 'vehical_sub_id');
    }

    public function vehicle_Categorie()
    {
        return $this->belongsTo(VehicalCategorie::class, 'vehical_cat_id');
    }

    public function good(){

    	return $this->belongsTo(Good::class);
    }

    public function Rides()
    {
        return $this->hasMany(Ride::class ,'booking_id');
    }

    public function Ridescomplete()
    {
        return $this->belongsTo(RidesCompleted::class);
    }

    public function captain()
    {
        return $this->belongsTo(Ride::class);
    }

    public function BookingDestination(){
         
        return $this->hasMany(BookingDestination::class ,'booking_id');
    }

    

    
}
