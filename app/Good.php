<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Good extends Model
{

   protected $fillable = ['good_name' , 'status'];
   
   public function bookings(){

       return $this->hasMany(Booking::class);
   }
   public function AdvanceBookings(){

       return $this->hasMany(AdvanceBooking::class);
   }

   public function ride(){

    return $this->belongsTo(Booking::class ,'good_id');
    }
}
