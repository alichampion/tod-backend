<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Route;
class ClearanceMiddleware
{
    
    
    public function handle($request, Closure $next)
    {
       
      if (Auth::guard('admin')->user()->hasRole('Super Admin')) //If user has this //permission
      {
        return $next($request);
      }
      //echo $request->url();exit;

      $routes=Route::with('permission')->get();
      foreach ($routes as $route) {
        if(strtolower($route->method)=='delete'){
        if ($request->isMethod('Delete')) {
            if (!Auth::user()->hasPermissionTo($route->permission->name)) {
                abort('401');
            } else {
                return $next($request);
            }
        }

        }else{

        if ($request->is($route->url)) {
            if (!Auth::user()->hasPermissionTo($route->permission->name)) {
                echo $route->url;
                abort('401');
            } else {
                return $next($request);
            }
        }

        }

      }
     
       


        return $next($request);
    }
}
