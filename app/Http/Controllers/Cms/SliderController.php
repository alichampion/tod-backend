<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Slider;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;

class SliderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin']);
    }

   
    public function create()
    {
        return view('admin.Cms.slider.slider_created');
    }

    
    public function store(Request $request)    
    { 
       $validator = Validator::make($request->all(),
    [
     'slide_image' => 'required', 

   ]);

       if($validator->fails())
       {
            $message = $validator->errors()->first();
           Session::flash('info', " $message") ;
          return Redirect::back()->withErrors($message);
       } 
       else {
       $obj = new Slider;
 
        $slide_image = $request->file('slide_image')->getClientOriginalName();

        if($request->hasfile('slide_image')){
          $Extension_profile = $request->file('slide_image')->getClientOriginalExtension();
          $slide_image = 'slide_image'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('slide_image')->move('slider/image/', $slide_image);
      }
         
        $obj->slide_image = $slide_image;
        $obj->slide_content =$request->slide_content;
        $record_affected = $obj->save();

        if($record_affected)
        session()->flash('success', 'Record has been updated. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');

         return redirect()->route('logo.index');
        
    }
}
  
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $slide = Slider::find($id);
        
        return view('admin.Cms.slider.slider_edit', compact('slide'));
    }

    
    public function update(Request $request, $id)
    {
       $data = Slider::findOrFail($id);
        $input = $request->all();
     if ($Get_Doc = $request->file('slide_image'))
      {
          $Get_Docs = $Get_Doc->getClientOriginalName();                        
          $Extension_profile = $request->file('slide_image')->getClientOriginalExtension();
           $Get_Doc = 'slide_image'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('slide_image')->move('slider/image/', $Get_Doc);  
          $input['slide_image'] =$Get_Doc;                  
      }   
       
       $record_affected = $data->update($input); 
        if($record_affected)
        session()->flash('success', 'Record has been updated. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');
        return redirect()->route('logo.index');

        
    }
    
    public function destroy($id)
    {
         $captains = Slider::find($id);
         $record_affected = $captains->delete();

        if($record_affected)
        session()->flash('success', 'Record has been updated. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');

         return redirect()->route('logo.index');
    }
}
