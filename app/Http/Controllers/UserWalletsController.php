<?php

namespace App\Http\Controllers;

use App\UserWallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;

class UserWalletsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin']);
    }
    
    
    public function index()
    {
        
    }

   
    public function store(Request $request)
    {
        //
    }

    
    public function show(UserWallets $userWallets)
    {
        $wallet = UserWallet::findOrFail($id); 

        return view('admin.user.userswallets' , compact('wallet'));
    }

    
    public function edit(UserWallets $userWallets)
    {
        //
    }

    
    public function update(Request $request, UserWallets $userWallets)
    {
        //
    }

    
    public function destroy(UserWallets $userWallets)
    {
        //
    }
}
