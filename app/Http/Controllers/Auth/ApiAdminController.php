<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Admin;
use DB;
use Auth;


class ApiAdminController extends Controller
{
    function __construct()
  {
    $this->middleware("guest:adminapi", ['except' => ['logout','verify']]);
  }

 /* public function register(Request $request)
  {
    // validation the form data;
    $validator = Validator::make($request->all(), [
      'phone_code' => 'required',
      'phone' => 'required|unique:users',
      'password' => 'required|min:6|confirmed',
      'password_confirmation' => 'required|min:6'
    ]);

    if ($validator->fails())
    {
      $status = false;
      $message = $validator->errors()->first();
      //$messages = $validator->messages();
      return response()->json(['status'=>$status,'message'=>$message], 200);

    }else{

     $newUser=Admin::create([
        'name' => $request->input('name'),
        'phone' => $request->input('phone'),
        'email' => $request->input('email'),
        'api_token'=> str_random(60),
        'verified_code'=> rand(100000,999999),
        'password' => bcrypt($request->input('password')),
      ]);

     if($newUser->save()){

          $status = true;

          $message = "Admin Account has been created successfully.";
          return response()->json(['status'=>$status,'message'=>$message,'user'=>$newUser], 200);
     }
     else{
          $status = false;
          $message = "Phone number is not valid";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }


     }

}*/
 public function login(Request $request){
      // validation the form data;
      $validator = Validator::make($request->all(), [
        'phone' => 'required',
        'password' => 'required|min:6'
        ]);
        $status = false;

      if ($validator->fails())
      {
        $message = $validator->errors()->first();
        return response()->json(['status'=>$status,'message'=>$message], 400);

      }

      //$credentials =$request->only('phone','password');
      $status = true;
      $message = 'Admin has been successfully login.';
      $user=Admin::where(['phone' => $request->phone,'password'=>Hash::make($request->password)])->first();
      if(isset($user) && !empty($user)){
        return response()->json(['status'=>$status,'message'=>$message,'user'=>$user], 200);

      }else{

        $message = 'Fail to login.';
        return response()->json(['status'=>$status,'message'=>$message], 400);
      }


    }

     public function logout()
   {

   }

}