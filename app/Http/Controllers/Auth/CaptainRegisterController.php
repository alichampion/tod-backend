<?php

namespace App\Http\Controllers\Auth;

use App\Captain;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;


class CaptainRegisterController extends Controller
{
    
    protected $redirectTo = '/home';

 
    public function __construct()
    {
        $this->middleware('guest');
    }

   
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'max:255',
            'phone' => 'max:30|unique:users',
            'email' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    
     protected function create(array $data)
    {
        return Captain::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone_code' => $data['phone_code'],
            'phone' => $data['phone'],
            'api_token'=> str_random(60),
            'password' => Hash::make($data['password']),
        ]);
    }
}
