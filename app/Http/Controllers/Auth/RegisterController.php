<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'max:255',
            'Lname' => 'required',
            'phone' => 'max:30|unique:users',
            'gender' => 'required',
            'email' => 'required|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'phone_code' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // $new_store = $data->all();
        //     $path_logo = './images'; // upload path
        //     if (isset($new_store['profile'])) {
        //         $extension_logo = Input::file('profile')->getClientOriginalExtension(); // getting image extension
        //         $logo = date('YmdHis') . '_' . $new_store['profile'] . '_logo' . '.' . $extension_logo; // renameing image
        //         Input::file('profile')->move($path_logo, $logo); // uploading file to given path
        //         $new_store['profile'] = url('/') . '/' . $path_logo . $logo;
        //         $new_store['profile'] = str_replace('162.243.43.52', 'webloader.com', $new_store['profile']);
        //     }
        // return User::create([
        //     'name' => $data['name'],
        //     'Lname' => $data['Lname'],
        //     'email' => $data['email'],
        //     'phone_code' => $data['phone_code'],
        //     'phone' => $data['phone'],
        //     'gender' => $data['gender'],
        //     'api_token'=> str_random(60),
        //     'password' => Hash::make($data['password']),
        //     'profile' => $data['profile'],
        // ]);
    }
}
