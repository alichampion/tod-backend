<?php


namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Auth;
use Validator;
use App\Captain;
use DB;



class ApiCaptainController extends Controller
{
    function __construct()
    {
        $this->middleware("guest:captainapi", ['except' => ['logout', 'verify']]);
    }

    public function register(Request $request)
    {
        // validation the form data;
        $validator = Validator::make($request->all(), [
            'phone' => 'required|unique:Captains',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        ]);

        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            //$messages = $validator->messages();
            return response()->json(['status' => $status, 'message' => $message], 200);

        } else {

            $newUser = Captain::create([
                'fname' => $request->input('fname'),
                'lname' => $request->input('lname'),
                'phone' => $request->input('phone'),
                'email' => $request->input('email'),
                'api_token' => str_random(60),
                'verified_code' => rand(100000, 999999),
                'password' => bcrypt($request->input('password')),
            ]);

            if ($newUser->save()) {

                $status = true;

                $message = "Captain Account has been created successfully.";
                return response()->json(['status' => $status, 'message' => $message, 'captain' => $newUser], 200);
            } else {
                $status = false;
                $message = "Phone number is not valid";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }


        }

    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'status' => 'required',
            'password' => 'required|min:6'
        ]);
        $status = false;

        if ($validator->fails()) {
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $user = Captain::where(['phone' => '+' . $request->phone])->with('CaptainStats')->first();
            // dd($user->toArray());
            if (isset($user) && !empty($user)) {
                if (Hash::check($request->password, $user->password)) {
                    $obj = array(
                        'fname' => isset($user->fname) ? $user->fname : '',
                        'lname' => isset($user->lname) ? $user->lname : '',
                        'phone' => isset($user->phone) ? $user->phone : '',
                        'longitude' => isset($user->longitude) ? $user->longitude : '',
                        'latitude' => isset($user->latitude) ? $user->latitude : '',
                        'images' => isset($user->images) ? $user->images : '',
                        'vehicle_no' => isset($user->vehicle_no) ? $user->vehicle_no : '',
                        'api_token' => isset($user->api_token) ? $user->api_token : '',
                        'rating' => $user->CaptainStats != null ? $user->CaptainStats->rating / $user->CaptainStats->users : 0,
                    );
                    $status = true;
                    $message = 'Captain has been successfully login.';
                    $captain = Captain::where(['phone' => '+' . $request->phone])->update(array('device_token' => isset($request->device_token) ? $request->device_token : '', 'status' => $request->status, 'last_loc_update' => date('Y-m-d h:i:s')));
                    return response()->json(['status' => $status, 'message' => $message, 'user' => $user, 'data' => $obj], 200);
                } else {
                    $status = false;
                    $message = 'Captain has enter invalid password.';
                    return response()->json(['status' => $status, 'message' => $message], 200);
                }
            } else {
                $message = 'Enter Invalid Phone No.';
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }

    public function logout()
    {
        $captain = Captain::where('id', Auth::guard('captainapi')->id())->update(array('status' => 0));

        if ($captain) {
            $status = true;
            $message = "'captain has been successfully logout.";
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $status = false;
            $message = "no record found";
            return response()->json(['status' => $status, 'message' => $message], 200);
        }
    }
}