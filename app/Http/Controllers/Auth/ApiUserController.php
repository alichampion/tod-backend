<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
//use App\Validator;
use Illuminate\Support\Facades\Validator;
use App\User;
use DB;
use Auth;


class ApiUserController extends Controller
{
    function __construct()
  {
    $this->middleware("guest:api", ['except' => ['logout','verify']]);
  }

  public function register(Request $request)
  {
    // validation the form data;
    $validator = Validator::make($request->all(),
     [
      'fname' => 'required',
      'lname' => 'required',
      'phone' => 'required|unique:users',
      'password' => 'required|min:6',
      'email' => 'unique:users'
    ]);

    if ($validator->fails())
    {
      $status = false;
      $message = $validator->errors()->first();
      return response()->json(['status'=>$status,'message'=>$message], 200);

    }else{

     $newUser=User::create([

        'fname' => $request->input('fname'),
        'lname' => $request->input('lname'),
        'email' => $request->input('email'),
        'phone' => $request->input('phone'),
        'api_token'=> str_random(60),
        'verified_code'=> rand(100000,999999),
        'password' => Hash::make($request->input('password')),
      ]);

     if($newUser->save()){

          $status = true;
          $message = "User Account has been created successfully.";
          return response()->json(['status'=>$status,'message'=>$message,'user'=>$newUser], 200);
     }
     else{
          $status = false;
          $message = "email number is not valid";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }


     }

  }

   public function register_validate(Request $request)
  {
    $validator = Validator::make($request->all(),
     [
      'fname' => 'required',
      'email' => 'required|email|unique:users',
      'phone' => 'required|unique:users',
      
    ]);
    if ($validator->fails())
    {
      $status = false;
      $message = $validator->errors()->first();
      return response()->json(['status'=>$status,'message'=>$message], 200);

    }else{
          $status = true;
          $message = "User Record not found";
          return response()->json(['status'=>$status,'message'=>$message], 200);
     }
  }
  
   public function login(Request $request){
      $validator = Validator::make($request->all(), [
        'password' => 'required|min:6'
        ]);    
      if ($validator->fails())
      {
        $status = false;
        $message = $validator->errors()->first();
        return response()->json(['status'=>$status,'message'=>$message], 200);
      }else{
      if(isset($request->email) && !empty($request->email)){
        $user = User::where(['email' => $request->email])->first();
      }else if(isset($request->phone) && !empty($request->phone)){
        $user=User::where(['phone' => $request->phone])->first();
      }else{
       $status = false;
       $message = "Email/Phone is required.";
       return response()->json(['status'=>$status,'message'=>$message], 200);
      }
      if(isset($user) && !empty($user)){
        if(Hash::check($request->password, $user->password)){
          if(!empty($request->device_token)){
            $fat['device_token']=$request->device_token;
          }
           $fat['status']=1;
          $captain=User::where('id', $user->id)->update($fat);
          $status = true;
          $message = 'user has been successfully login.';
            return response()->json(['status'=>$status,'message'=>$message,'user'=>$user], 200);
        }else{
        $status = false;
        $message = 'user has enter invalid password.';
          return response()->json(['status'=>$status,'message'=>$message], 200);
          }
      }else{
        $status = false;
        $message = 'Enter invalid Phone/Email.';
        return response()->json(['status'=>$status,'message'=>$message], 200);
      }
    }
  }

    public function logout()
   {
      $user = User::where('id', Auth::guard('api')->id())->update(array('timeout'=> now()));

     if($user){
          $status = true;
          $message = "'user has been successfully logout.";
          return response()->json(['status'=>$status,'message'=>$message], 200);
     }
     else{
          $status = false;
          $message = "no record found";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }
    }


    public function forgotton(Request $request){
      $validator = Validator::make($request->all(), [
        'email' => 'required_without:phone|min:6' ,
        'phone' => 'required_without:email|min:6',
        ]);    
      if ($validator->fails())
      {
        $status = false;
        $message = $validator->errors()->first();
        return response()->json(['status'=>$status,'message'=>$message], 200);
      }
      else
      {
        if(isset($request->email) && !empty($request->email))
        {
          $user = User::select('api_token')->where(['email' => $request->email])->first();
        }
        else if(isset($request->phone) && !empty($request->phone))
        {
          $user=User::select('api_token')->where(['phone' => $request->phone])->first();
        }
        else{
          $status = false;
          $message = "Email/Phone is not matched.";
          return response()->json(['status'=>$status,'message'=>$message], 200);
        }
        if(isset($user) && !empty($user))
        {
          $status = true;
          $message = 'Enter new password.';
          return response()->json(['status'=>$status,'message'=>$message, 'user'=>$user], 200);
        }
      
      }
    }

    public function set_user_password(Request $request)
    {
      $validator = Validator::make($request->all(),
      [
        'email' => 'required_without:phone|min:6', 
        'phone' => 'required_without:email|min:6',
        'password'=>'required'
      ]);
    if($validator->fails())
      {
        $status = false;
        $error = $validator->errors()->first();
        return response()->json(['status'=>$status, 'message'=>$error], 200);
      } else
      {
      $pass = $request->password;
      $hash = Hash::make($pass);
      $update = User::where('phone', $request->phone)->update(['password' => $hash]);
      if($update)
      {
        $status = true;
        $message = 'Password Update Success';
        return response()->json(['message'=>$message, 'status'=>$status], 200);
      } else 
      {
        $status = false;
        $message = 'Error on updation try again';
        return response()->json(['message'=>$message, 'status'=>$status], 200);
      }
    }
  }
      
}
