<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Session;
use App\file;
use App\Captain;
use App\News;
use App\Trad;
use App\Drive;
use App\Slider;
use App\Companies;
use App\User;
use Auth;

class PublicHomeController extends Controller
{
  public function index()
  {
        $captaians =Captain::orderBy('id', 'DESC')->limit(4)->get();
        $new = News::orderBy('id', 'DESC')->first();
        $buss = Trad::orderBy('id', 'DESC')->first();
       $drive = Drive::orderBy('id', 'DESC')->first();
        $slidd = Slider::orderBy('id', 'DESC')->first();
        $compan = Companies::orderBy('id', 'DESC')->first();
        $captai= collect($captaians);

    return view('public.home' , compact('drive' , 'captaians' , 'buss', 'slidd' , 'new', 'compan'));
  }  
    public function ride()
    {
        return view('public.ride');
    }

    public function signin()
    {
        return view('public.publicsign');
    }

    public function store(Request $request)
    { 
        $validator = Validator::make($request->all(),
    [
     'fname' => 'required',
     'lname' => 'required',
     'email' => 'required|email|unique:users',
     'phone' => 'required',
     'password' => 'required|min:6',   
   ]);
       if($validator->fails())
       {      
        Session::flash('error', 'some fields missing') ;
          return redirect()->back();
       }
   else
   		{
       $obj = new User;
       
      $obj->fname = $request->fname;
      $obj->lname = $request->lname;
      $obj->phone = $request->phone;
      $obj->email = $request->email;
      $obj->password =  Hash::make($request->password);
      $obj->api_token = str_random(60);
      $obj->save();

       Session::flash('success', 'User Register successfuly') ;
        return redirect()->back() ;
   		}
	}

  public function login(Request $request)
    {


        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required|min:6'
            ]);
        // Attempt to login the user in
        if(Auth::guard('web')->attempt(['email' => $request->email, 'password'=>$request->password], 
            $request->remember)){
        
            return redirect('/');
        }
  
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }

   public function logout()
   {
    Auth::guard('web')->logout();
    return redirect('/');
   }

}
