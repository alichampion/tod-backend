<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Financial;
use Session;

class FinancialController extends Controller
{
     public function __construct()
    {
        $this->middleware(['auth:admin']);
    }

    public function index()
    {
        $Financial = Financial::orderBy('id', 'DESC')->get();


       return view('admin.financial', compact('Financial' ));
    }

   

    public function edit($id)
    {
         $Datatypes = Financial::find($id);
        return view('admin.financial', compact('Datatypes'));
        
    }

    public function store(Request $request)
    {
         $validator = Validator::make($request->all(),
        [
        'commisions' => 'required',
        'Serg_fee' => 'required', 
        'reject_booking_amount' => 'required',
        'labour_fare' => 'required', 
        ]);

       if($validator->fails())
       {
            $message = $validator->errors()->first();
            Session::flash('info', " $message") ;
            return Redirect::back()->withErrors($validator);
       } 
       else
       {
        $Datatypes = Financial::findOrFail($request['id']);
        $input = $request->all();
        $good_affected = $Datatypes->update($input);
        if($good_affected)
        session()->flash('success', 'Record has been updated. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');

        return redirect()->route('financial.index');
        
            }
    }
    public function update(Request $request, $id)
    {
        $Datatypes = Financial::findOrFail($id);
        $input = $request->all();
        $finance_affected = $Datatypes->update($input);
        if($finance_affected)
        session()->flash('success', 'Record has been updated. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');
        return redirect()->route('financial.index');
    }
    public function destroy($id)
    {
       	$datatyp = Financial::find($id);
        $good_affected = $datatyp->delete();
        if($good_affected){
        $response['status'] = true;
        $response['message'] = "Record has been delete Successfully.";
        }else{
        $response['status'] = false; 
        $response['message'] = "Fail to delete Record.";
        }
        return response()->json($response);
    }
}
