<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\VehicleSubCategorie;
use App\VehicalCategorie;
use Session;
class VehicleSubCategoriesController extends Controller
{
   public function __construct()
    {
        $this->middleware(['auth:admin','clearance']);
    }
    
    public function index()
    {
         //$subcategorie = VehicleSubCategorie::orderBy('id', 'DESC')->paginate(5);
        
        return view('admin.vehicaltype', compact(['vehicals' ,'subcategorie']));
        
    }

    
    public function create()
    {
    	$scategorie = VehicalCategorie::orderBy('id', 'DESC')->get();
        return view('admin.vSubCategoriesCreate' , compact('scategorie'));
    }

    
    public function store(Request $request)
    {
    	 $validator = Validator::make($request->all(),
    	[
       	'name' => 'required',
     	'vehical_categorie_id' => 'required',
        'vehicle_image' => 'required',
   		
   		]);

       if($validator->fails())
       {
        $message = $validator->errors()->first();
          Session::flash('info', " $message");
           return Redirect::back()->withErrors($validator);
       } 
       else
       {
        if($request->hasfile('vehicle_image')){
          $vehicle_image = $request->file('vehicle_image')->getClientOriginalName();
          $Extension_profile = $request->file('vehicle_image')->getClientOriginalExtension();
          $vehicle_image = 'vehicle_sub_categories'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('vehicle_image')->move('images/vehicle_sub_categories/', $vehicle_image);
        }
        
        $vehical_added = VehicleSubCategorie::create([
            'name' => $request->name,
            'vehical_categorie_id' => $request->vehical_categorie_id,
            'vehicle_image' => $vehicle_image,
            ]);

        if($vehical_added)
        session()->flash('success', 'Record has been added. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');
        return redirect()->route('vehicle-sub-categories.index');
        }
    }
    public function show(datatype $datatype)
    {
        
    }

    
    public function edit($id)
    {
    	$scategorie = VehicalCategorie::orderBy('id', 'DESC')->get();
         $Datatypes = VehicleSubCategorie::find($id);
  
        return view('admin.SCategoriesEdit', compact('Datatypes' ,'scategorie'));
        
    }

    
    public function update(Request $request, $id)
    {

        $Datatypes = VehicleSubCategorie::findOrFail($id);

        $input = $request->all();

           if($request->hasfile('vehicle_image')){
          $profiles = $request->file('vehicle_image')->getClientOriginalName();
          $Extension_profile = $request->file('vehicle_image')->getClientOriginalExtension();
          $profile = 'vehicle_sub_categories'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('vehicle_image')->move('images/vehicle_sub_categories/', $profile);
          $input['vehicle_image'] =$profile;
          }

        $vehical_updated = $Datatypes->update($input);

         if($vehical_updated)
        session()->flash('success', 'Record has been updated. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');

        return redirect()->route('vehicle-sub-categories.index');
    }

   
    public function delete_record($id)
    {
       	$datatyp = VehicleSubCategorie::find($id);
        $vehical_destory = $datatyp->delete();
        if($vehical_destory){
        $response['status'] = true;
        session()->flash('success_message', 'Record has been Delete. successfully.');
        $response['message'] = "Record has been delete Successfully.";
        }else{
        $response['status'] = false; 
        $response['message'] = "Fail to delete Record.";
        }
        return redirect()->route('vehicle-sub-categories.index');

    }

      public function get_sub_vehical(Request $request)
    {
    $datatable = ! empty( $request->datatable ) ? $request->datatable : array();
        $datatable = array_merge( array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable );

        $sort  = ! empty( $datatable[ 'sort' ][ 'sort' ] ) ? $datatable[ 'sort' ][ 'sort' ] : 'desc';
        $field = ! empty( $datatable[ 'sort' ][ 'field' ] ) ? $datatable[ 'sort' ][ 'field' ] : 'id';

        $page    = ! empty( $datatable[ 'pagination' ][ 'page' ] ) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
        $perpage = ! empty( $datatable[ 'pagination' ][ 'perpage' ] ) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;

        $filter = isset( $datatable[ 'query' ][ 'generalSearch' ] ) && is_string( $datatable[ 'query' ][ 'generalSearch' ] ) ? $datatable[ 'query' ][ 'generalSearch' ] : '';

        $pages = 1;
        $total = VehicleSubCategorie::count();

        // $perpage 0; get all data
        if ( $perpage > 0 ) {
            $pages  = ceil( $total / $perpage ); // calculate total pages
            $page   = max( $page, 1 ); // get 1 page when $_REQUEST['page'] <= 0
            $page   = min( $page, $pages ); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ( $page - 1 ) * $perpage;
            if ( $offset < 0 ) {
                $offset = 0;
            }

        }
        $users = VehicleSubCategorie::orderby('id' ,'DESC')->with('vehiclecategorie')->offset($offset)->limit($perpage)->orderBy($field,$sort);

        if ( ! empty( $filter ) ) {
            $users->where('id', 'like', "%{$filter}%")
                ->orWhere('name','like',"%{$filter}%")
                ->orWhere('status','like',"%{$filter}%")
                ->orWhere('created_at','like',"%{$filter}%");

            unset( $datatable[ 'query' ][ 'generalSearch' ] );

        }

        $data=$users->get();


        $meta = array(
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
        );

        $result = array(
            'meta' => $meta + array(
                    'sort'  => $sort,
                    'field' => $field,
                ),
            'data' => $data
        );
          // print_r($result);exit();
        return  response()->json($result);

    }

    public function vcategories_change_status(Request $request)
    {
        $id = $request->input('user_id');
        $status = $request->input('activity');
        $user = VehicleSubCategorie::find($id) ;
        $user->status  = $status;
        $effected=$user->save();
        
        $response = array();
        if($effected){
        $response['activity'] = true;
        $response['message']=" Status has been changed Successfully.";
          }else{
        $response['activity'] = false;
        $response['message']="Fail to change the user status.";
          }
        return response()->json($response, 200);


    }
    public function select_vehicle_categories(Request $request)
      {
         // print_r($request->id);
          // print_r($request);
        $vehicle_sub_categories = VehicleSubCategorie::where('vehical_categorie_id',$request->id)->get();

        if($vehicle_sub_categories)
         return response()->json($vehicle_sub_categories, 200);
       else
        $response = "no data found";
          return response()->json($response, 200);
      }




}






