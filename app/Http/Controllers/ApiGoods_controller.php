<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Good;

class ApiGoods_controller extends Controller
{
  function get_all_goods()
  {

 		  $goodrecord = Good::where('status',1)->get();

 		if(count($goodrecord)){

          $status = true;

          $message = "goods found  successfully.";
          return response()->json(['status'=>$status,'message'=>$message,'goodrecord'=>$goodrecord], 200);
     	}
     	else{
          $status = false;
          $message = "no record found";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }
  } 

  function get_goods_by_id($id)
  {

      $goodrecord = Good::where('id',$id)->where('status', 1)->first();

    if(!empty($goodrecord)){

          $status = true;

          $message = "goods found  successfully.";
          return response()->json(['status'=>$status,'message'=>$message,'goodrecord'=>$goodrecord], 200);
      }
      else{
          $status = false;
          $message = "no record found";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }
  } 


}
