<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\VehicalCategorie;
use App\VehicleSubCategorie;
use App\Ride;
use App\Booking;
use App\Captain;
use Illuminate\Support\Facades\Log;
use App\Http\Resources\RidesCompletedCollection;
use App\Http\Resources\MapDetailCollection;
class ApiUserController extends Controller
{
   
     public function get_user_location()
    {
        $user = User::where('id', Auth::guard('api')->id())->select('latitude', 'longitude')->first();

        if(!empty($user)){
        $status = true;
        $message = "User location found.";
        return response()->json(['user' => $user, 'message' => $message, 'status' => $status], 200);
    } else {
        $status = false;
        $message = "No, record found.";
        return response()->json(['message' => $message, 'status' => $status], 200);
    }
}


 public function update_user_location(Request $request)
    {

        $validate = Validator::make($request->all(),[
            'latitude'  =>  'required',
            'longitude' =>  'required',
            'device_token' => 'required',
        ]);

        if($validate->fails()){
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['message' => $message, 'status' => $status], 200);
        } else {

                $update = User::where('id' , Auth::guard('api')->id())->update($request->all());

                if(!empty($update)){
                    $status = true;
                    $message = "User Location Updated.";

                    Log::info($message.Auth::guard('api')->id().' Latitude: '.$request->latitude.' Longitude: '.$request->longitude);
                    return response()->json(['message' => $message, 'status' => $status], 200);
                } else {
                    $status = false;
                    $message = 'Updated location is fail.';
                    return response()->json(['message' => $message, 'status' => $status], 200);
                }
        }
    }

     public function update_device_token(Request $request ,$id)
  {
    // validation the form data;
    $validator = Validator::make($request->all(),
     [
          'device_token' =>  'required',
    ]);

    if ($validator->fails())
    {
      $status = false;
      $message = $validator->errors()->first();
      return response()->json(['status'=>$status,'message'=>$message], 200);

    }else{
     $deviceToken = User::where('id', Auth::guard('api')->id())->update($request->all());

     if($deviceToken){

          $status = true;

          $message = "update_device_token  successfully.";
          return response()->json(['status'=>$status,'message'=>$message], 200);
     }
     else{
          $status = false;
          $message = "no record found";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }


     }

  }
  public function get_device_token(){

          $dtoken = User::select('device_token')->where('id', Auth::guard('api')->id())->first();

        if(!empty($dtoken)){

          $status = true;

          $message = "device_token is found.";
          return response()->json(['status'=>$status,'message'=>$message,'dtoken'=>$dtoken], 200);
        }
        else{
          $status = false;
          $message = "no record found";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }

  }

  public function get_user_by_phone(request $request){
      $validator = Validator::make($request->all(), [

        'phone' => 'required',
        ]);    
      if ($validator->fails())
      {
        $status = false;
        $message = $validator->errors()->first();
        return response()->json(['status'=>$status,'message'=>$message], 200);
      }
      else
      {
        if(isset($request->phone) && !empty($request->phone))
        {
          $user=User::select('api_token')->where(['phone' => $request->phone])->first();
        }
        
        if(isset($user) && !empty($user))
        {
          $status = true;
          $message = 'Enter new password.';
          return response()->json(['status'=>$status,'message'=>$message, 'data'=>$user], 200);
        }
        else{
          $status = false;
          $message = "Phone is not matched.";
          return response()->json(['status'=>$status,'message'=>$message], 200);
        }
  }
}

  public function update_user_password(Request $request){
    $validator = Validator::make($request->all(),
      [
        'password'=>'required'
    ]);
    if($validator->fails()){
      $status = false;
      $error = $validator->errors()->first();
      return response()->json(['status'=>$status, 'message'=>$error], 200);
    } else {
      $new_pass = $request->password;
      $hash = Hash::make($new_pass);
      $update = User::where('id', Auth::guard('api')->id())->update(['password' => $hash]);
      if($update){
        $status = true;
        $message = 'Password Update Success';
        return response()->json(['message'=>$message, 'status'=>$status], 200);
      } else {
        $status = false;
        $message = 'Error on updation try again';
        return response()->json(['message'=>$message, 'status'=>$status], 200);
      }
    }
  }

  public function captaindetailformap(request $request){
    $validator = Validator::make($request->all(),
      [
        'id'=>'required',
        'booking_id'=> 'required',
      ]);
    if($validator->fails()){
      $status = false;
      $error = $validator->errors()->first();
      return response()->json(['status'=>$status, 'message'=>$error], 200);
    } else {
    $detail = Ride::where('id' ,$request->id)->where('booking_id',$request->booking_id)->with('captain')->with('booking.vehicleSubCategorie.vehiclecategorie')->with('booking.good')->get();
    // dd($detail->toArray());
    if(!empty($detail)){
          $status = true;
          $responsen = new MapDetailCollection($detail);
          $message = "detail found successfully.";
          return response()->json(['status'=>$status,'message'=>$message,'detail'=>$responsen], 200);
      }
      else{
          $status = false;
          $message = "no record found";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }
    }
  }
  public function user_ride_cancel(Request $request){
    $validator = Validator::make(
      $request->all(),
      [
        'booking_id' => 'required',
      ]
    );
    if ($validator->fails()) {
      $status = false;
      $error = $validator->errors()->first();
      return response()->json(['status' => $status, 'message' => $error], 200);
    } else {
        $data=Booking::where('id',$request->booking_id)->where('status','2')->first();
      if (!empty($data)) {
        $status = true;
        $data->status=3;
        $data->save();
        $captains = Captain::select('device_token')->where('status',1)->where('vehical_cat_id',$data->vehical_cat_id)->where('vehical_sub_id',$data->vehical_sub_id)->get();
        foreach ($captains as $key => $value) {
          $tokens[]=$value->device_token;
        }
        $message = ['message' => 'Ride has been Canceled', 'type' => 'ride_cancel'];
        $message_status = captain_send_notification($tokens, $message, null);
        $message = "Booking Canceled Successfully";
        return response()->json(['status' => $status, 'message' => $message], 200);
      } else {
        $status = false;
        $message = "Some Thing Went Wrong Or Booking Already Accepted Or Completed";
        return response()->json(['status' => $status, 'message' => $message], 200);
      }
    }
  }
   public function changepassword(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'password' => 'required|min:6' ,
        ]);    
      if ($validator->fails()){
        $status = false;
        $message = $validator->errors()->first();
        return response()->json(['status'=>$status,'message'=>$message], 200);
      }else{
        $password = $request->password;
        $passwords['password'] = Hash::make($password);
        $user = User::where('id', Auth::guard('api')->id())->update($passwords);
        if($user){
          $status = true;
          $message = "'user password has been successfully changed.";
          return response()->json(['status'=>$status,'message'=>$message], 200);
        }else{
          $status = false;
          $message = " password not matched";
          return response()->json(['status'=>$status,'message'=>$message], 200);
        }
      }
    }
     function userProfile(Request $request)
   {
    $data = User::findOrFail(Auth::guard('api')->id());
    $validator = Validator::make($request->all(),
    [
      'fname' => 'required',     
      'phone' => 'required|unique:users,'.'id', 
    ]);

    if ($validator->fails())
    {
      $status = false;
      $message = $validator->errors()->first();
      return response()->json(['status'=>$status,'message'=>$message], 200);
    }else{
      $data = User::findOrFail(Auth::guard('api')->id());
      $input = $request->all();
      ///here captain image update
      $user_image='';
       if($images = $request->file('images')){
          $user_image = $request->file('images')->getClientOriginalName();
          $Extension_profile = $request->file('images')->getClientOriginalExtension();
          $user_image = 'profile'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('images')->move('images/user/profile/', $user_image);
      }if($request->password){
         $new_pass = $request->password;
          $input['password']  = Hash::make($new_pass);
      }
        $output_file = 'images/user/profile/'.$user_image;
        $profile['user'] = URL($output_file);
        $input['images'] = $user_image;
        $input['fname'] = $request->fname ? $request->fname : $data->fname;
        $input['lname'] = $request->lname ? $request->lname : $data->lname;
        $input['email'] = $request->email ? $request->email : $data->email;
        $input['phone'] = $request->phone ? $request->phone : $data->phone;
        $update_image = $data->update($input);
        if(!empty($update_image)){
          $data = User::where('id',Auth::guard('api')->id())->first();
          $status = true;
          $message = "User Image update successfully";
        return response()->json(['status'=>$status,'message'=>$message, 'data'=>$data ], 200);}
        else{
          $status = false;
          $message = " User image not update. ";
          return response()->json(['status'=>$status,'message'=>$message], 200);
        }
    }
  }

   public function get_profile_image(){
    $location = User::where('id',Auth::guard('api')->id())->first();

          $output_file = 'images/user/profile/'.$location->images;
          
          $profile = URL($output_file);
         
    if(!empty($location->images)){
          $status = true;
          $message = "user profile found successfully.";
          return response()->json(['status'=>$status,'message'=>$message, 'profile' =>$profile ], 200);
      }
      else{
          $status = false;
          $message = "user profile image not found";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }
  
  }

    function user_update_account(Request $request)
   {
     
      $data = User::findOrFail(Auth::guard('api')->id());
      $input['fname'] = $request->fname?$request->fname:$data->fname;
      $input['email'] = $request->email?$request->email:$data->email;
      $input['phone'] = $request->phone?$request->phone:$data->phone;
       if($images = $request->file('images')){
          $user_image = $request->file('images')->getClientOriginalName();
          $Extension_profile = $request->file('images')->getClientOriginalExtension();
          $user_image = 'profile'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('images')->move('images/user/profile/', $user_image);
          $input['images'] = $user_image;
          }
         $update_account = $data->update($input);

         if(!empty($update_account)){
          $status = true; 
          $message = "User update profile  successfully";
        return response()->json(['status'=>$status,'message'=>$message, 'data'=>$update_account], 200);}
        else{
          $status = false;
          $message = " User profile not update. ";
          return response()->json(['status'=>$status,'message'=>$message], 200);
        }
      
    }

}
