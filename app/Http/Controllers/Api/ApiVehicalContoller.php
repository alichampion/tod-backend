<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\VehicalCategorie;
use App\VehicleSubCategorie;
use App\Captain;
use App\Ride;
use App\CaptainOffer;
use App\UserOffer;
use DB;
class ApiVehicalContoller extends Controller
{
     function get_all_vehicals_categories()
    {
    	$vehicals =VehicalCategorie::where('status',1)->get();

    	if(count($vehicals)> 1){
    		 $status = true;
            $message= 'Records found successfully.';
            return response()->json(['message' => $message, 'status' => $status,'VehicalCategorie'=>$vehicals]);

    	}else{
    		$status = false;
            $message = 'No Record Found:';
            return response()->json(['message' => $message, 'status' => $status]);
    	}
    }


    function get_all_vehicals(Request $request)
    {

        $validator = Validator::make($request->all(),
    [
          'vehical_categorie_id' => 'required',
    ]);

    if ($validator->fails())
    {
      $status = false;
      $message = $validator->errors()->first();
      return response()->json(['status'=>$status,'message'=>$message], 200);

    }else{

      $vehicals = VehicleSubCategorie::where('vehical_categorie_id' , $request->vehical_categorie_id)->get();
        
        if(count($vehicals)){
             $status = true;
            $message= 'Records found successfully.';
            return response()->json(['message' => $message, 'status' => $status,'vehical_sub_categories'=>$vehicals]);

        }else{
            $status = false;
            $message = 'No Record Found:';
            return response()->json(['message' => $message, 'status' => $status]);
        }
    }
    }

    function get_vehical_by_id($id)
    {
       
        $vehical = VehicalCategorie::where('id',$id)->where('status',1)->with('VehicleSubCategorie')->first();
        
        if(!empty($vehical)){
             $status = true;
            $message= 'Records found successfully.';
            return response()->json(['message' => $message, 'status' => $status,'vehicals_sub_cat'=>$vehical]);

        }else{
            $status = false;
            $message = 'No Record Found:';
            return response()->json(['message' => $message, 'status' => $status]);
        }
    }

    //  old One
    
     function get_active_vehical_captain(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'longitude' => 'required',
                'latitude' => 'required',
            ]
        );
        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $locations = Captain::select('longitude', 'latitude', 'api_token', 'status')->where('activity', 1)->where('status', 1)->get()->toArray();
            if (count($locations)) {
                $status = true;
                $message = 'following drivers available';
                $options = DB::table('options')->where('name', 'radius')->get()->toArray();
                if ($options) {
                    $radius = $options[0]->value;
                } else {
                    $radius = '15';
                }
                $vehicles = [];
                foreach ($locations as $key => $value) {
                    $km = distanceCalculation($value['latitude'], $value['longitude'], $request->latitude, $request->longitude);
                    $km = explode('.', $km)[0];
                    if ($km <= round($radius, '2')) {
                        $vehicles[] = array('vehicle_details' => array_merge($value, ['vehicle_current_radius' => $km]), 'radius' => $radius);
                    }
                }
                return response()->json(['message' => $message, 'status' => $status, 'vehicles' => $vehicles], 200);
            } else {
                $status = false;
                $message = 'no drivers available';
                return response()->json(['message' => $message, 'status' => $status], 200);
            }
        }
    }
    function activity_update_cron(){
        $captains = Captain::select( 'activity','status', 'last_loc_update','id')->where('activity', 1)->where('status','!=', 0)->get();
        if(!empty($captains)){
            foreach ($captains as $key => $value) {
                $secs=[];
                if (!empty($value->last_loc_update)) {
                    $secs = explode(':', timeDiffInSec(explode(' ', date('Y-m-d h:i:s'))[1], explode(' ', $value->last_loc_update)[1]));
                }
                if (isset($secs[1]) && $secs[1] >= 1 || isset($secs[1]) && $secs[2] >= 50) {
                    DB::table('captains')->where( 'id', $value->id )->update(['status'=>0]);
                }elseif(isset($secs[1]) && $secs[1] <= 1 || isset($secs[1]) && $secs[2] <= 50){
                    DB::table('captains')->where('id', $value->id)->update(['status' => 1]);
                }
            }
        }
    }
    


    function new_coords($latitude, $longitude, $bearing, $distance, $unit = 'km')
    {

    if ($unit == 'm')
    {
        $radius = 3959;
    }
    elseif ($unit == 'km')
    {
        $radius = 6371;
    }

//  New latitude in degrees.
    $new_latitude = rad2deg(asin(sin(deg2rad($latitude)) * cos($distance / $radius) + cos(deg2rad($latitude)) * sin($distance / $radius) * cos(deg2rad($bearing))));
        
//  New longitude in degrees.
    $new_longitude = rad2deg(deg2rad($longitude) + atan2(sin(deg2rad($bearing)) * sin($distance / $radius) * cos(deg2rad($latitude)), cos($distance / $radius) - sin(deg2rad($latitude)) * sin(deg2rad($new_latitude))));

//  Assign new latitude and longitude to an array to be returned to the caller.
    $coord['latitude'] = $new_latitude;
    $coord['longitude'] = $new_longitude;

        return $coord;

    }
    public function offer_done(Request $request)
    {
        // $offer_timing = Offer::
        $captains = DB::table('rides')->select('captain_id', DB::raw('count(*) as total'))->groupBy('captain_id')->get();
         $users = DB::table('rides')->select('user_id', DB::raw('count(*) as total'))->groupBy('user_id')->get();

    foreach ($captains as $key => $value){ 
        if($request->offer == 'captain_offer' && $value->total > $offer->rides){
            switch ($offer->time_period) {
                case 'weakly':
                            $captain_offer = CaptainOffer::insert([
                                'captain_id'=> $value->captain_id,
                                'offer_id'=> $offer->id,
                                'amount'=> $offer->amount,
                                'time_period' => 'weakly',
                            ]);
                    break;
                case 'monthly':
                    
                    break;
                case 'yearly':
                    
                    break;
            }
        }
    }
    foreach ($users as $key => $value) {
        if($value->total >= $request->rides){
            switch ($offer->time_period) {
                case 'weakly':
                         $captain_offer = UserOffer::insert([
                                'user_id'=> $value->user_id,
                                'offer_id'=> $offer->id,
                                'amount'=> $offer->amount,
                                'time_period' => 'weakly',
                            ]);
                    break;
                case 'monthly':
                    
                    break;
                case 'yearly':
                        
                    break;   
            }
        }
    }
}

}
