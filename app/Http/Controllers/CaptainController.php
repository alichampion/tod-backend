<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Admin;
use App\User;
use App\Captain;
use App\Ride;
class CaptainController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:captain');
    }

   
    public function index()
    {
    
        $admin = Admin::all();
        $user= User::all();
        $captain= Captain::all(); 
        $users =User::count();
        $captains= Captain::count(); 
        $admins = Admin::count();
        $rides = Ride::count(); 

       return View('captain.captainindex', compact(['admin', 'admins', 'user', 'users', 'captain', 'captains' , 'rides']));
        
        
    }

   
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
