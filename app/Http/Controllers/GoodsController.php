<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Good;

class GoodsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin']);
    }
    
    public function index()
    {
       // $food = Good::all();
        return view('admin.goodstype');
    }
    public function create()
    {
    
        return view('admin.goodCreate');
        
    }
    public function store(Request $request)
    {
         $validator = Validator::make($request->all(),
        [
        'good_name' => 'required|max:40||unique:goods',
        ]);

       if($validator->fails())
       {
            return Redirect::back()->withErrors($validator);
       } 
       else
       {
        $good_affected = Good::create([
            'good_name'          => $request['good_name'],
            ]);

        if($good_affected)
        session()->flash('success', 'Record has been added. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');

        return redirect()->route('goods.index');
        }
    }
    
    public function show(Request $request)
    {
        
    }

    
    public function edit($id)
    {
         $Datatypes = Good::find($id);
 
    
        return view('admin.goodstypeEdit', compact('Datatypes'));
        
    }

    
    public function update(Request $request, $id)
    {
        $Datatypes = Good::findOrFail($id);
        $input = $request->all();

        $good_affected = $Datatypes->update($input);

        if($good_affected)
        session()->flash('success', 'Record has been added. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');
        return redirect()->route('goods.index');
    }

   
    public function delete_record($id)
    {
        $good_item = Good::find($id);

        $good_item_destory = $good_item->delete();

        if($good_item_destory){
        $response['status'] = true;
        $response['message'] = "Record has been delete Successfully.";
        }else{
        $response['status'] = false; 
        $response['message'] = "Fail to delete Record.";
        }
        return redirect()->route('goods.index');
    } 

     public function get_good(Request $request)
    {
       $datatable = ! empty( $request->datatable ) ? $request->datatable : array();
        $datatable = array_merge( array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable );

        $sort  = ! empty( $datatable[ 'sort' ][ 'sort' ] ) ? $datatable[ 'sort' ][ 'sort' ] : 'desc';
        $field = ! empty( $datatable[ 'sort' ][ 'field' ] ) ? $datatable[ 'sort' ][ 'field' ] : 'id';

        $page    = ! empty( $datatable[ 'pagination' ][ 'page' ] ) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
        $perpage = ! empty( $datatable[ 'pagination' ][ 'perpage' ] ) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;

        $filter = isset( $datatable[ 'query' ][ 'generalSearch' ] ) && is_string( $datatable[ 'query' ][ 'generalSearch' ] ) ? $datatable[ 'query' ][ 'generalSearch' ] : '';

        $pages = 1;
        $total = Good::count();

        // $perpage 0; get all data
        if ( $perpage > 0 ) {
            $pages  = ceil( $total / $perpage ); // calculate total pages
            $page   = max( $page, 1 ); // get 1 page when $_REQUEST['page'] <= 0
            $page   = min( $page, $pages ); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ( $page - 1 ) * $perpage;
            if ( $offset < 0 ) {
                $offset = 0;
            }

        }
        $users = Good::offset($offset)->limit($perpage)->orderBy($field,$sort);

        if ( ! empty( $filter ) ) {
            $users->where('id', 'like', "%{$filter}%")
                ->orWhere('good_name','like',"%{$filter}%")
                ->orWhere('status','like',"%{$filter}%")
                ->orWhere('created_at','like',"%{$filter}%");
            unset( $datatable[ 'query' ][ 'generalSearch' ] );

        }

        $data=$users->get();
        $meta = array(
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
        );

        $result = array(
            'meta' => $meta + array(
                    'sort'  => $sort,
                    'field' => $field,
                ),
            'data' => $data
        );
          // print_r($result);exit();
        return  response()->json($result);


    }

    public function good_change_status(Request $request)
    {
        $id = $request->input('user_id');
        $status = $request->input('activity');
        $user = Good::find($id) ;
        $user->status  = $status;
        $effected=$user->save();
        
        $response = array();
        if($effected){
        $response['activity'] = true;
        $response['message']=" Status has been changed Successfully.";
          }else{
        $response['activity'] = false;
        $response['message']="Fail to change the user status.";
          }
        return response()->json($response, 200);

    }


}

