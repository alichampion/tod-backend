<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\City;
use App\Country;
use Session;
class CityController extends Controller
{
     public function __construct()
    {
        $this->middleware(['auth:admin']);
    }
    
    public function index()
    {
     
        return view('admin.city.citi_index');
    }

    
    public function create()
    {
        $countries = Country::orderBy('id' , 'DESC')->get();
      // return $countries;
        return view('admin.city.citi_Create' , compact(['countries']));

    }

    
    public function store(Request $request)
    {
         $validator = Validator::make($request->all(),
        [
        'name' => 'required|max:40||unique:cities',
        ]);

       if($validator->fails())
       {
            $message = $validator->errors()->first();
            Session::flash('info', " $message") ;
            return Redirect::back()->withErrors($validator);
       } 
       else
       {
        $good_affected = City::create([
            'name'  => $request['name'],
            'countrie_id'  => $request['countrie_id'],
    
            ]);

        if($good_affected)
        session()->flash('success', 'Record has been added. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');

        return redirect()->route('cities.index');
        }
    }
    
    public function show(Request $request)
    {
        
    }
    
    public function edit($id)
    {
        $countries = Country::orderBy('id' , 'DESC')->get();
        $city_data = City::find($id);

        return view('admin.city.citi_Edit', compact('city_data' ,'countries'));
        
    }

    
    public function update(Request $request, $id)
    {
        $branch_item = City::findOrFail($id);
        $input = $request->all();

        $branch_affected = $branch_item->update($input);

        if($branch_affected)
        session()->flash('success', 'Record has been added. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');
        return redirect()->route('cities.index');
    }

   
    public function delete_record($id)
    {
        $Branch_item = City::find($id);

        $branch_item_destory = $Branch_item->delete();

        if($branch_item_destory){
        $response['status'] = true;
        session()->flash('success_message', 'Record has been deleted. successfully.');
        $response['message'] = "Record has been delete Successfully.";
        }else{
        $response['status'] = false; 
        $response['message'] = "Fail to delete Record.";
        }
        return redirect()->route('cities.index');
    } 

     public function get_citi(Request $request)
    {

    
    $datatable = ! empty( $request->datatable ) ? $request->datatable : array();
        $datatable = array_merge( array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable );

        $sort  = ! empty( $datatable[ 'sort' ][ 'sort' ] ) ? $datatable[ 'sort' ][ 'sort' ] : 'desc';
        $field = ! empty( $datatable[ 'sort' ][ 'field' ] ) ? $datatable[ 'sort' ][ 'field' ] : 'id';

        $page    = ! empty( $datatable[ 'pagination' ][ 'page' ] ) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
        $perpage = ! empty( $datatable[ 'pagination' ][ 'perpage' ] ) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;

        $filter = isset( $datatable[ 'query' ][ 'generalSearch' ] ) && is_string( $datatable[ 'query' ][ 'generalSearch' ] ) ? $datatable[ 'query' ][ 'generalSearch' ] : '';

        $pages = 1;
        $total = City::count();

        // $perpage 0; get all data
        if ( $perpage > 0 ) {
            $pages  = ceil( $total / $perpage ); // calculate total pages
            $page   = max( $page, 1 ); // get 1 page when $_REQUEST['page'] <= 0
            $page   = min( $page, $pages ); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ( $page - 1 ) * $perpage;
            if ( $offset < 0 ) {
                $offset = 0;
            }

        }
        $users = City::with('country')->offset($offset)->limit($perpage)->orderBy($field,$sort);

        if ( ! empty( $filter ) ) {
            $users->where('id', 'like', "%{$filter}%")
                ->orWhere('name','like',"%{$filter}%")
                ->orWhere('status','like',"%{$filter}%")
                ->orWhere('created_at','like',"%{$filter}%");

            unset( $datatable[ 'query' ][ 'generalSearch' ] );

        }

        $data=$users->get();


        $meta = array(
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
        );

        $result = array(
            'meta' => $meta + array(
                    'sort'  => $sort,
                    'field' => $field,
                ),
            'data' => $data
        );
          // print_r($result);exit();
        return  response()->json($result);

    }
    public function citi_change_status(Request $request)
    {
        $id = $request->input('user_id');
        $status = $request->input('activity');
        $user = City::find($id) ;
        $user->status  = $status;
        $effected=$user->save();
        
        $response = array();
        if($effected){
        $response['activity'] = true;
        $response['message']=" Status has been changed Successfully.";
          }else{
        $response['activity'] = false;
        $response['message']="Fail to change the user status.";
          }
        return response()->json($response, 200);


    }
      public function city_on_select(Request $request)
      {
         // print_r($request->id);
          // print_r($request);
        $city_item = City::where('countrie_id',$request->id)->get();

        if($city_item)
         return response()->json($city_item, 200);
       else
        $response = "no data found";
          return response()->json($response, 200);
      }


}


