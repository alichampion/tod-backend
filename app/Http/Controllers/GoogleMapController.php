<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Captain;

class GoogleMapController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function index()
    {
        $captains =Captain::where('status','<>', 0)->get();
        return view('admin.maps_vector',compact('captains'));
    }

      
}
