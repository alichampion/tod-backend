<?php

namespace App\Http\Controllers\Admin\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Setting;
use App\Account;
use App\Master;
use Session;
use DB;
use Auth;

class RecivableController extends Controller
{
       public function __construct()
  {
    $this->middleware(['auth:admin']);
  }
    public function index()
    {
       $recivable_amount = Account::where('c_balance','<',0)->orderBy('id' , 'DESC')->sum('c_balance');
       $amount = Account::where('c_balance','<',0)->get();
        return view('admin.Account.reciable_account',compact('recivable_amount','amount'));
    }
    public function reciveable_account_record(Request $request){
    	$limit = $request->input('length');
        $start = $request->input('start');
        $start = $start?$start+1:$start;
        $search = $request->input('search.value');
        $order_column_no = $request->input('order.0.column');
        $order_dir = $request->input('order.0.dir');
        $order_column_name = $request->input("columns.$order_column_no.data");
   
          $user = Account::where('c_balance','<',0)->orderBy('id' , 'DESC')->offset($start)->limit($limit)->orderBy($order_column_name,$order_dir);
       
          if(!empty($search)){
            $user->where('code', 'like', "%{$search}%")
            ->orWhere('title','like',"%{$search}%")
            ->orWhere('c_balance','like',"%{$search}%")
            ->orWhere('created_at','like',"%{$search}%"); 
          }

          $data = $user->get();

          $totalFiltered = Account::count();

          $json_data = array(
          "draw"      => intval($request->input('draw')),
          "recordsTotal"  => count($data),
          "recordsFiltered" => intval($totalFiltered),
          "data"      => $data
        );

           return response()->json($json_data, 200);
    }
}
