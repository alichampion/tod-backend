<?php

namespace App\Http\Controllers\Admin\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Setting;
use App\Master;
use App\Account;
use App\Invoice;
use Session;
use DB;

class FinancialController extends Controller
{
      public function __construct()
  {
    $this->middleware(['auth:admin']);
  }
    public function index()
    {
    	   $dabit_amount=0;
         $credit_amount=0;
         $records=array();
      $accounts = Account::where('c_balance' ,'<>',0)->orderBy('account_cat_id', 'ASC')->get();
      $masters = Master::get();
      $records = array();
      $index = 0;
        foreach($accounts as $codes){
          $user = Master::where('code',$codes->code)->get();
            $dabit_amont=0;
            $credit_total=0;
            $opening_balance= $codes->balance;
            $code= $codes->code;
            $title = $codes->title;
            $account_cat_id = $codes->account_cat_id;
            foreach ($user as $key2 ) {
              if($key2->amt_type == 'CR'){ 
                $credit_total += $key2->amount;
                $credit_amount  =$credit_amount + $key2->amount;
                 
              }else{
                $dabit_amont +=$key2->amount;
                $dabit_amount  =$dabit_amount + $key2->amount;
              }
              
            }
           
            

            $data= new \stdClass();
            $data->id  =$index;
            $data->dabit  =$dabit_amont;
            $data->credit  =$credit_total;
            $data->code  =$code;
            $data->title  =$title;
            $data->account_cat_id=$account_cat_id;
            $data->opening_balance  =$opening_balance;              
            $records[$index++]=$data;
          }
          
         return view('admin.Account.financial', compact('dabit_amount','credit_amount' ,'records'));
    }

    public function get_search_value(Request $request){

    	$limit = $request->input('length');
        $start = $request->input('start');
        $start = $start?$start+1:$start;
        $search = $request->input('search.value');
        $order_column_no = $request->input('order.0.column');
        $order_dir = $request->input('order.0.dir');
        $order_column_name = $request->input("columns.$order_column_no.data");
    	   $dabit_amount=0;
	       $credit_amount=0;
	       $records=array();

	    $accounts = Account::where('c_balance' ,'<>',0)->orderBy('account_cat_id', 'ASC')->offset($start)->limit($limit)->orderBy($order_column_name,$order_dir);
          if(!empty($search)){
            $accounts->where('code', 'like', "%{$search}%")
            ->orWhere('title','like',"%{$search}%")
            ->orWhere('c_balance','like',"%{$search}%");    
          }
           $account = $accounts->get();
          // dd($account);
      $masters = Master::get();
      // $invoices = Invoice::get();
      $records = array();
      $index = 0;
        foreach($account as $codes){
          // $test2 = Invoice::orWhere('code',$codes->code)->orWhere('payable_cd',$codes->code)->orWhere('income_cd',$codes->code)->get();
          $user = Master::where('code',$codes->code)->get();
          // dd($codes->code);
            $dabit_amont=0;
            $credit_total=0;
            $opening_balance= $codes->balance;
            $code= $codes->code;
            $title = $codes->title;
            // foreach($test2 as $test1){
            //   if($test1->payable_cd == $code)
            //     $credit_total += $test1->payable_amount;
            //   elseif($test1->income_cd == $code)
            //     $credit_total += $test1->income;
            //   elseif($test1->code == $code)
            //    $dabit_amont +=$test1->reciveable_amount;
            // }
            foreach ($user as $key2 ) {
              if($key2->amt_type == 'CR') 
                $credit_total += $key2->amount;
              else
                $dabit_amont +=$key2->amount;
            }
            $data= new \stdClass();
            $data->id  =$index;
            $data->dabit  =$dabit_amont;
            $data->credit  =$credit_total;
            $data->code  =$code;
            $data->title  =$title;
            $data->opening_balance  =$opening_balance;              
            $records[$index++]=$data;
        }
          $data = $records;
          // dd($data);
          $totalFiltered = Account::count();
          $json_data = array(
          "draw"      => intval($request->input('draw')),
          "recordsTotal"  => count($data),
          "recordsFiltered" => intval($totalFiltered),
          "data"      => $data
        );
           return response()->json($json_data, 200);
    }
}
