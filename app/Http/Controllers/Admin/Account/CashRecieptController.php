<?php

namespace App\Http\Controllers\Admin\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Setting;
use App\Master;
use App\Account;
use Session;
use DB;
class CashRecieptController extends Controller
{
       public function __construct()
  {
    $this->middleware(['auth:admin']);
  }
    public function index()
    {
         return view('admin.Account.cash_reciept');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
         $validator = Validator::make($request->all(),
        [
         'code' => 'required',
         'goes_to' => 'required',
         'recpno' => 'required',
         'amount' => 'required',
         'posting_date' => 'required',
       ]);
       if($validator->fails())
        {
            $message = $validator->errors()->first();
            Session::flash('info', " $message") ;
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            DB::beginTransaction();
            $posting_date=date('Y-m-d', strtotime($request->posting_date)). ' ' .date('H:i:s',strtotime(now()));
            $master_affected = Master::insert([
            'v_type' => 'CR',
            'amt_type' => 'DB',
            'code' => request('code'),
            'recpno' => request('recpno'),
            'description' => request('description'),
            'amount' => request('amount'),
            'goes_to' => request('goes_to'),
            'posting_date' => $posting_date,
            'created_by' => request('created_by'),
            'chqno' => request('chqno')?request('chqno'):''
            ]);
            if(!$master_affected){
                DB::rollback();
                session()->flash('danger', 'Fail to insert the record.');
                return Redirect()->route('cash-reciept.index');
            }else{
                $acc_upd=Account::where('code', $request->code)->first();
                $upd['c_balance']=$acc_upd->c_balance + $request->amount;
                $acc_upd->update($upd);
                 $obj_master_affected = Master::insert([
                    'v_type' => 'CR',
                    'amt_type' => 'CR',
                    'code' => request('goes_to'),
                    'recpno' => request('recpno'),
                    'from_cd' => request('from_cd'),
                    'description' => request('description'),
                    'amount' => request('amount'),
                    'posting_date' => $posting_date,
                    'created_by' => request('created_by'),
                    'chqno' => request('chqno')?request('chqno'):''
                ]);
                if(!$obj_master_affected){
                DB::rollback();
                session()->flash('danger', 'Fail to insert the record.');
                return Redirect()->route('cash-reciept.index');
                }else{
                    $acc_upd=Account::where('code', $request->goes_to)->first();
                    $upd['c_balance']=$acc_upd->c_balance + $request->amount;
                    $acc_upd->update($upd);
                    DB::commit();
                    session()->flash('success', 'Record has been inserted. successfully.');
                    return Redirect()->route('cash-reciept.index');
                }
            }
          
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cash_reciept= Master::where('id', $id)->first();
        return view('admin.Account.cash_reciept_edit',compact('cash_reciept'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),
        [
         'code' => 'required',
         'goes_to' => 'required',
         'recpno' => 'required',
         'amount' => 'required',
         'posting_date' => 'required',
       ]);
       if($validator->fails())
        {
            $message = $validator->errors()->first();
            Session::flash('info', " $message") ;
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            DB::beginTransaction();
            $posting_date=date('Y-m-d', strtotime($request->posting_date)). ' ' .date('H:i:s',strtotime(now()));
            $record['v_type'] = 'CR';
            $record['amt_type'] = 'DB';
            $record['code'] = request('code');
            $record['goes_to'] = request('goes_to');
            $record['recpno'] = request('recpno');
            $record['description'] = request('description');
            $record['amount'] = request('amount');
            $record['posting_date'] = $posting_date;
            $record['created_by'] = request('created_by');
            $record['chqno'] = request('chqno')?request('chqno'):'';
            $master_affected = Master::where('id',$id)->update($record);
            if(!$master_affected){
                DB::rollback();
                session()->flash('danger', 'Fail to insert the record.');
                return Redirect()->route('cash-reciept.index');
            }else{
                $acc_upd=Account::where('code', $request->goes_to)->first();
                $upd['c_balance']=$acc_upd->c_balance - $request->amount;
                $acc_upd->update($upd);
                DB::commit();
                session()->flash('success', 'Record has been updated. successfully.');
                return Redirect()->route('cash-reciept.index');
                }
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
