<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use App\User;
use App\Captain;
use App\Ride;
use Auth;



class HomeController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    
    public function index()
    { 
        $admin = Admin::all();
        $user= User::all();
        $captain= Captain::all(); 
        $users =User::count();
        $captains= Captain::count(); 
        $admins = Admin::count();
        $rides = Ride::count(); 
        return view('user.index', compact([ 'admin', 'user', 'captain'  , 'users',  'captains', 'admins' , 'rides'])); 
    }

    
}
