<?php

namespace App\Http\Controllers\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Good;


class GoodsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }
    
    public function index()
    {
        $datatype = Good::orderBy('id', 'DESC')->paginate(5);
       // $datatypes =goods::count();
       // $categoris =goods_categories::count();
        return view('user.goodstype', compact(['$datatype', 'datatype']));
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        Good::create([
            'good_name'          => $request['good_name'],
            'good_category_id'   => $request['good_category_id'],
            ]);
        return redirect('good');
    }

    
    public function show(datatype $datatype)
    {
        //
    }

    
    public function edit($id)
    {
         $Datatypes = Good::find($id);
    
        return view('user.goodstypeEdit', compact('Datatypes'));
        
    }

    
    public function update(Request $request, $id)
    {
        $Datatypes = Good::findOrFail($id);
        $input = $request->all();

        $Datatypes->update($input);
        return redirect()->route('good.index');
    }

   
    public function destroy($id)
    {
       $datatyp = Good::find($id);
    
     
       
        $datatyp->delete();
        return Redirect::route('good.index');
    }    
}
