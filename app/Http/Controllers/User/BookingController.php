<?php

namespace App\Http\Controllers\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Booking;
use App\Ride;
use Illuminate\Support\Facades\Input;
use Auth;

class BookingController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:web');
    }
    public function index()
    {
        $book = Booking::where('user_id', Auth::guard('web')->id())->orderBy('id', 'DESC')->with('BookingDestination')->with('user')->with('good')->paginate(10);
        $books =Booking::count();
        return view('user.bookings' , compact([ 'book', 'books']));
    }

    public function show($id)
    {
        $Booking = Booking::where('id', $id)->with('BookingDestination')->with('good')->with('user')->with('vehical')->get();
        $ride = Ride::where('booking_id' , $id)->with('captain')->with('RideCompleted')->get();
        //return $Booking;exit;
        //dd($ride);exit;
      return view('user.bookingDetail', compact('Booking', 'ride'));
    }
}
