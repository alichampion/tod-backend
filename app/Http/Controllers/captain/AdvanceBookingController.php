<?php

namespace App\Http\Controllers\captain;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\AdvanceBooking;
use Illuminate\Support\Facades\Input;

class AdvanceBookingController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:captain');
    }
    
    public function index()
    {
        $book = AdvanceBooking::orderBy('id', 'DESC')->with("user")->with("vehical")->with("good")->paginate(5);
       
        $books =AdvanceBooking::count();
        return view('captain.advanceBooking' , compact([ 'book', 'books']));
    }
}
