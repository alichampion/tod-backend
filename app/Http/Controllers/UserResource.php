<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Ride;
use App\Booking;
use App\UserWallet;
use App\file;
use Illuminate\Support\Facades\Input;
use App\Mail\RecieptMail;
use Illuminate\Support\Facades\Mail;
class UserResource extends Controller
{

  public function __construct()
    {
        $this->middleware(['auth:admin']);
    }
    
    
    public function index()
    {
       $user = User::get();
       // return $user;
        return view('admin.user.userRosource');
    }

    
    public function create()
    {
       return view('admin.user.page_user_profile');
    }

    
    public function store(Request $request)
    { 
        $validator = Validator::make($request->all(),
    [
     'fname' => 'required',
     'lname' => 'required',
     'email' => 'required|email|unique:users',
     'phone' => 'required',
      'images' => 'required',
     'password' => 'required|min:6|confirmed',
     'password_confirmation' => 'required_with:password|same:password|min:6'
     
   
   ]);

       if($validator->fails())
       {
            return Redirect::back()->withErrors($validator);
       }
   else {
       $obj = new User;

        $profile = $request->file('images')->getClientOriginalName();

        if($request->hasfile('images')){
          $Extension_profile = $request->file('images')->getClientOriginalExtension();
          $profile = 'user'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('images')->move('images/user/profile/', $profile);
      }
       
      $obj->fname = request('fname');
      $obj->lname = request('lname');
      $obj->phone = request('phone');
      $obj->email = request('email');
      $obj->password =  Hash::make($request->password);
      $obj->images = $profile;
      $obj->api_token = str_random(60);
      $affected = $obj->save();
      if($affected)
        session()->flash('success', 'Record has been updated. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');
      return redirect()->route('userResource.index');
   }
}

    public function edit($id)
    {
        $User = User::find($id);
    
        return view('admin.user.page_user_profile_edit', compact('User'));
    }

    
    public function update(Request $request, $id)
    {


         $User = User::findOrFail($id);

        $input = $request->all();

        if(empty($input['password']))
            unset($input['password']);

          if($request->hasfile('images')){
          $profiles = $request->file('images')->getClientOriginalName();
          $Extension_profile = $request->file('images')->getClientOriginalExtension();
          $profile = 'profiles'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('images')->move('images/user/profile/', $profile);
          $input['images'] =$profile;
          }

          if(isset($request->password) && !empty($request->password)) 
        {
          $input['password'] = Hash::make($request->password);
        }

        $affected=$User->update($input);
        if($affected)
        session()->flash('success', 'Record has been updated. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');
        return redirect()->route('userResource.index');
    }

    
    public function delete_record($id)
    {

        $User = User::find($id);
        $effected=$User->delete();
        
        if($effected){
        $response['status'] = true;
        $response['message'] = "User has been delete Successfully.";
         session()->flash('success_message', 'Record has been updated. successfully.');
        }else{
        $response['status'] = false; 
        $response['message'] = "Fail to delete User.";
         session()->flash('failed', 'Fail to delete User.');
        }
         // response()->json($response);
        return redirect()->route('userResource.index');
    }

     public function show($id)
    {

      $users = Booking::where('user_id' ,$id)->with('Rides')->with('good')->with('vehical')->orderBy('id', 'DESC')->get();
      return view('admin.user.user_profile_Byid', compact('users'));

    }
    public function walletShow($id)
    {
        //\DB::enableQueryLog();
        $wallet = UserWallet::where('user_id',$id)->with("user")->first();
        //dd(\DB::getQueryLog());
        
        return view('admin.user.userswallets' , compact('wallet'));
    }

    public function bookingdetail($id)
    {

      $users = Ride::where('booking_id' ,$id)->with('captain')->with('RideCompleted')->orderBy('id', 'DESC')->get();
   
      return view('admin.user.historydetail', compact('users'));

    }


     public function get_users(Request $request)
    {
      $datatable = ! empty( $request->datatable ) ? $request->datatable : array();
        $datatable = array_merge( array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable );

        $sort  = ! empty( $datatable[ 'sort' ][ 'sort' ] ) ? $datatable[ 'sort' ][ 'sort' ] : 'desc';
        $field = ! empty( $datatable[ 'sort' ][ 'field' ] ) ? $datatable[ 'sort' ][ 'field' ] : 'id';

        $page    = ! empty( $datatable[ 'pagination' ][ 'page' ] ) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
        $perpage = ! empty( $datatable[ 'pagination' ][ 'perpage' ] ) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;

        $filter = isset( $datatable[ 'query' ][ 'generalSearch' ] ) && is_string( $datatable[ 'query' ][ 'generalSearch' ] ) ? $datatable[ 'query' ][ 'generalSearch' ] : '';

        $pages = 1;
        $total = User::count();

        // $perpage 0; get all data
        if ( $perpage > 0 ) {
            $pages  = ceil( $total / $perpage ); // calculate total pages
            $page   = max( $page, 1 ); // get 1 page when $_REQUEST['page'] <= 0
            $page   = min( $page, $pages ); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ( $page - 1 ) * $perpage;
            if ( $offset < 0 ) {
                $offset = 0;
            }

        }
        $users = User::offset($offset)->limit($perpage)->orderBy($field,$sort);

        if ( ! empty( $filter ) ) {
            $users->where('id', 'like', "%{$filter}%")
                ->orWhere('fname','like',"%{$filter}%")
                ->orWhere('email','like',"%{$filter}%")
                ->orWhere('phone','like',"%{$filter}%")
                ->orWhere('status','like',"%{$filter}%")
                ->orWhere('created_at','like',"%{$filter}%");

            unset( $datatable[ 'query' ][ 'generalSearch' ] );

        }

        $data=$users->get();


        $meta = array(
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
        );

        $result = array(
            'meta' => $meta + array(
                    'sort'  => $sort,
                    'field' => $field,
                ),
            'data' => $data
        );
          // print_r($result);exit();
        return  response()->json($result);

    }


           public function downloadExcel($type)
        {
    
            $data = User::all()->toArray();

            return Excel::create('laravelcode', function($excel) use ($data) {
                $excel->sheet('mySheet', function($sheet) use ($data)
                {
                    $sheet->fromArray($data);
                });
            })->download($type);
        }
   public function user_change_status(Request $request)
    {
      
        $id = $request->input('user_id');
        $status = $request->input('activity');
        $user = User::find($id) ;
        $user->status  = $status;
        $effected=$user->save();
        
        $response = array();
        if($effected){
        $response['activity'] = true;
        $response['message']=" Status has been changed Successfully.";
          }else{
        $response['activity'] = false;
        $response['message']="Fail to change the user status.";
          }
        return response()->json($response, 200);

    }
    public function send()
    {
        $objDemo = new \stdClass();
        $objDemo->demo_one = 'Demo One Value';
        $objDemo->demo_two = 'Demo Two Value';
        $objDemo->sender = 'Loader';
        $objDemo->receiver = 'Shafqat Bhatti';
 
        Mail::to("shafqatghafoor99@gmail.com")->send(new RecieptMail($objDemo));
    }



}
