<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CaptainStateResource extends JsonResource
{
    public function toArray($request)
    {
        return [
                'captainstate' => $this->rating/$this->users,
            ];
    }
}
