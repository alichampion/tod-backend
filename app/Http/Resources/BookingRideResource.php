<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookingRideResource extends JsonResource
{
    
    public function toArray($request)
    {
        return [
                'id' => $this->id,
                'pickup_place' => $this->pickup_place!=null?$this->pickup_place:'',
                'booking_date' => $this->booking_date!=null?$this->booking_date:'',
                'vehical_sub_id' => $this->vehical_sub_id!=null?$this->vehical_sub_id:'',
                'vehical_cat_id' => $this->vehical_cat_id!=null?$this->vehical_cat_id:'',
                'amount' => $this->amount!=null?$this->amount:0,
                'created_at' => $this->created_at,
                'PaymentMethod' => $this->PaymentMethod!=null?$this->PaymentMethod:'Cash',
                'distance' => $this->distance!=null?$this->distance:0,
                'toll_plaza' => $this->toll_plaza!=null?$this->toll_plaza:0,
                'serge' => $this->serge!=null?$this->serge:0,
                'serge_charge' => $this->serge_charge!=null?$this->serge_charge:'',
                'loading_charge' => $this->loading_charge!=null?$this->loading_charge:'',
                'unloading_charge' => $this->unloading_charge!=null?$this->unloading_charge:'',
                'distance_charge' => $this->distance_charge!=null?$this->distance_charge:0,

                'loading_image' => URL('images/loading_image/').($this->loading_image!=null?'/'.$this->loading_image:'/no-image.png') ,
                'booking_destination' =>BookingDestinationResource::collection($this->BookingDestination),
                'rides' => RideBookingResource::collection($this->Rides),
                'vehicleSubCategorie'=>new VehicleSubCategoriesResource($this->vehicleSubCategorie),
                'good'=>new GoodResource($this->Good),

            ];

    }
}
