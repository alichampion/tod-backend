<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DataResource extends JsonResource
{
    public function toArray($request)
    {
        return [
                'id' => $this->id,
                'status' => $this->status!=null?$this->status:'',
                'pickup_place' => $this->pickup_place!=null?$this->pickup_place:'',
                'vehical_sub_id' => $this->vehical_sub_id!=null?$this->vehical_sub_id:'',
                'vehical_cat_id' => $this->vehical_cat_id!=null?$this->vehical_cat_id:'',
               // 'booking_date' => $this->booking_date,
                'amount' => $this->amount!=null?$this->amount:'',
                'distance' => $this->distance!=null?$this->distance:'',
                'estimated_time' => $this->distance!=null?$this->distance:'',              
    'booking_destination' =>BookingDestinationResource::collection($this->BookingDestination),
                'vehicleSubCategorie'=>new VehicleSubCategoriesResource($this->vehicleSubCategorie),
                'good'=>new GoodResource($this->Good),
                
                
            
            ];

    }
}
