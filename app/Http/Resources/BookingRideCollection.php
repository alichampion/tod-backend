<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BookingRideCollection extends ResourceCollection
{
    
    public function toArray($request)
    {
        return BookingRideResource::collection($this->collection); 
    }
}
