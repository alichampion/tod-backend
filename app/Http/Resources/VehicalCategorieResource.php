<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VehicalCategorieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'base_fare'=>$this->base_fare!=null?$this->base_fare:'',
            'amount_per_km' => $this->amount_per_km!=null?$this->amount_per_km:'' ,
            'destination_charge' => $this->destination_charge!=null?$this->destination_charge:'',
            'free_time' => $this->free_time!=null?$this->free_time:'' ,
            'loading_waiting_price' => $this->loading_waiting_price!=null?$this->loading_waiting_price:'',
            'unloading_waiting_price' => $this->unloading_waiting_price!=null?$this->unloading_waiting_price:'',

                       
        ];
    }
}
