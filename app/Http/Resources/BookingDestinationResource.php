<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookingDestinationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
    return [
                'id' => $this->id,
               
                'drop_place' => $this->drop_place!=null?$this->drop_place:'',
            ];
        }
}
