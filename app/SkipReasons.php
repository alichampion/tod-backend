<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkipReasons extends Model
{
     protected $fillable = ['reasons','status'];


     public function ride_complete()
    {
        
        return $this->hasMany(RidesCompleted::class ,'skip_reasons_id');
    }
}
