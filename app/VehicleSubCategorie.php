<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleSubCategorie extends Model
{
    protected $fillable = ['vehical_categorie_id' , 'type' ,'base_fare', 'name', 'vehicle_image' ,'status'];
   

     public function vehiclecategorie()
    {
        return $this->belongsTo(VehicalCategorie::class ,'vehical_categorie_id');
    }
    
}
