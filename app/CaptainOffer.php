<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaptainOffer extends Model
{
     protected $fillable = ['captain_id','offer_id' , 'amount'  , 'status' ];
}
