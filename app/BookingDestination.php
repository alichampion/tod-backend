<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingDestination extends Model
{
    protected $fillable = [
    	'booking_id', 'longitude', 'latitude', 'drop_place'
    ];

    protected $hidden = ['status'];

    public function booking()
    {
        return $this->belongsTo(Booking::class);
    
    }

     public function ride_complete()
    {
        return $this->hasMany(RidesCompleted::class);
    
    }
}
