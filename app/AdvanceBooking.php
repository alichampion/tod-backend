<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvanceBooking extends Model
{
    protected $fillable = ['goods_id', 'vehical_sub_id', 'pickup_longitude', 'pickup_latitude', 'ab_date', 'user_id'];

    protected $hidden = ['booking_status'];

     public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function vehical()
    {
        return $this->belongsTo(Vehical::class);
    }

    public function good()
    {
        return $this->belongsTo(Good::class);
    }

    public function Rides()
    {
        return $this->hasMany(Ride::class);
    }

    public function AdvanceBookingDesitination()
    {
        

         return $this->hasMany(AdvanceBookingDestination::class , 'advance_booking_id');
    }
}
