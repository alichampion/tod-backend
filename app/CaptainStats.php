<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaptainStats extends Model
{
    protected $fillable = ['rating', 'users','captain_id'];

   public function captain(){
        
        return $this->belongsTo(Captain::class ,  'captain_id');
    }
}
