<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RidesCompleted extends Model
{
    protected $fillable = ['image','complete_at','status' ,'ride_id','destination_id' ,'map_image' , 'unloading_waiting_time' , 'skip_reasons_id' , 'status'];


    public function ride(){
        
        return $this->belongsTo(Ride::class);
    }

    public function bookingDestination(){

    	return $this->belongsTo(BookingDestination::class, 'destination_id');
    }

    public function booking(){

    	return $this->belongsTo(Booking::class);
    }

    public function user()
    {
        return $this->belongsTo(Ride::class );
    }

    public function skipReasons()
    {
        return $this->belongsTo(SkipReasons::class );
    }
}
