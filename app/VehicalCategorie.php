<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicalCategorie extends Model
{
     protected $fillable = ['name' , 'weight', 'volume', 'status' , 'free_time' , 'loading_waiting_price' , 'unloading_waiting_price', 'amount_per_km', 'destination_charge','base_fare', 'image'];

    public function VehicleSubCategorie(){

        return $this->hasMany(VehicleSubCategorie::class , 'vehical_categorie_id');
    }
    
    // public function Vehical(){

    //     return $this->belongsTo(Vehical::class );
    // }

}
