<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = ['offer_name','time_period' , 'rides' ,'amount' , 'offer_type' ,'offer_expairy', 'description' , 'status' ,'start_date' , 'end_date' ];
}
