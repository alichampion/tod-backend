<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = [
        'rate' ,'user_id' , 'captain_id', 'feedback','ride_id' 
        ];

    protected $hidden = [];

    public function ride(){
    	return $this->belongsTo(Ride::class, 'ride_id');
    }
}
