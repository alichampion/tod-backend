<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Vehical;
class VehicalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('vehicals')->delete();
    	Vehical::create(['vehical_name'=>'Picup','status'=>1]);
    	Vehical::create(['vehical_name'=>'Truck','status'=>1]);
    	Vehical::create(['vehical_name'=>'Vigo','status'=>1]);
    }
}
