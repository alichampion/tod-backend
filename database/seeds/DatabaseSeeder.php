<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    
    public function run()
    {
        $this->call(AdminsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CaptainsTableSeeder::class);
        $this->call(GoodsTableSeeder::class);      
        $this->call(FinancialTableSeeder::class);
        // $this->call(VehicalsTableSeeder::class);
        $this->call(PermisionsTableSeeder::class);
       $this->call(RolesTableSeeder::class);
       
        
    }
}
