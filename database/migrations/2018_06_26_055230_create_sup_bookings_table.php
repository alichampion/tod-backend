<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sup_bookings', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');

           

            $table->integer('vehical_sub_id')->unsigned()->nullable();
            $table->foreign('vehical_sub_id')->references('id')->on('vehicle_sub_categories')->onDelete('set null')->onUpdate('cascade');
            

            $table->integer('good_id')->unsigned()->nullable();
            $table->foreign('good_id')->references('id')->on('goods')->onDelete('set null')->onUpdate('cascade');

           
            $table->string('vehical_cat_id');
            $table->string('pickup_longitude');
            $table->string('pickup_latitude');
            $table->string('pickup_place')->nullable();
            $table->string('booking_date')->nullable();
            $table->boolean('status')->default(1);
            $table->string('captain_id')->nullable();
            $table->string('old_booking_id')->default(0);

             $table->string('PaymentMethod')->nullable();
            $table->double('amount')->nullable();
            $table->string('distance')->nullable();
            $table->string('labour_loader')->nullable();
            $table->string('emergencies_reasons')->nullable();
            $table->string('labour_unloader')->nullable();
            $table->string('estimated_time')->nullable();
             $table->string('serge')->default(0);
            $table->string('serge_charge')->default(0);
            $table->string('loading_charge')->default(0);
            $table->string('unloading_charge')->default(0);
            $table->string('rejected_by')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sup_bookings');
    }
}
