<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerUserwallats extends Migration
{
    
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER `user_wallet_trigger` AFTER INSERT ON `users` FOR EACH ROW 
        BEGIN 
        INSERT INTO user_wallets ( `user_id`, `amount`, `debit_amount`, `credit_amount`, `status`, `created_at`, `updated_at`) VALUES(NEW.id,0,0,0,1,now(),now());
        END;
        ');
    }

    
    public function down()
    {
        DB::unprepared('DROP TRIGGER `user_wallet_trigger`');
    }
}
