<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatebookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');

           

            $table->integer('vehical_sub_id')->unsigned()->nullable();
            $table->foreign('vehical_sub_id')->references('id')->on('vehicle_sub_categories')->onDelete('set null')->onUpdate('cascade');
            $table->string('vehical_cat_id')->nullable();

            $table->integer('good_id')->unsigned()->nullable();
            $table->foreign('good_id')->references('id')->on('goods')->onDelete('set null')->onUpdate('cascade');

           

            $table->string('pickup_longitude');
            $table->string('pickup_latitude');
            $table->string('pickup_place')->nullable();
            $table->string('booking_date')->nullable();
            $table->boolean('status')->default(1);
            $table->string('toll_plaza')->default(0);

            $table->string('booking_type')->nullable();
            $table->string('old_booking_id')->default(0)->nullable();

             $table->string('PaymentMethod')->nullable();
            $table->double('amount')->default(0)->nullable();
            $table->string('distance')->default(0);
            $table->string('labour_loader')->nullable();
            $table->string('emergencies_reasons')->nullable();
            $table->string('labour_unloader')->nullable();
            $table->string('estimated_time')->nullable();
            $table->string('captain_id')->nullable();
            $table->string('rejected_by')->default(0);

            $table->string('serge')->default(0);
            $table->string('serge_charge')->default(0);
            $table->string('loading_charge')->default(0);
            $table->string('unloading_charge')->default(0);
            $table->string('distance_charge')->default(0);
            $table->string('reject_booking_image')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('bookings');
        Schema::enableForeignKeyConstraints();
    }
}
