<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
           $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->unique();           
            $table->string('phone')->unique()->nullable();
            $table->string('cnic')->unique()->nullable();
            $table->string('images')->default('no-image.png')->nullable();
            $table->string('password');
            $table->string('belongs')->nullable();
            $table->string('parrent_admin')->nullable();
            $table->string('api_token',60)->unique()->nullable();
            $table->integer('activity')->default(1);
            $table->boolean('status')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('admins');
        Schema::enableForeignKeyConstraints();
    }
}
