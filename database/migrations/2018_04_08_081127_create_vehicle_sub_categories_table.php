<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleSubCategoriesTable extends Migration
{
  
    public function up()
    {
        Schema::create('vehicle_sub_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();

            $table->integer('vehical_categorie_id')->unsigned()->nullable();
            $table->foreign('vehical_categorie_id')->references('id')->on('vehical_categories')->onDelete('set null')->onUpdate('cascade');
            $table->string('vehicle_image')->nullable();

          
            
            $table->boolean('status')->default(1);
            $table->timestamps();    
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('vehicle_sub_categories');
    }
}
