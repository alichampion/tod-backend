<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRidesTable extends Migration
{
    
    public function up()
    {
        Schema::create('rides', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
            $table->integer('booking_id')->unsigned()->nullable();
            $table->foreign('booking_id')->references('id')->on('bookings')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('captain_id')->unsigned()->nullable();
            $table->foreign('captain_id')->references('id')->on('captains')->onDelete('set null')->onUpdate('cascade');
             $table->string('rejected_by')->nullable();
            $table->boolean('status')->default(1);
            $table->string('loading_waiting_time')->nullable();
            $table->string('loading_image')->nullable();
            $table->string('loading_time')->nullable();
            $table->string('arrival_time')->default(0);
            $table->string('ride_start_time')->default(0);
            $table->string('ride_end_time')->default(0);
            $table->string('total_ride_time')->default(0);

            $table->timestamps();
        });
    }


    public function down()
    {
        
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('rides');
        Schema::enableForeignKeyConstraints();
    }
}
   