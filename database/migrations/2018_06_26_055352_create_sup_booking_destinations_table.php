<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupBookingDestinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sup_booking_destinations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('sup_booking_id')->unsigned();
            $table->foreign('sup_booking_id')->references('id')->on('sup_bookings')->onDelete('cascade')->onUpdate('cascade');

            $table->string('longitude');
            $table->string('latitude');
            $table->string('drop_place')->nullable();
            $table->boolean('status')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sup_booking_destinations');
    }
}
