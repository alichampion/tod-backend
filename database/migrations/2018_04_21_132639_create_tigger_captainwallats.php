<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiggerCaptainwallats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         DB::unprepared('
        CREATE TRIGGER `captain_wallet_trigger` AFTER INSERT ON `captains` FOR EACH ROW 
        BEGIN 
        INSERT INTO captain_wallets ( `captain_id`, `amount`, `debit_amount`, `credit_amount`, `status`, `created_at`, `updated_at`) VALUES(NEW.id,0,0,0,1,now(),now());
        END;
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       DB::unprepared('DROP TRIGGER `captain_wallet_trigger`');
    }
}
