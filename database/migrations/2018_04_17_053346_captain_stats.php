<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CaptainStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('captain_stats', function (Blueprint $table){
            $table->increments('id');
            $table->integer('captain_id')->unsigned()->nullable();
            $table->foreign('captain_id')->references('id')->on('captains');
            $table->double('rating');
            $table->double('users');
            $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('captain_stats');
    }
}
