<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRejectBookingRecordsTable extends Migration
{
   
    public function up()
    {
        Schema::create('reject_booking_records', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id')->unsigned()->nullable();
            $table->foreign('booking_id')->references('id')->on('bookings')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('captain_id')->unsigned()->nullable();
            $table->foreign('captain_id')->references('id')->on('captains')->onDelete('set null')->onUpdate('cascade');
            $table->integer('ride_id')->unsigned()->nullable();
            $table->foreign('ride_id')->references('id')->on('rides')->onDelete('set null')->onUpdate('cascade');
            
            $table->boolean('status')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reject_booking_records');
    }
}
