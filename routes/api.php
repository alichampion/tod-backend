<?php

use Illuminate\Http\Request;

Route::group(['prefix' => 'v1'],function() {
    // Route::post('/login', ['middleware' => 'guest:api', 'uses' => 'Auth\ApiLoginController@login']);
    // For User
    Route::get('activity_update_cron', 'Api\ApisController@activity_update_cron');
    Route::group(['prefix' => 'user'], function () {
        Route::post('/register', 'Auth\ApiUserController@register');
        Route::post('/validate_user', 'Auth\ApiUserController@register_validate');
        Route::post('/login', 'Auth\ApiUserController@login');
        Route::post('/forgot-password', 'Api\ApiUserController@get_user_by_phone');

        Route::group(['middleware' => ['auth:api']], function () {
            Route::post('/logout', 'Auth\ApiUserController@logout');
            Route::post('/verify', 'Auth\ApiUserController@verify');
            Route::post('/change-password', 'Api\ApiUserController@changepassword');

            Route::get('/get-location', 'Api\ApiUserController@get_captain_location')->middleware('throttle:999999,1');
            Route::post('/update-location', 'Api\ApiUserController@update_user_location')->middleware('throttle:999999,1');

            Route::post('/booking', 'Api\ApiBookingController@user_booking')->middleware('throttle:999999,1');
            Route::post('/booking-rejected', 'Api\ApiBookingController@booking_reject_request')->middleware('throttle:999999,1');
            Route::post('/active-captains-vehical', 'Api\ApiVehicalContoller@get_active_vehical_captain')->middleware('throttle:999999,1');
            Route::post('/rating', 'Api\ApiCaptainController@captain_rating')->middleware('throttle:999999,1');
            Route::post('/user-update', 'Api\ApiUserController@userProfile')->middleware('throttle:999999,1')->middleware('throttle:999999,1');
            Route::post('/user-profile', 'Api\ApiUserController@get_profile_image')->middleware('throttle:999999,1')->middleware('throttle:999999,1')->middleware('throttle:999999,1');
            Route::post('/loading-time', 'Api\ApiBookingController@update_loading_time');
            Route::post('/update-password', 'Api\ApiUserController@update_user_password');
            Route::post('/user_ride_cancel', 'Api\ApiUserController@user_ride_cancel');
            Route::get('/user-history', 'Api\ApiBookingController@user_ride_history');
            Route::post('/map-detail', 'Api\ApiUserController@captaindetailformap');
            Route::post('/ride-detail', 'Api\ApiRideController@get_ride_detail');
            Route::post('/user_reject_request', 'Api\ApiRideController@user_reject_request');
            Route::get('/userwallet', 'Api\ApiFinanceController@userwallet');

            Route::get('/user_ride_reject', 'Api\ApiUserRideHistoryController@user_ride_reject');
            Route::get('/user_ride_completed', 'Api\ApiUserRideHistoryController@user_ride_completed');
            Route::get('/user_ride_current', 'Api\ApiUserRideHistoryController@user_ride_current');

            Route::post('/complaint', 'Api\ApiHelpController@add_complaint');
            Route::post('/without-destination-booking', 'Api\ApiBookingController@without_destination_booking')->middleware('throttle:999999,1');
            Route::post('/user_update_account', 'Api\ApiUserController@user_update_account');
        });

    });


    // For Captain
    Route::group(['prefix' => 'captain'], function () {
        Route::post('/register', 'Auth\ApiCaptainController@register');
        Route::post('/login', 'Auth\ApiCaptainController@login');
        Route::post('/forgot-password', 'Api\ApiCaptainController@forgot_password');
        Route::put('update/{captain}', 'Api\ApiCaptainController@update');

        Route::group(['middleware' => ['auth:captainapi']], function () {
            Route::post('/verify', 'Auth\ApiCaptainController@verify');
            Route::post('/logout', 'Auth\ApiCaptainController@logout');
            Route::post('/update-location', 'Api\ApiCaptainController@update_captain_location')->middleware('throttle:999999,1');
            Route::get('/get-location', 'Api\ApiCaptainController@get_captain_location')->middleware('throttle:999999,1');
            Route::get('/get-location-by-id', 'Api\ApiCaptainController@get_captain_location_by_id')->middleware('throttle:999999,1');
            Route::get('/captainwallet', 'Api\ApiFinanceController@captainwallet');
            Route::get('/update-activity-status', 'Api\ApiCaptainController@update_captain_activity');
            Route::get('/get-activity-status', 'Api\ApiCaptainController@get_captain_activity');
            Route::post('/update-password', 'Api\ApiCaptainController@update_captain_password');
            Route::get('/history', 'Api\ApiBookingController@captain_ride_history');
            Route::get('/captain_ride_reject_history', 'Api\ApiBookingController@captain_ride_reject_history');
            Route::post('/user-detail', 'Api\ApiRideController@user_detail_for_captain');
            Route::post('/arrival', 'Api\ApiRideController@arrival');
            Route::post('/captain_reject_request', 'Api\ApiRideController@captain_reject_request');
            Route::post('/captain_reject_booking', 'Api\ApiCaptainController@captain_reject_booking');

            Route::post('/labour-update', 'Api\ApiCaptainController@update_captain_labour');
            Route::post('/ride-start', 'Api\ApiRideController@ride_start');
            Route::post('/captain-update', 'Api\ApiCaptainController@captainProfile');
            Route::post('/captain-profile', 'Api\ApiCaptainController@get_profile_image');
            Route::post('/captain-loading', 'Api\ApiBookingController@captain_loading_time');
            Route::get('/accidental-causion', 'Api\ApiHelpController@accidental_causion');
            Route::post('/user_rating', 'Api\ApiHelpController@user_rating');
            Route::get('/skip-reason', 'Api\ApiSkipReasonController@skips_reasons');
            Route::post('/skip-destinatin', 'Api\ApiSkipReasonController@skipdestination');
            Route::post('/captain-booking', 'Api\ApiBookingController@captain_booking')->middleware('throttle:999999,1');
            Route::post('/coustom-ride-proof', 'Api\ApiRideController@coustom_ride_proof')->middleware('throttle:999999,1');

            Route::post('/custom-fare-detail', 'Api\ApiRideController@custom_fare_detail')->middleware('throttle:999999,1');
            Route::post('/skip_destination_notify', 'Api\ApiHelpController@skip_destination_notify');
            Route::post('/unloading_notification', 'Api\ApiTrackingController@unloading_notification');
            Route::post('/captain_ride_start', 'Api\ApiTrackingController@captain_ride_start');
        });

    });

    // General APis
    Route::get('/vehicals-categories', 'Api\ApiVehicalContoller@get_all_vehicals_categories');
    Route::post('/vehicle-sub-category', 'Api\ApiVehicalContoller@get_all_vehicals');
    Route::get('/vehicals/{id}', 'Api\ApiVehicalContoller@get_vehical_by_id');
    Route::post('/tracking/store', 'Api\ApiTrackingController@store');
    Route::post('/tracking/show', 'Api\ApiTrackingController@show');
    Route::get('/goods', 'Api\ApiGoodController@get_all_goods');
    Route::get('/goods/{id}', 'Api\ApiGoodController@get_goods_by_id');
    Route::post('/job-details', 'Api\ApiBookingController@get_job_data');
    Route::post('/ride-accept', 'Api\ApiRideController@ride_accept');
    Route::post('/ride-complete', 'Api\ApiRideController@ride_complete')->middleware('throttle:999999,1');
    Route::post('/delivery', 'Api\ApiRideController@deliveryproof');
    Route::post('/deliveryimage', 'Api\ApiRideController@proofImage');
    Route::post('/custom_proofImage', 'Api\ApiRideController@custom_proofImage');
    Route::post('/fileupload', 'Api\ApiRideController@fileupload');
    Route::post('/fare-detail', 'Api\ApiBookingController@fareAccount')->middleware('throttle:999999,1');
    Route::get('/estimate-fare', 'Api\ApiBookingController@userfare')->middleware('throttle:999999,1');
    Route::post('/transaction', 'Api\ApiFinanceController@commission_detect')->middleware('throttle:999999,1');
    Route::post('/custom_ride_complete', 'Api\ApiHelpController@custom_ride_complete');

    Route::post('/loading_notification', 'Api\ApiTrackingController@loading_notification');
    Route::post('/offer_done', 'Api\ApiVehicalContoller@offer_done');
});

