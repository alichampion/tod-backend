@extends('layout.admin.headerAdmin')
@section('content')
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-truck" style="color: #fff;"></i>
                                        <span class="caption-subject font-red sbold uppercase">Goods Categories</span>
                                    </div>
                                   
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="btn-group"> 
                                                    <a href="{{route('good-categories.create')}}" class="btn green"> Add New
                                                        <i class="fa fa-plus"></i>
                                                    </a href="{{route('good-categories.create')}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="btn-group pull-right">

                                                    

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            
                                        </div>
                                        
                                    </div>
                                    <div class="table-responsive">
                                    <table class="table table-striped table-hover table-bordered" id="category">
                                       <thead>
                                            <tr>
                                             <th align="left">
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                                        <span></span>
                                                    </label>
                                                </th>
                                                <th> Categories Name </th>
                                                <th> Status </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        
                                    </table>
                                    </div>
                                    
                                </div>

                                
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
               

                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
          
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
             <div class="modal fade" id="add_new_modal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Modal Header</h4>
                        </div>
                    <div class="modal-body">
                        <div class="tab-pane active" id="tab_0">
                                <div class="portlet box green">
                                          <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Categories Add  
                                                </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                    <a href="javascript:;" class="reload"> </a>
                                                    <a href="javascript:;" class="remove"> </a>
                                                </div>
                                          </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="{{ route('good-categories.store') }}" class="form-horizontal" method="POST">
                                                        {{ csrf_field() }}
                                                                <div class="form-body">
                                                                    <div class="form-group">
                                                                        <label class="col-md-3 control-label" style="font-size: 13px;"> categories Name</label>
                                                                        <div class="col-md-4">
                                                                            <input type="text" class="form-control input-circle" placeholder="Categories" name="name" required>
                                                                            
                                                                        </div>
                                                                    </div>


               
                                                                </div> 
                                                                
                                                                <div class="form-actions">
                                                                    <div class="row">
                                                                        <div class="col-md-offset-3 col-md-9">
                                                                            <button type="submit" class="btn btn-circle green">Submit</button>
                                                                            <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                     </form>
                                                </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                </div>
                        </div>
                   </div>
                </div>   
             </div>
           </div>  
            
          
 @endsection       <!-- BEGIN FOOTER -->

@section('styles')
    @parent
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('assets/apps/css/stylebutton.css') }}" rel="stylesheet" type="text/css" />
     <link href="{{ asset('assets/apps/css/jquery.dataTables.css') }}" rel="stylesheet" type="text/css" />
     
     <style type="text/css">
         td a {
            float: left;
         }
     </style>
@endsection

@section('javascripts')
@parent


<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js" type="text/javascript"></script>




<script>
            $('#category').DataTable( {
                "processing": true,
                "serverSide": true,

                Length:10,
                ajax: {
                    "url":"<?= route('get_cat') ?>",
                    "dataType":"json",
                    "type":"POST"
                },

                columns:[
                    {"data":"id","render":function(id){
                       return  '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"> <input type="checkbox" class="checkboxes" value="'+id+'" />  <span></span></label>'; 
                   },"searchable":false,"orderable":false},
                    
                    {"data":"name"},
                    
                    {"data":"status","render":function(status,type,row){
                        return status?'<a href="javascript:;" onclick="change_status(this,'+row.id+',0)" class="btn btn-success"> Active </a>':'<a href="javascript:;" onclick="change_status(this,'+row.id+',1)" class="btn btn-danger"> Deactive </a>';
                    }},
                    {"data":"id","render":function(id){
                        confirmInit();
                        var edit_route = '{{ route("good-categories.edit", ":id") }}';
                            edit_route = edit_route.replace(':id', id);

                             var delete_route = '{{ route("good-categories.destroy", ":id") }}';
                                delete_route = delete_route.replace(':id', id);

          var buttons = '<a href="'+edit_route+'" class="btn btn-warning btn-xs">Edit</a>';

          buttons +='<a href="javascript:;" data-toggle="confirmation" data-placement="left" onclick="delete_row(this,'+id+')" data-action="'+delete_route+'" class="btn btn-danger btn-xs">Delete</a>';

          return buttons;
                    },"searchable":false,"orderable":false}
                ]

                
            } );
        </script>
 <script type="text/javascript">
            
      function change_status(obj,id,status) {
             
          $.ajax({
            url: "{{ route('cat-change-status') }}", 
            method:"POST",
            data:{'user_id':id,'status':status},
            success: function(response){
                if(response.status){
                if(status)
                $(obj).replaceWith('<a href="javascript:;" onclick="change_status(this,'+id+',0)" class="btn btn-success"> Active </a>');
                else
                $(obj).replaceWith('<a href="javascript:;" onclick="change_status(this,'+id+',1)" class="btn btn-danger"> Deactive </a>');

                 toastr.success(response.message);

                }else{
                 toastr.error(response.message);

                }
           }});

            }

        </script>



<!-- <script type="text/javascript">
    $(document).ready(function() {
    $('#captain').DataTable( {
        dom: 'Bfrtip',

        "paging": false,
        "bInfo": false,
        "searching": false,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script> -->
<script>
$(document).ready(function(){
    $('.wallet').tooltip({title: "Wallet"}); 
    $('.show').tooltip({title: "show!"}); 
    $('.edit').tooltip({title: "Edit"});
    $('.pdf').tooltip({title: "report in pdf"});
});
</script>



<script type="text/javascript">
var action_url ;
var user_id ;
var row_obj;
function delete_row(obj,id) {
        action_url = $(obj).attr('data-action');
        user_id = id;
        row_obj = obj;
          confirmInit();
       $(obj).on('confirmed.bs.confirmation', function () {
         
          $.ajax({
            url: action_url, 
            method:"DELETE",
            data:{'id':user_id},
            success: function(response){
                if(response.status){
                    $(row_obj).parent().parent().remove();
                }
                else{
                    console.log(response.message);
               }
           }});
        });

       

    }    
    
function delete_action() {
var r = confirm("Are you sure you want to delete this!");
if (r == true) {
   return true;
} else {
   
       return false;
}
    }



</script>

@endsection


