<?php
/**
 * Project: hajjtrack.
 * User: naveed
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')


<style type="text/css">
  .m-portlet__body{
  overflow: hidden;
}
</style>

<div class="m-portlet m-portlet--mobile">


        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                    Financials
                    </h3>
                </div>
            </div>
        </div>
        @component('_components.alerts-default')
        @endcomponent

      <div class="m-portlet__body"> 
        <div class="col-sm-12 col-md-12 main" id="show_ledger">
          <div class="row" >
            <div class="col-sm-10 col-md-10" style="padding-left:0px;">
                <h1 class="page-header">Financial Report</h1>
            </div>
          </div>
          <div class="row">
            @if(isset($errors) && $errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            @if(Session::has('danger'))
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <strong>Error!</strong>  {{ Session::get('danger') }}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <strong>Success!</strong>  {{ Session::get('success') }}
                </div>
            @endif

            @if(Session::has('warning'))
                <div class="alert alert-warning" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <strong>Warning!</strong> {{ Session::get('warning') }}
                </div>
            @endif
          </div>
        
          <div class="row">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                  <thead>
                    <tr>
                      <th align="left">
                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="float: right;">
                          <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes">
                          <span></span>
                        </label>
                      </th>
                    <th>Code</th>
                    <th>Account Title</th>
                    <th>D/C</th>
                    <th>Opening Balance</th>
                    <th>Dabit</th>
                    <th>Credit</th>
                    <th>Balance</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $dabit_amont=0;
                    $credit_total = 0;
                    $categories_id=1;
                    $categories_amount =0;
                    $balance_cr='';
                    // $total_balance=$dabit_amont - $credit_total;
                    ?>
                    @if(isset($records) && !empty($records))
                    @foreach ($records as $posts)
                      
                      
                    <tr>

                      <td>
                        <input type="checkbox" name="">
                      </td>
                      <td>{{$posts->code}} </td>
                      <td> {{$posts->title}}  </td>
                      <td>  @if($posts->dabit > $posts->credit) DB @else CR @endif </td>
                       <?php $categories_amount += $posts->dabit;  $categories_amount -= $posts->credit;  ?>
                      <td> @if($posts->opening_balance){{$posts->opening_balance}} @else 0 @endif</td>
                      <td > @if($posts->dabit){{$posts->dabit}} @else 0 @endif  </td>
                      <td > @if($posts->credit){{$posts->credit}} @else 0 @endif</td>

                       @if( $categories_id < $posts->account_cat_id)
                      <td> 
                        @if($categories_amount> 0){{$categories_amount}} 
                        @else {{-($categories_amount)}}@endif  <span class="pull-right">@if($categories_amount> 0){{'DB'}} @else {{'CR'}}@endif</span>
                      </td>
                        <?php $categories_amount=0; $categories_id= $posts->account_cat_id; ?>
                      @else
                      <td></td>
                      @endif
                      
                    </tr>
                    @endforeach
                    @else
                    <tr>
                      <td colspan="10" class="alert alert-info">No Record Found..</td>
                    </tr>
                    @endif

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        <br>
        <br>
        <div class="sec_1">
          <div class="row">
            <div class="col-md-12">
              <div class="col-xl-6 col-lg-8 col-md-8 col-sm-8 pull-right">
                <div class="col-md-3" style="float: left;">
                  <label for="exampleInputEmail2" style="font-weight: bold; color: #337ab7;">Dabit:</label>
                  <br>
                  <input type="text" name="" value="{{$dabit_amount}}" placeholder="00.00" style="width: 75px;;background: #5bc0de;">
                </div>
                <div class="col-md-3" style="float: left;">
                  <label for="exampleInputEmail2" style="font-weight: bold; color: #337ab7;margin-top: -4px;">Credit:</label>
                  <br>
                  <input type="text" name="" value="{{$credit_amount}}" placeholder="00.00" style="width: 75px;;background: #5bc0de;">
                </div>
                <div class="col-md-3" style="float: left;">
                  <label for="exampleInputEmail2" style="font-weight: bold; color: #337ab7;">Total:</label>
                  <br>
                  <input type="text" name="" value="@if($dabit_amount>$credit_amount) {{$dabit_amount-$credit_amount}}@else {{$credit_amount -$dabit_amount}}@endif" placeholder="00.00" style="width: 75px;;background:blue;color: white;">
                </div>
                <div class="col-md-3" style="float: left;">
                  <br>
                  <input type="text" name="" value="@if($dabit_amount>$credit_amount){{'DABIT'}}  @else {{'CREDIT'}}@endif" placeholder="00.00" style="width: 75px;;background: green;margin-top: 7px; color: white;">
                </div>
              </div>
              <div class="col-xl-6 col-lg-4 col-md-4 col-sm-4">
                <div class="col-md-2" style="float: left;">
                  
                </div>
                <div class="col-md-2" style="float: left;">
                  <br>
                  
                </div>
                <div class="col-md-3" style="float: left;">
                 
                </div>
                <div class="col-md-1" style="float: left;">
                  
                </div>
                <div class="col-md-2" style="float: left;">
                  
                </div>
                <div class="col-md-2" style="float: left;">
                 
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>


  @endsection

  @push('post-styles')
   <link href="{{asset('js/bootstrap-datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('js/bootstrap-datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
  @endpush

  @push('post-scripts')


<script src="{{asset('js/bootstrap-datatable/js/jquery.dataTables.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/dataTables.bootstrap4.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/dataTables.buttons.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/buttons.bootstrap4.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/jszip.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/pdfmake.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/vfs_fonts.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/buttons.html5.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/buttons.print.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/buttons.colVis.min.js')}}"></script>

   <script>
    $(document).ready(function() {
     //Default data table
      $('#default-datatable').DataTable();


      var table = $('#example').DataTable( {
       lengthChange: false,
       buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
     } );

    table.buttons().container()
       .appendTo( '#example_wrapper .col-md-6:eq(0)' );

     } );

   </script>
        

      @endpush