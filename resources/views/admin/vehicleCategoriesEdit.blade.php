<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                <div class="portlet box box_shadow_form">
                   <div class="portlet-title head_21" style="padding: 20px!important;" >              
                       <i class="fa fa-key" style="color: #fff;"></i>
                       <span class="caption-subject  sbold uppercase" style="color: #fff; font-size: 20px!important;">  Vehicle categories Edit</span>
                   </div>
                   <div class="portlet-body form">
                    <!-- BEGIN FORM-->  
                    {!! Form::open(['method' => 'PATCH' ,'files'=>true,  'route' => ['vehical-categories.update',$Datatypes->id], 'class'=>'form-horizontal']) !!}

                    <div class="form-body">
                             <div class="row">
                             <div class="col-md-4">
                                 <div class="fileinput fileinput-new" data-provides="fileinput" >
                                    <div class="fileinput-new thumbnail" style="max-width:250px!important; height: 250px!important">
                                       <img src="{{asset('images/vehicle_categories/' . $Datatypes->image)}}" id="blah" height="250px" width = "250px">
                                          </div>                                                                       
                                           </div>
                                       <div class="form-group" >
                                        <label for="image" class="btn btn-primary">Upload Profile</label>
                                        <input type="file" id="image" name="image" class="hide" style="opacity: 0;">
                                    </div>
                                        @if ($errors->has('image'))
                                        <div class="alert alert-danger" role="alert">
                                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                              <span aria-hidden="true">×</span>
                                              <span class="sr-only">Close</span>
                                          </button>
                                          <strong>Warning!</strong> {{$errors->first('image')}}
                                      </div>
                                      @endif
                               
                             </div>

                         <div class="col-md-8">
                          <div class="form-group">
                            <label class="col-md-6 control-label">Vehical Categories Name</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control input-circle"  name="name" value="{{ $Datatypes->name }}">

                            </div>
                              </div>

                        <div class="form-group">
                            <label class="col-md-6 control-label"> weight</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control input-circle"  name="weight" value="{{ $Datatypes->weight}}">

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-6 control-label"> volume </label>
                            <div class="col-md-12">
                                <input type="text" class="form-control input-circle"  name="volume" value="{{ $Datatypes->volume}}">

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> Base Fare</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control input-circle"  value="{{ $Datatypes->base_fare}}" name="base_fare" >

                            </div>
                        </div>
                         </div>
                       
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-6 control-label">Amount /Km </label>
                           
                                <input type="text" class="form-control input-circle"  value="{{ $Datatypes->amount_per_km}}" name="amount_per_km" >

                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-6 control-label"> Destination Charge </label>
                             
                                <input type="text" class="form-control input-circle" value="{{ $Datatypes->destination_charge}}"  name="destination_charge" >

                            </div>
                        </div>
                       <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-6 control-label"> Free Wait min</label>
                             
                                <input type="text" value="{{ $Datatypes->free_time}}"  class="form-control input-circle"  name="free_time" >

                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-6 control-label"> Loading waiting min </label>
                           
                                <input type="text" value="{{ $Datatypes->loading_waiting_price}}" class="form-control input-circle"  name="loading_waiting_time" >

                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-6 control-label"> Unloading Waiting min </label>
                            
                                <input type="text" value="{{ $Datatypes->unloading_waiting_price}}" class="form-control input-circle"  name="unloading_waiting_time" >

                            </div>
                        </div>

                      
                               




   <div class="col-sm-12" style="margin-top: 10px;">
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn btn-info ">Submit</button>
                         {!! Form::close() !!}
                            <a  href="{{route('vehical-categories.index')}}" class="btn btn-success  btn-outline">cancel</a>
                        
                    </div>
                </div>
            </div>  
        </div>
                 
                  <!-- END FORM-->
              </div>
    
</div>
</div>

</div>
</div>
</div>
</div>

@endsection
@stack('post-styles')
<link href="{{asset('assets/global/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
<style>
.head_21{padding: 20px 10px;background-color:#f26727;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey; border-bottom: 1px solid #f26727!important;
} 
.head_color{background-color:#f26727; padding: 10px; font-size: 20px!important; font-weight: bold; color: #fff!important; width: 95%;

}
</style>
@push('post-scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


<script type="text/javascript">
    function readURL(input) {

        if (input.files) {
            var reader = new FileReader();
            var totalfiles = input.files.length;
            for(i = 0; i < totalfiles; i ++){
                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    }
    $("#image").change(function() {
        readURL(this);
    });
</script>
<script type="text/javascript">
    $(function() {
                        // Multiple images preview in browser
                        var imagesPreview = function(input, placeToInsertImagePreview) {

                            if (input.files) {
                                var filesAmount = input.files.length;

                                for (i = 0; i < filesAmount; i++) {
                                    var reader = new FileReader();

                                    reader.onload = function(event) {
                                        $($.parseHTML('<img height = "250" width = "250" style = "margin:0px 10px 10px 0px;" >')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                                    }

                                    reader.readAsDataURL(input.files[i]);
                                }
                            }

                        };

                        
                    });
                </script>

                @endpush  


