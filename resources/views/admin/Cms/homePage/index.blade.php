<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                       <div class="portlet-title head_21">
                        <div class="caption">
                            <i class="fa fa-truck" style="color: #fff;"></i>
                            <span class="caption-subject font-red sbold uppercase dd_1">Drive Section</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{ route('drive-section.create')}}" id="sample_editable_1_new" class="btn btn-success  btn-sm"> Add New
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered responsive_class" id="captain">
                               <thead>
                                <tr>
                                    <th align="left">
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                            <span></span>
                                        </label>
                                    </th>
                                    <th>#id</th>
                                    <th> image </th>
                                    <th> content </th>
                                    <th>Action </th>
                                </tr>
                            </thead>

                            <tbody>

                             @foreach ($drive as $i) 
                             <tr>

                                <td align="left">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="checkboxes" value="1" />
                                        <span></span>
                                    </label>
                                </td>


                                <td> {{ $i->id }}   </td>
                                <td> <img src="{{asset('public/drive/' . $i->drive_image)}}" alt="admin image" class="logo-default" style="width: 40px; height: 40px; margin-top: 0px;" >
                                </td>
                                <td>{{$i->drive_content}}</td>
                                <td class="pull-right"> 
                                    <a href="{{ route('drive-section.edit', $i['id']) }}"><button class="btn edit btn-warning btn_color btn-xs"  data-title="Edit" data-toggle="modal" data-target="#editModal" data-placement="top" rel="tooltip" title="edit"><span class="glyphicon glyphicon-pencil">Edit</span></button></a>

                                    <span  style="display: inline-block;">
                                        {{ Form::open(['method' => 'DELETE', 'route' => ['drive-section.destroy', $i->id]]) }}
                                        {{ Form::hidden('id', $i->id) }}
                                        <button class="btn btn-danger btn-xs" onclick="return delete_action()"  data-title="Delete" data-toggle="modal" data-target="#delete" data-placement="top" rel="tooltip"><span class="glyphicon glyphicon-trash">Delete</span></button>
                                        {!! Form::close() !!}
                                    </span>

                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
        <!-- loader for  bussiness Data start -->
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-truck" style="color: #fff;"></i>
                    <span class="caption-subject font-red sbold uppercase dd_1">loader for bussiness Section</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="{{ route('Bussiness-section.create')}}" id="sample_editable_1_new" class="btn btn-success btn-sm"> Add New
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered responsive_class" id="captain">
                       <thead>
                        <tr>
                            <th align="left">
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                    <span></span>
                                </label>
                            </th>
                            <th>#id</th>
                            <th> image </th>
                            <th> content </th>
                        
                            <th>Action </th>
                        </tr>
                    </thead>

                    <tbody>

                     @foreach ($bussiness as $i) 
                     <tr>

                        <td align="left">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                <input type="checkbox" class="checkboxes" value="1" />
                                <span></span>
                            </label>
                        </td>


                        <td> {{ $i->id }}   </td>
                        <td> <img src="{{asset('bussiness/image/' . $i->bussiness_image)}}" alt=" image" class="logo-default" style="width: 40px; height: 40px; margin-top: 0px;" >
                        </td>
                        <td>{{$i->bussiness_content}}</td>


                        <td class="pull-right" style="width: 167px;">                           
                            <a href="{{ route('Bussiness-section.edit', $i['id']) }}"><button class="btn edit btn_color btn-warning btn-xs"  data-title="Edit" data-toggle="modal" data-target="#editModal" data-placement="top" rel="tooltip" title="edit"><span class="glyphicon glyphicon-pencil">Edit</span> </button></a>
                            <span  style="display: inline-flex;">
                                {{ Form::open(['method' => 'DELETE', 'route' => ['Bussiness-section.destroy', $i->id]]) }}
                                {{ Form::hidden('id', $i->id) }}
                                <button class="btn btn-danger btn-xs" onclick="return delete_action()"  data-title="Delete" data-toggle="modal" data-target="#delete" data-placement="top" rel="tooltip"><span class="glyphicon glyphicon-trash">Delete</span> </button>
                                {!! Form::close() !!}
                            </span>
                        </td>


                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
        
    </div>
</div>
<!-- loader for  bussiness Data end -->

<!-- loader for  Companies Data start -->
<div class="portlet light portlet-fit bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-industry" style="color: #fff;"></i>
            <span class="caption-subject font-red sbold uppercase dd_1">Companies Information Section</span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a href="{{ route('companies-section.create')}}" id="sample_editable_1_new" class="btn btn-success btn-sm pull-right"> Add New
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>

            </div>

        </div>
        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered responsive_class" id="captain">
               <thead>
                <tr>
                    <th align="left">
                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                            <span></span>
                        </label>
                    </th>
                    <th>#id</th>
                    <th> image </th>
                    <th> Email </th>
                    <th> phone </th>
                    <th> address </th>
                    <th>description</th>
                    <th>Action </th>
                </tr>
            </thead>

            <tbody>

             @foreach ($companies as $i) 
             <tr>

                <td align="left">
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                        <input type="checkbox" class="checkboxes" value="1" />
                        <span></span>
                    </label>
                </td>


                <td> {{ $i->id }}   </td>
                <td> <img src="{{asset('companies/image/' . $i->images)}}" alt=" image" class="logo-default" style="width: 40px; height: 40px; margin-top: 0px;" >
                </td>
                <td>{{$i->email}}</td>
                <td>{{$i->phone}}</td>
                <td>{{$i->address}}</td>
                <td>{{$i->description}}</td>


                <td class="pull-right"> 
                    <a href="{{ route('companies-section.edit', $i['id']) }}"><button class="btn edit btn-warning btn_color btn-xs"  data-title="Edit" data-toggle="modal" data-target="#editModal" data-placement="top" rel="tooltip" title="edit"><span class="glyphicon glyphicon-pencil">Edit</span></button></a>

                    <span  style="display: inline-block;">
                        {{ Form::open(['method' => 'DELETE', 'route' => ['companies-section.destroy', $i->id]]) }}
                        {{ Form::hidden('id', $i->id) }}
                        <button class="btn btn-danger btn-xs btn-xs1" onclick="return delete_action()"  data-title="Delete" data-toggle="modal" data-target="#delete" data-placement="top" rel="tooltip"><span class="glyphicon glyphicon-trash">Delete</span></button>
                        {!! Form::close() !!}
                    </span>

                </td>
            </tr>
            @endforeach

        </tbody>
    </table>
</div>

</div>
</div>
<!-- loader for  Companies Data end -->
<!-- loader for  News Data start -->
<div class="portlet light portlet-fit bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-newspaper-o" style="color: #fff;"></i>
            <span class="caption-subject font-red sbold uppercase">News Section</span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a href="{{ route('news-section.create')}}" id="sample_editable_1_new" class="btn btn-sm btn-success pull-right">Add New
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>

            </div>

        </div>
        <div class="table-responsive">
                <table class="table table-striped table-hover table-bordered responsive_class" id="captain">
                   <thead>
                    <tr>
                        <th align="left">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                <span></span>
                            </label>
                        </th>
                        <th>#id</th>
                        <th> image </th>
                        <th> content </th>
                        <th>Action </th>
                    </tr>
                </thead>

                <tbody>

                 @foreach ($news as $i) 
                 <tr>

                    <td align="left">
                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input type="checkbox" class="checkboxes" value="1" />
                            <span></span>
                        </label>
                    </td>


                    <td> {{ $i->id }}   </td>
                    <td> <img src="{{asset('Cms/news_image/' . $i->news_image)}}" alt=" image" class="logo-default" style="width: 40px; height: 40px; margin-top: 0px;" >
                    </td>
                    <td>{{$i->news_content}}</td>

                    <td style="display: inline-flex;"> 
                        <a href="{{ route('news-section.edit', $i['id']) }}"><button class="btn edit btn-warning btn_color btn-xs"  data-title="Edit" data-toggle="modal" data-target="#editModal" data-placement="top" rel="tooltip" title="edit"><span class="glyphicon glyphicon-pencil">Edit</span></button></a>
                        <br>

                        <span   style="display: inline-block;">
                            {{ Form::open(['method' => 'DELETE', 'route' => ['news-section.destroy', $i->id]]) }}
                            {{ Form::hidden('id', $i->id) }}
                            <button class="btn btn-danger btn-xs btn-xs1" onclick="return delete_action()"  data-title="Delete" data-toggle="modal" data-target="#delete" data-placement="top" rel="tooltip"><span class="glyphicon glyphicon-trash">Delete</span></button>
                            {!! Form::close() !!}
                        </span>

                    </td>
                </tr>
                @endforeach

                </tbody>
            </table>
        </div>
        
    </div>
</div>
<!-- loader for  bussiness Data end -->


</div>
</div>
</div>


@endsection
@stack('post-styles')

<link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />


