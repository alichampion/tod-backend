<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="page-content-wrapper">
  <div class="page-content">
    <div class="row">
      <div class="col-md-12">
        <div class="portlet light portlet-fit bordered">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-newspaper" ></i>
              <span class="caption-subject font-red sbold uppercase" >Vehicle Category Edit</span>
            </div>

          </div>
       
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    
                    {!! Form::open(['method' => 'PATCH' ,'files'=>true, 'route' => ['vehicle-sub-categories.update',$Datatypes->id], 'class'=>'form-horizontal']) !!}
                    
                    <div style="padding: 60px 0px;">     
                      <div class="row">
                         <div class="col-md-4">
                           <div class = "col-md-10 col-sm-10">
                            <div class="fileinput fileinput-new" data-provides="fileinput" >
                                <div class="fileinput-new thumbnail" style="max-width:250px; height: 250px;">
                                   
                                 <img src="{{asset('images/vehicle_sub_categories/' . $Datatypes->vehicle_image)}}" id="blah" height="250px" width = "250px">
                                 
                             </div>
                         </div>
                         <div class="form-group">
                            <label for="vehicle_image" class="btn btn-primary">Upload Profile</label>
                            <input type="file" id="vehicle_image" name="vehicle_image"  class="hide" style="opacity: 0;">
                        </div>
                        
                    </div> 
                </div>
                <div class="col-md-6">
                 
                    <div class="form-group">
                        <label class="col-md-4 control-label">Sub Categories Name</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control input-circle" value="{{ $Datatypes->name }}" name="name" >

                        </div>
                    </div>
                 
                    <div class="form-group">
                        <label class="col-md-4 control-label" style="font-size: 13px;"> Categorie Name</label>
                        <div class="col-md-12">
                            <select   class="form-control input-circle" name="vehical_categorie_id" value="{{ $Datatypes->vehical_categorie_id }}" >
                                
                                @foreach ($scategorie as $categorie)

                                <option  value="{{ $categorie->id }}" @if($categorie->id==$Datatypes->vehical_categorie_id) selected='selected' @endif> {{ $categorie->name}} </option>
                                @endforeach
                            </select>
                        </div>
                        
                    </div>
                    <br>
                    <br>
                    <div class="form-actions">
                            <div class="col-sm-8 col-md-12 pull-left">
                                <button type="submit" class="btn btn-info green">Submit</button>
                                 {!! Form::close() !!}
                                <a href="{{route('vehicle-sub-categories.index')}}" class="btn btn-success btn-outline">Cancel</a>
                            </div>
                     
                    </div>
                </div>
            </div>
            </div>
        </div>
       
        <!-- END FORM-->
    </div>
</div>


</div>
</div>


@endsection
@stack('post-styles')
<link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />

@push('post-scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


<script type="text/javascript">
    function readURL(input) {

        if (input.files) {
            var reader = new FileReader();
            var totalfiles = input.files.length;
            for(i = 0; i < totalfiles; i ++){
                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    }
    $("#vehicle_image").change(function() {
        readURL(this);
    });
</script>
<script type="text/javascript">
    $(function() {
                        // Multiple images preview in browser
                        var imagesPreview = function(input, placeToInsertImagePreview) {

                            if (input.files) {
                                var filesAmount = input.files.length;

                                for (i = 0; i < filesAmount; i++) {
                                    var reader = new FileReader();

                                    reader.onload = function(event) {
                                        $($.parseHTML('<img height = "250" width = "250" style = "margin:0px 10px 10px 0px;" >')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                                    }

                                    reader.readAsDataURL(input.files[i]);
                                }
                            }

                        };

                        
                    });
                </script>


                
                @endpush

                
                