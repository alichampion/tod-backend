<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="m-portlet m-portlet--mobile">

 <div class="m-portlet__head">
    <div class="m-portlet__head-caption head_21">
        <div class="m-portlet__head-title ">
            <h3 class="m-portlet__head-text sbold uppercase">
              <i class="fa fa-car" style="color: #fff;"></i>Edit New Franchise 
          </h3>
      </div>
  </div>
</div>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">

        </div>

        <div class="tab-content">

            <div class="caption">
            </div>

        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->


            {!! Form::open(['method' => 'PATCH' ,'files'=>true, 'route' => ['franchise.update',$franchise_data->id], 'class'=>'form-horizontal']) !!}

            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">Franchise Name</label>
                    <div class="col-md-12">
                        <input type="text" value="{{ $franchise_data->name}}" class="form-control input-circle"  name="name" >

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Address</label>
                    <div class="col-md-12">
                        <input type="text" value="{{ $franchise_data->address}}"  class="form-control input-circle"  name="address" >

                    </div>
                </div>


                <div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label" style="font-size: 13px;"> City Name</label>
                        <div class="col-md-12">
                            <select   class="form-control input-circle" value="{{ $franchise_data->branch_id}}" name="branch_id" >

                               @foreach ($branch_data as $categorie)

                               <option  value="{{ $categorie->id }}"> {{ $categorie->name}}
                               </option>
                               @endforeach
                           </select>
                       </div>

                   </div>
               </div>

           </div>

           <div class="form-actions">
           <div class=" col-md-12">
                  <div class="pull-right">
                    <button type="submit" class="btn btn-info green">Submit</button>
                     {!! Form::close() !!}
                    <button type="button" class="btn btn-success ">Cancel</button>
                </div>
            </div>
        </div>

       
        <!-- END FORM-->
    </div> 
</div>
</div>
</div>
</div>
</div>



@endsection

