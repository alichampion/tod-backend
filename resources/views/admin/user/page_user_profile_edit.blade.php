<?php
/**
 * Project: loader.
 * User: naveed
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
          <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title" style="background-color:#f26727;">
                <h3 class="m-portlet__head-text">
                 User Resources
              </h3>
          </div>
      </div>
  </div>
         {!! Form::open(['method' => 'PATCH' ,'files'=>true, 'route' => ['userResource.update',$User->id], 'class'=>'form-horizontal']) !!}
                                   
                                                    <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label class="control-label">First Name</label>
                                                            <input name = "fname" type="text" placeholder="John" class="form-control" 
                                                            value="{{ $User->fname }}"> </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Last Name</label>
                                                            <input type="text" name = "lname" placeholder="Doe" class="form-control" value="{{ $User->lname }}"> </div>
                                                            <div class="form-group col-md-6">
                                                            <label class="control-label">Email</label>
                                                            <input name = "email" type="email" class="form-control" placeholder="email" value="{{ $User->email }}"> </div>
                                                        
                                                            
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">phone</label>
                                                            <input type="text" name = "phone" placeholder="phone" class="form-control" value="{{ $User->phone }}"> 
                                                        </div>
                                                            
                                                            
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Password</label>
                                                            <input type="password" class="form-control" name = "password" value="">
                                                        </div>
                                                         <div class="form-group col-md-6">
                                                            <label class="control-label">Confirm Password</label>
                                                            <input name = "password_confirmation" type="password" class="form-control"> 
                                                        </div>
                                                       
                                                         <div class="col-md-12">
                                        <div class="col-md-6">
                                                    <div class = "col-md-4"  style="margin-left: 0px;">
                                         <div class="fileinput fileinput-new" data-provides="fileinput">
                                                  <div class="fileinput-new thumbnail" style="width: 250px; height: 150px;">
                                 
                                               <img src="{{asset('images/captain/profiles/no-image.png')}}" id="blah" height="250px" width = "150px">
                                                    <!-- </div> -->
                                                </div>
                                            </div>
                                        </div>
                                   <br><br><br><br>

                                            <div class="form-group">
                                             <label for="images" class="btn btn-primary">Upload Profile</label>
                                             <input type="file" id="images" name="images" class="hide" style="opacity: 0;">
                                         </div>
                                                    </div>
                                         </div>
                                                            <div class="col-md-12 ">
                                        <div class="col-md-9"> </div>
                                        <div class="col-md-3">
                                        <div class="margiv-top-10" style="float: right">
                                            <a href="javascript:;" class="btn green"> Save </a>
                                              {!! Form::close() !!}
                                            <a href="javascript:;" class="btn default"> Cancel </a>
                                        </div>
                                    </div>
                                    </div>
                                                         
                                                         </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
 @stack('post-styles')
<link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />

     <link href="{{asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/global/plugins/jquery-multi-select/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />

@push('post-scripts')

        
        <script src="{{asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

                <script type="text/javascript">
                    function readURL(input) {

                        if (input.files) {
                            var reader = new FileReader();
                            var totalfiles = input.files.length;
                            for(i = 0; i < totalfiles; i ++){
                                reader.onload = function(e) {
                                    $('#blah').attr('src', e.target.result);
                                }
                                reader.readAsDataURL(input.files[i]);
                            }
                        }
                    }
                    $("#images").change(function() {
                        readURL(this);
                    });
                </script>
                <script type="text/javascript">
                    $(function() {
                        // Multiple images preview in browser
                        var imagesPreview = function(input, placeToInsertImagePreview) {

                            if (input.files) {
                                var filesAmount = input.files.length;

                                for (i = 0; i < filesAmount; i++) {
                                    var reader = new FileReader();

                                    reader.onload = function(event) {
                                        $($.parseHTML('<img height = "250" width = "250" style = "margin:0px 10px 10px 0px;" >')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                                    }

                                    reader.readAsDataURL(input.files[i]);
                                }
                            }

                        };

                        
                    });
                </script>


@endpush