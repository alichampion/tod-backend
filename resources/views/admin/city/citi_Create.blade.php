<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                <div class="portlet box green">
                    <div class="portlet-title ">
             
                        <div class="caption">

                                     <i class="fa fa-car" style="color: #fff;"></i>
                <span class="caption-subject font-red sbold uppercase"> Add New City  </span> 
         
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="{{route('cities.store') }}" class="form-horizontal" method="POST">
                            {{ csrf_field() }}

                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">City Name</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control input-circle"  name="name" >
                                        @if ($errors->has('name'))
                                        <div class="alert alert-danger" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                              <span aria-hidden="true">×</span>
                                              <span class="sr-only">Close</span>
                                          </button>
                                          <strong>Warning!</strong> {{$errors->first('name')}}
                                      </div>
                                      @endif

                                  </div>
                              </div>
                              <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" style="font-size: 13px;"> Country Name</label>
                                    <div class="col-md-12">
                                        <select   class="form-control input-circle" name="countrie_id" >

                                         @foreach ($countries as $categorie)

                                         <option  value="{{ $categorie->id }}"> {{ $categorie->country_name}}
                                         </option>
                                         @endforeach
                                     </select>
                                 </div>

                             </div>
                         </div>

                     </div>
                     <div class="form-actions">                      
                            <div class="col-md-offset-4 col-md-11">
                              <div class="pull-right">
                                  <button type="submit" class="btn btn-info">Submit</button>
                                 </form>
                               <a href="{{ route('cities.index')}}" class="btn btn-success btn-outline">Cancel</a>
                              </div>
                              
                             </div>
                            </div>
                            <br>
                        </div>
                    </div>     
                 </div>
                </div>
              </div>
              </div>

   @endsection
   @stack('post-styles')
   <link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css"/>
