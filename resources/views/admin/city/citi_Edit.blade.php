<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')

  <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">

        </div>

        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        </div>

                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->

                        {!! Form::open(['method' => 'PATCH' ,'files'=>true, 'route' => ['cities.update',$city_data->id], 'class'=>'form-horizontal']) !!}


                        <div class="form-body">

                            <div class="form-group">
                                <label class="col-md-3 control-label">City Name</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control input-circle"  name="name" value="{{$city_data->name}}" >

                                </div>
                            </div>


                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" style="font-size: 13px;"> Country Name</label>
                                    <div class="col-md-4">
                                        <select   class="form-control input-circle" name="countrie_id" >

                                           @foreach ($countries as $categorie)

                                           <option  value="{{ $categorie->id }}"> {{ $categorie->country_name}}
                                           </option>
                                           @endforeach
                                       </select>
                                   </div>

                               </div>
                           </div>

                       </div>

                       <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-circle green">Submit</button>
                                <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}
                    <!-- END FORM-->
                </div> 
            </div>
        </div>
    </div>
</div>


@endsection

