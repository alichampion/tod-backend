<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
      <!-- BEGIN PAGE HEAD-->
    <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">       
              <i class="fa fa-key" style="color: #fff;"></i>
              <span class="caption-subject  sbold uppercase" style="color: #fff; font-size: 20px!important;"> Vehicle Sub Categories </span>
          </div> </div>  

                 <div class="row">
                <div class="col-md-12">
               
            <!-- BEGIN FORM-->
            <form action="{{route('vehicle-sub-categories.store') }}" class="form-horizontal" method="POST" enctype = "multipart/form-data" id="upload_new_form">
              {{ csrf_field() }}
                 <div class="row">
                  <div class="col-md-4">    
                    <div class="fileinput fileinput-new" data-provides="fileinput" >
                      <div class="fileinput-new thumbnail" style="max-width:250px; height: 250px;">

                       <img src="{{asset('images/captain/profiles/no-image.png')}}" id="blah" height="250px" width = "250px">
                     </div>
                    </div>

                       <div class="form-group" style="margin-left:35px;">
                        <label for="vehicle_image" class="btn btn-primary">Upload Profile</label>
                        <input type="file" id="vehicle_image" name="vehicle_image" class="hide" style="opacity: 0;">            
               
                            @if ($errors->has('vehicle_image'))
                              <div class="alert alert-danger" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                      <span aria-hidden="true">×</span>
                                      <span class="sr-only">Close</span>
                                  </button>
                                  <strong>Warning!</strong> {{$errors->first('vehicle_image')}}
                              </div>
                          @endif                                       
                            </div>
                   </div>
                    <dsiv class="col-md-6">

                <div class="form-group">
                  <label class="control-label">Sub Categories Name</label>
               
                    <input type="text" class="form-control input-circle"  name="name" >
                     @if ($errors->has('name'))
                              <div class="alert alert-danger" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                      <span aria-hidden="true">×</span>
                                      <span class="sr-only">Close</span>
                                  </button>
                                  <strong>Warning!</strong> {{$errors->first('name')}}
                              </div>
                          @endif      

                </div>
          
           
                <div class="form-group">
                  <label class="control-label" style="font-size: 13px;"> Categorie Name</label>
             
                    <select   class="form-control input-circle" name="vehical_categorie_id" >


                      @foreach ($scategorie as $categorie)

                      <option  value="{{ $categorie->id }}"> {{ $categorie->name}} </option>
                      @endforeach
                    </select>
            
                </div>
                <br>
                <br>
        
                     <div class="form-actions">
          
                    <div class="col-xm-8 col-md-6 pull-right">
                      <button type="submit" class="btn btn-info ">Submit</button>
                       {!! Form::close() !!}
                   <a  href="{{route('vehicle-sub-categories.index')}}" class="btn btn-success  btn-outline">cancel</a>
                    </div>        
            </div>
          <!-- END FORM-->
        </div>

      </div>
    </div>
  </div>


@endsection
@stack('post-styles')
<link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css"/>         
@push('post-scripts')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


<script type="text/javascript">
  function readURL(input) {

    if (input.files) {
      var reader = new FileReader();
      var totalfiles = input.files.length;
      for(i = 0; i < totalfiles; i ++){
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[i]);
      }
    }
  }
  $("#vehicle_image").change(function() {
    readURL(this);
  });
</script>
<script type="text/javascript">
  $(function() {
                        // Multiple images preview in browser
                        var imagesPreview = function(input, placeToInsertImagePreview) {

                          if (input.files) {
                            var filesAmount = input.files.length;

                            for (i = 0; i < filesAmount; i++) {
                              var reader = new FileReader();

                              reader.onload = function(event) {
                                $($.parseHTML('<img height = "250" width = "250" style = "margin:0px 10px 10px 0px;" >')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                              }

                              reader.readAsDataURL(input.files[i]);
                            }
                          }

                        };

                        
                      });
                    </script>



                    @endpush

