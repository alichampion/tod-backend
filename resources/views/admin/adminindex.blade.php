<style type="text/css">
.portlet.light.bordered>.portlet-title {
    border-bottom: 1px solid #eef1f5;
    text-align: center;
    border-radius: 8px;
    padding-left: 15px;
    background-color: #f26727;
}

</style>

<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>
@extends('_layouts.admin.app')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="row">
            <div class="col-xl-12 col-md-12 col-sm-12 col-xs-12">
                @component('_components.alerts-default')
                @endcomponent
                <div class="row" style="display: -webkit-box;">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 bordered">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-green-sharp">
                                        <span data-counter="counterup" data-value="{{ $users }}">0</span>
                                        <small class="font-green-sharp"></small>
                                    </h3>
                                    <small> User</small>
                                </div>
                                <div class="icon">
                                    <i class="icon-user"></i>
                                </div>
                            </div>
                            <div class="progress-info">
                                <div class="progress">
                                    <span style="width: 100%;" class="progress-bar progress-bar-success green-sharp">
                                        <span class="sr-only">100% progress</span>
                                    </span>
                                </div>
                                <div class="status">
                                    <div class="status-title"> Total User </div>
                                    <!-- <div class="status-number"> 76% </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 bordered green">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-red-haze">
                                        <span data-counter="counterup" data-value="{{ $captains }} / {{$captains_login}}">0</span>
                                    </h3>
                                    <small>Captain</small>
                                </div>
                                <div class="icon">
                                    <i class="icon-user"></i>
                                </div>
                            </div>
                            <div class="progress-info">
                                <div class="progress">
                                    <span style="width: 100%;" class="progress-bar progress-bar-success red-haze">
                                     <!--  <span class="sr-only">85% change</span> -->
                                 </span>
                             </div>
                             <div class="status">
                                <div class="status-title"> Total Captains </div>
                                <!--  <div class="status-number"> 85% </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat2 bordered">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-blue-sharp">
                                    <span data-counter="counterup" data-value="{{ $admins }}"></span>
                                </h3>
                                <small> Admin</small>
                            </div>
                            <div class="icon">
                                <i class="icon-user"></i>
                            </div>
                        </div>
                        <div class="progress-info">
                            <div class="progress">
                                <span style="width: 100%;" class="progress-bar progress-bar-success blue-sharp">
                                 <!--  <span class="sr-only">45% grow</span> -->
                             </span>
                         </div>
                         <div class="status">
                            <div class="status-title"> total Admin </div>
                            <!-- <div class="status-number"> 45% </div> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-purple-soft">
                                <span data-counter="counterup" data-value="{{ $rides  }}"></span>
                            </h3>
                            <small>Ride</small>
                        </div>
                        <div class="icon">
                            <i class="icon-pie-chart"></i>

                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: 100%;" class="progress-bar progress-bar-success purple-soft">
                                <!-- <span class="sr-only">56% change</span> -->
                            </span>
                        </div>
                        <div class="status">
                            <div class="status-title"> Total Ride </div>
                            <!-- <div class="status-number"> 57% </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="display: -webkit-box;">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject bold uppercase font-white">Captain</span>
                            <span class="caption-helper" style="color: white;">captain stats...</span>
                        </div>
                        <div class="actions">
                           
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="dashboard_amchart_1" class="CSSAnimationChart"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6  col-sm-6 col-xm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption ">
                            <span class="caption-subject font-white bold uppercase" style="font-size: 18px;">User</span>
                            <span class="caption-helper" style="color: white;">users stats...</span>
                        </div>
                        <div class="actions">
                          
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div id="dashboard_amchart_user" class="CSSAnimationChart"></div>
                    </div>
                </div>
            </div>
        </div>
<div class="row" style="display: -webkit-box;">
         <div class="col-lg-6 col-md-6 col-sm-6 col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title ">
                    <div class="caption">
                        <i class="icon-bubbles font-dark hide"></i>
                        <span class="caption-subject  bold uppercase" style="color: #fff!important;">Admins</span>
                    </div>

                </div>
                <div class="portlet-body" >
                    <div class="tab-content" style="overflow: auto;">
                        <div class="tab-pane active " id="portlet_comments_1">
                         <table class="table table-striped table-hover table-bordered table_responsiv table-responsiv1" id="captain">
                             <thead>
                                <tr>

                                    <th> image </th>
                                    <th> Name </th>
                                    <th> email </th>

                                    <th> phone </th>

                                    <th>Action </th>
                                </tr>
                            </thead>
                            <tbody  style="overflow-x: scroll;">

                               @foreach ($admin as $i) 
                               <tr>


                                <td> <img src="{{asset('images/admin/profile/' . $i->images)}}" alt="admin image" class="logo-default" style="width: 40px; height: 40px; margin-top: 0px;" > </td>
                                <td> {{ $i->name }}   </td>
                                <td> {{ $i->email }} </td>
                                <td> {{ $i->phone }} </td>


                                <td style="min-width: 140px;">
                                <span style="display: inline;">
                                    <a href="{{ route('adminResource.show', $i['id']) }}" class="btn btn-primary btn-sm">Show</a>
                                    <a href="{{ route('adminResource.edit', $i['id']) }}" class="btn btn-primary btn-sm" >Edit</a>
                                    </span> 
                                </td>



                            </tr>
                            @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="portlet light bordered">
        <div class="portlet-title tabbable-line">
            <div class="caption">
                <i class=" icon-social-twitter font-dark hide"></i>
                <span class="caption-subject font-dark bold uppercase" style="color: #fff!important;">Captains</span>
            </div>

        </div>
        <div class="portlet-body">
            <div class="tab-content" style="overflow: auto;">
                <div class="tab-pane active" id="tab_actions_pending">
                    <!-- BEGIN: Actions -->
                    <div class="mt-actions table-responsive">
                       <table class="table table-striped table-hover table_responsiv table-bordered table-responsiv1" id="captain-table">
                         <thead>
                            <tr>

                                <th> Image </th>
                                <th> Name </th>
                                <th> email </th>
                                <th> phone </th>
                                <th>Action </th>
                            </tr>
                        </thead>
                        <tbody style="overflow-x: scroll!important;">

                           @foreach ($captain as $i) 
                           <tr>


                            <td> <img src="{{asset('images/captain/profiles/'.$i->images)}}" alt=" image" class="logo-default" style="width: 40px; height: 40px; margin-top: 0px;" />  </td>
                            <td> {{ $i->fname }}  {{$i->lname}} </td>
                            <td> {{ $i->email }} </td>
                            <td> {{ $i->phone }} </td>


                            <td style="min-width: 145px;">
                                <span style="display: inline;">
                               <a href="admin/captain-wallet/{{$i['id']}}"><button class="btn btn-primary btn-sm"  data-title="Edit" data-toggle="modal" data-target="#editModal" data-placement="top" rel="tooltip"><span class="glyphicon glyphicon-briefcase"></span>Wallet</button></a>
                               

                               <a href="{{ route('captainResource.edit', $i['id']) }}"><button class="btn btn-primary btn-sm"  data-title="Edit" data-toggle="modal" data-target="#editModal" data-placement="top" rel="tooltip"><span class="glyphicon glyphicon-pencil"></span>Edit</button></a>
                               </span>
                           </td>



                       </tr>
                       @endforeach

                   </tbody>
               </table>
           </div>
           <!-- END: Actions -->
       </div>
       <div class="tab-pane" id="tab_actions_completed">
        <!-- BEGIN:Completed-->
        <div class="mt-actions">
            <div class="mt-action">
                <div class="mt-action-img">
                    <img src="../assets/pages/media/users/avatar1.jpg" /> </div>
                    <div class="mt-action-body">
                        <div class="mt-action-row">
                            <div class="mt-action-info ">
                                <div class="mt-action-icon ">
                                    <i class="icon-action-redo"></i>
                                </div>
                                <div class="mt-action-details ">
                                    <span class="mt-action-author">Frank Cameron</span>
                                    <p class="mt-action-desc">Lorem Ipsum is simply dummy</p>
                                </div>
                            </div>
                            <div class="mt-action-datetime ">
                                <span class="mt-action-date">3 jun</span>
                                <span class="mt-action-dot bg-red"></span>
                                <span class="mt=action-time">9:30-13:00</span>
                            </div>
                            <div class="mt-action-buttons ">
                                <div class="btn-group btn-group-circle">
                                    <button type="button" class="btn btn-outline green btn-sm">Appove</button>
                                    <button type="button" class="btn btn-outline red btn-sm">Reject</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-action">
                    <div class="mt-action-img">
                        <img src="../assets/pages/media/users/avatar8.jpg" /> </div>
                        <div class="mt-action-body">
                            <div class="mt-action-row">
                                <div class="mt-action-info ">
                                    <div class="mt-action-icon ">
                                        <i class="icon-cup"></i>
                                    </div>
                                    <div class="mt-action-details ">
                                        <span class="mt-action-author">Ella Davidson </span>
                                        <p class="mt-action-desc">Text of the printing and typesetting industry</p>
                                    </div>
                                </div>
                                <div class="mt-action-datetime ">
                                    <span class="mt-action-date">3 jun</span>
                                    <span class="mt-action-dot bg-green"></span>
                                    <span class="mt=action-time">9:30-13:00</span>
                                </div>
                                <div class="mt-action-buttons">
                                    <div class="btn-group btn-group-circle">
                                        <button type="button" class="btn btn-outline green btn-sm">Appove</button>
                                        <button type="button" class="btn btn-outline red btn-sm">Reject</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-action">
                        <div class="mt-action-img">
                            <img src="../assets/pages/media/users/avatar5.jpg" /> </div>
                            <div class="mt-action-body">
                                <div class="mt-action-row">
                                    <div class="mt-action-info ">
                                        <div class="mt-action-icon ">
                                            <i class=" icon-graduation"></i>
                                        </div>
                                        <div class="mt-action-details ">
                                            <span class="mt-action-author">Jason Dickens </span>
                                            <p class="mt-action-desc">Dummy text of the printing and typesetting industry</p>
                                        </div>
                                    </div>
                                    <div class="mt-action-datetime ">
                                        <span class="mt-action-date">3 jun</span>
                                        <span class="mt-action-dot bg-red"></span>
                                        <span class="mt=action-time">9:30-13:00</span>
                                    </div>
                                    <div class="mt-action-buttons ">
                                        <div class="btn-group btn-group-circle">
                                            <button type="button" class="btn btn-outline green btn-sm">Appove</button>
                                            <button type="button" class="btn btn-outline red btn-sm">Reject</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-action">
                            <div class="mt-action-img">
                                <img src="../assets/pages/media/users/avatar2.jpg" /> </div>
                                <div class="mt-action-body">
                                    <div class="mt-action-row">
                                        <div class="mt-action-info ">
                                            <div class="mt-action-icon ">
                                                <i class="icon-badge"></i>
                                            </div>
                                            <div class="mt-action-details ">
                                                <span class="mt-action-author">Jan Kim</span>
                                                <p class="mt-action-desc">Lorem Ipsum is simply dummy</p>
                                            </div>
                                        </div>
                                        <div class="mt-action-datetime ">
                                            <span class="mt-action-date">3 jun</span>
                                            <span class="mt-action-dot bg-green"></span>
                                            <span class="mt=action-time">9:30-13:00</span>
                                        </div>
                                        <div class="mt-action-buttons ">
                                            <div class="btn-group btn-group-circle">
                                                <button type="button" class="btn btn-outline green btn-sm">Appove</button>
                                                <button type="button" class="btn btn-outline red btn-sm">Reject</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END: Completed -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--------------- here next row for user  ------------->
<div class="row" style="display: -webkit-box;">

   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-bubble font-dark hide"></i>
                <span class="caption-subject font-hide bold uppercase" style="color: #fff!important;">Recent Users</span>
            </div>
            <div class="actions">
                <div class="btn-group">
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row" style="display: -webkit-box;">
                @foreach ($user as $data)

                <div class="col-md-4" style="display: -webkit-box;">
                    <!--begin: widget 1-1 -->
                    <div class="mt-widget-1">
                        <div class="mt-icon">
                           
                        </div>
                        <div class="mt-img">
                            <img  img alt="" class="img-circle" style="width: 100px; height: 80px;" 
                            src="{{asset('images/user/profile/' . $data->images)}}"> </div>
                            <div class="mt-body">
                                <h3 class="mt-username">{{$data->fname}} {{$data->lname}}</h3>
                                <p class="mt-user-title"> {{$data->phone}} </p>
                                
                            </div>
                        </div>
                        <!--end: widget 1-1
         <!--            </div>
                    @endforeach
                </div>
            </div>
        </div>
        
    </div> --> 
    </div>
</div>
</div>
</div>
</div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bubble font-dark hide"></i>
                    <span class="caption-subject font-hide bold uppercase" style="color: #fff!important;">Recent Users</span>
                </div>
                <div class="actions">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row" style="display: -webkit-box!important;">
                    @foreach ($user as $data)

                    <div class="col-md-4">
                        <!--begin: widget 1-1 -->
                        <div class="mt-widget-1">
                            <div class="mt-icon">
                               
                            </div>
                            <div class="mt-img">
                                <img  img alt="" class="img-circle" style="width: 80px; height: 80px;" 
                                src="{{asset('images/user/profile/' . $data->images)}}"> </div>
                                <div class="mt-body">
                                    <h3 class="mt-username">{{$data->fname}} {{$data->lname}}</h3>
                                    <p class="mt-user-title"> {{$data->phone}} </p>
                                    
                                </div>
                            </div>
                            <!--end: widget 1-1 -->
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            
        </div>
</div>
<div class="clear-fix"></div>
    <!-- BEGIN : ECHARTS -->
    <div class="row">
        <div class="col-xm-12 col-md-12 col-sm-12 col-xm-12">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green bold uppercase" style="color: #fff!important;">Transaction Amount Chart</span>
                        </div>
                        <div class="actions">
                         
                        </div>
                    </div>
                    <div class="portlet-body" style="overflow-x: scroll!important;">
                        <div id="echarts_bar" style="height: 500px;"></div>
                    </div>
                </div>
            </div>
        </div> 

        

        <!-- END : ECHARTS -->






        @endsection


        

        @stack('post-styles')
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        
        <link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />
        
        
        <link href="{{asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css" />
        <!--   -->
        <link href="{{asset('assets/global/plugins/morris/morris.css')}}" rel="stylesheet" type="text/css" />
        <!--  --> 
        <link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{asset('assets/global/css/plugins.css')}}" rel="stylesheet" type="text/css" />

        <link href="{{asset('assets/layouts/layout4/css/layout.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/layouts/layout4/css/themes/default.css')}}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{asset('assets/layouts/layout4/css/custom.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/layouts/layout4/css/stylish.css')}}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" />

        <link href="{{asset('assets/global/plugins/jqvmap/jqvmap/jqvmap.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/global/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
        <style type="text/css">
        body{
            overflow-x: hidden;
        }
    </style>

    @push('post-scripts')
    <script src="{{asset('assets/global/plugins/amcharts/amcharts/amcharts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/amcharts/amcharts/serial.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/amcharts/amcharts/pie.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/amcharts/amcharts/radar.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/light.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/patterns.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/chalk.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/amcharts/ammap/ammap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/amcharts/amstockcharts/amstock.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/pages/scripts/charts-echarts.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/echarts/echarts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/morris/morris.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/morris/raphael-min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/counterup/jquery.waypoints.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/counterup/jquery.counterup.min.js')}}" type="text/javascript"></script>
    
    <script src="{{asset('assets/global/plugins/fullcalendar/fullcalendar.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/horizontal-timeline/horizontal-timeline.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/flot/jquery.flot.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/flot/jquery.flot.resize.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/flot/jquery.flot.categories.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery.sparkline.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('assets/pages/scripts/dashboard.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js')}}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->

    <script src="{{asset('assets/layouts/layout4/scripts/layout.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/layouts/layout4/scripts/demo.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/layouts/global/scripts/quick-nav.min.js')}}" type="text/javascript"></script>

    

    <script>


       var chartData = <?php echo json_encode($captain_months); ?>;

       var chart = AmCharts.makeChart("dashboard_amchart_1", {
          "type": "serial",
          "theme": "light",
          "marginRight": 70,
          "dataProvider": chartData,
          "valueAxes": [{
            "axisAlpha": 0,
            "position": "left",
            "title": "Captians"
        }],
        "startDuration": 1,
        "graphs": [{
            "balloonText": "<b>[[category]]: [[value]]</b>",
            "fillColorsField": "color",
            "fillAlphas": 0.9,
            "lineAlpha": 0.2,
            "type": "column",
            "valueField": "total_captain"
        }],
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "categoryField": "month",
        "categoryAxis": {
            "gridPosition": "start",
            "labelRotation": 45
        },
        "export": {
            "enabled": true
        }

    });
</script>

<!-- for user graphs -->
<script>





   var chartData = <?php echo json_encode($user_months); ?>;

   var chart = AmCharts.makeChart("dashboard_amchart_user", {
      "type": "serial",
      "theme": "light",
      "marginRight": 70,
      "dataProvider": chartData,
      "valueAxes": [{
        "axisAlpha": 0,
        "position": "left",
        "title": "Users"
    }],
    "startDuration": 1,
    "graphs": [{
        "balloonText": "<b>[[category]]: [[value]]</b>",
        "fillColorsField": "color",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "type": "column",
        "valueField": "total_user"
    }],
    "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
    },
    "categoryField": "month",
    "categoryAxis": {
        "gridPosition": "start",
        "labelRotation": 45
    },
    "export": {
        "enabled": true
    }

});
</script>

<script >
    jQuery(document).ready(function() {
    // ECHARTS
    require.config({
        paths: {
            echarts: '../assets/global/plugins/echarts/'
        }
    });

    // DEMOS
    require(
        [
        'echarts',
        'echarts/chart/bar',
        
        ],
        
        function(ec) {
            //--- BAR ---
            var myChart = ec.init(document.getElementById('echarts_bar'));
            myChart.setOption({
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: ['Amount', 'Collect_Amount']
                },
                toolbox: {
                    show: true,
                    feature: {
                        mark: {
                            show: true
                        },
                        dataView: {
                            show: true,
                            readOnly: false
                        },
                        magicType: {
                            show: true,
                            type: ['line', 'bar']
                        },
                        restore: {
                            show: true
                        },
                        saveAsImage: {
                            show: true
                        }
                    }
                },
                calculable: true,
                xAxis: [{
                    type: 'category',
                    data: <?php echo json_encode($year_months);?>
                }],
                yAxis: [{
                    type: 'value',
                    splitArea: {
                        show: true
                    }
                }],
                series: [{
                    name: 'Amount',
                    type: 'bar',
                    data: <?php echo json_encode($total_amount);?>
                }, {
                    name: 'Collect_Amount',
                    type: 'bar',
                    data: <?php echo json_encode($collected_amount);?>
                }]
            });
        }
        );

    
});
</script>





@endpush  

