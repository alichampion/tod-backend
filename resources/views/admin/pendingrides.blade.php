<?php
/**
 * Project: hajjtrack.
 * User: naveed
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="m-portlet m-portlet--mobile">


    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                 Current Rides Record- {{  $current }} 
                 <span class="badge badge-danger fade in" style="background-color: black; width: 50px; height:35px; "><p style="position: relative;top: -15px;font-weight: bold;font-size: 18px;">{{  $current }}</p></span>
             </h3>
         </div>
     </div>
 </div>
 @component('_components.alerts-default')
 @endcomponent

 <div class="m-portlet__body">

    <!--begin: Search Form -->
    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
        <div class="row align-items-center">
           <div class="col-xl-8 order-2 order-xl-1">
                <div class="form-group m-form__group row align-items-center">
                    <div class="col-md-4">
                         <div class="m-input-icon m-input-icon--left">
                            <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                <span>
                                    <i class="la la-search"></i>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="btn-group" style="margin-right: 5px; margin-bottom:5px ">
                            <a href="{{ URL::to('admin/pendingExcel/xls') }}" style="margin-right: 5px;"><button class="btn btn-success"> Excel xls</button></a>
                            <a href="{{ URL::to('admin/pendingExcel/xlsx') }}"  style="margin-right: 5px;"><button class="btn btn-success"> Excel xlsx</button></a>
                            <a href="{{ URL::to('admin/pendingExcel/csv') }}"><button class="btn btn-success"> CSV</button></a>
                          
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--end: Search Form -->

    <div class="m_datatable" id="child_data_ajax"></div>
</div>


</div>


@endsection

@push('post-scripts')
<script type="application/javascript">

    var datatable = $('.m_datatable').mDatatable({
    // datasource definition
    data: {
        type: 'remote',
        source: {
            read: {
                url: BASE_URL+'/admin/pending_data'
            }
        },
    pageSize: 10, // display 20 records per page
    saveState: {
        cookie: false,
        webstorage: false
    },
    serverPaging: true,
    serverFiltering: true,
    serverSorting: true
},

    // layout definition
    layout: {
        theme: 'default',
        scroll: false,
        height: null,
        footer: false
    },

    // column sorting
    sortable: true,

    pagination: true,

    detail: false,

    search: {
        input: $('#generalSearch')
    },

    // columns definition
    columns: [ 
    {
        field: "id",
        title: "#Id ",
    // responsive: {hidden: 'md'}

},
{
    field: "user_id",
    title: "User Name",
    template: function (row) {
        return row.user.fname+" "+row.user.lname;
    }
},
{
    field: "captain_id",
    title: "Captain Name",
    template: function (row) {
        return row.captain.fname+" "+row.captain.lname;
    }
},
{
    field: "booking_id",
    title: "Pickup Place ",
    template: function (row) {
        return row.booking.pickup_place;
    }
},
{
    field: "amount",
    title: "Amount ",
    template: function (row) {
        return row.booking.amount;
    }
},
{
    field: "distance",
    title: "Distance ",
    template: function (row) {
        return row.booking.distance;
    }
},


{
    field: "created_at",
    title: "Data/time",
},
{
    field: "status",
    title: "Status",
    template: function (status) {
        return status?'Pending':'pending';
    }
}

]
});

</script>



@endpush
