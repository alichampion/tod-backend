<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="m-portlet m-portlet--mobile">


    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                  Captain Report
              </h3>
          </div>
      </div>
  </div>

  <div class="page-content-wrapper">

    <div class="portlet-body form">
        <div class="form-body">
          
            <div class="col-md-12">
            <div class="form-group">
                      
                <form action="{{route('captain_report')}}" class="mt-repeater form-horizontal" method="get">
                    {{ csrf_field() }}

                    <div data-repeater-list="group-a">
                        <div data-repeater-item class="mt-repeater-item">
                            <!-- jQuery Repeater Container -->
                            <div class="mt-repeater-input">
                                 <label class="control-label">Captain </label>
                                <br/>
                                <select id="select2" name="captain_id" class="js-example-data-ajax form-control">
                                  <option value="" selected="selected">select2/select2</option>
                              </select>
                               
                            </div>
                            <!-- <div class="mt-repeater-input">
                                <label class="control-label">Name</label>
                                <br/>

                                <input type="text" name="fname" class="form-control" value="shafqat" /> 

                            </div> -->
                            <div class="mt-repeater-input">
                                <label class="control-label">start Date</label>
                                <br/>
                                <input class="input-group form-control form-control-inline date date-picker" size="16" type="date"  value="2018-03-27" name="start_date" data-date-format="yyyy-mm-dd" /> </div>
                                <div class="mt-repeater-input">
                                    <label class="control-label">Till Date</label>
                                    <br/>
                                    <input class="input-group form-control form-control-inline date date-picker" size="16" type="date" value="2018-04-27" name="last_date" data-date-format="yyyy-mm-dd" /> </div>



                                </div>
                            </div>
                                        <!-- <a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add">
                                            <i class="fa fa-plus"></i> Add</a> -->
                                      
                                    </div>

                                    <div>
                                        <button type="submit" class="btn btn-circle green"style="margin-bottom: 20px!important;">Submit</button>
                                    </div>
                                    </div>
                                      </form>
                                </div>
                            </div>
                        </div>
 </div>


                        @endsection
@push('post-scripts')


<script type="text/javascript">
 $('#select2').select2({
  ajax: {
    url: "{{route('captain_account')}}",
    method:"post",
    dataType: 'json',
    processResults: function (_data, params) {
      
        var data1= $.map(_data, function (obj) {
        var newobj = {};
        newobj.id = obj.id;
        newobj.text= obj.fname+" "+obj.lname;
        newobj.balance= obj.wallet.credit_amount;
        return newobj;
      });
       //console.log(data1);return false;
      return { results:data1};
    }
  }
}).on('change', function() {

  console.log($(this).select2('data')[0].balance);
  var balance = $(this).select2('data')[0].balance;
  $('#title').val(balance);
});

</script>


@endpush