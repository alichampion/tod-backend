 
<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<style >
.row{
  display: -webkit-box;
}
</style>
<div class="page-content-wrapper">
  <div class="page-content">
    <div class="portlet light">
      <div class="portlet-title" style="border: 0;">
        <div class="caption">
          <span class="caption-subject font-green-sharp bold uppercase">
            <!--   Add new -->
          </span>
        </div>
      </div><!--portlet-title ends-->

      <form method="POST" action="{{ route('captainResource.store') }}" enctype = "multipart/form-data" id="upload_new_form">
        {{ csrf_field() }}
 @component('_components.alerts-default')
        @endcomponent

        <input type="hidden" name="driving_licenes_images[]" class="driving_licenes_images" id="driving_licenes_images" value="">
        <input type="hidden" name="driving_cnic_images[]" class="driving_cnic_images" id="driving_cnic_images" value="">
        <input type="hidden" name="vehicle_registration_image[]" class="vehicle_registration_image" id="vehicle_registration_image" value="">
        <input type="hidden" name="vehicle_image[]" class="vehicle_image" id="vehicle_image" value="">
        <div class="form-body">
          <div class="panel-group">

            <div class="panel panel-primary basic_info_panel">
              <div class="panel-heading" style="padding: 20px 0px;background-color:#f26727;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-user" style="color: #fff;"> </i> <b style="color: #fff!important;
              text-transform: uppercase;">Basic Info</b></h4></div>
              <div class="panel-body">

                <div class="row">

                  <div class="col-md-3">
                    <div class="fileinput fileinput-new" data-provides="fileinput" >
                      <div class="fileinput-new thumbnail" style="width: 250px; height: 250px;">
                        <!-- <div style="text-align: center;font-size:20px;margin:80px;">Profile<br>Photo -->
                          <img src="{{asset('images/captain/profiles/no-image.png')}}" id="profile-img-tag" height="250" width = "250">
                          <!-- </div> -->
                        </div>
                      </div>
                      <div class="form-group">
                       <label for="images" class="btn btn-primary" style="position: relative;left: 50px;top: 20px;">Upload Profile</label>
                       <input type="file" id="images" value="{{ old('images') }}" name="images" class="hide" style="opacity: 0;">
                       @if ($errors->has('images'))
                       <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          <span class="sr-only">Close</span>
                        </button>
                        <strong>Warning!</strong> {{$errors->first('images')}}
                      </div>
                      @endif
                    </div>
                    <!-- cnic image upload here -->
                    <div class="col-md-4">
                      <div class="form-group" style="padding: 0px; ">
                        <div class = "gallery"></div>
                      </div>
                    </div>
                  </div><!--form-group ends-->

                  <div class="col-md-9" style="margin-bottom: 20px;">
                    <div class="row" style="margin: 10px 0px;">

                      <div class="col-md-6">
                       <label class="control-label">First Name</label>
                       <input name="fname" value="{{ old('fname') }}" type="text" placeholder="First Name" class="form-control">
                       @if ($errors->has('fname'))
                       <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          <span class="sr-only">Close</span>
                        </button>
                        <strong>Warning!</strong> {{$errors->first('fname')}}
                      </div>
                      @endif
                    </div><!--form-group ends-->

                    <div class="col-md-6">
                     <label class="control-label">Last Name</label>
                     <input type="text" name="lname" value="{{ old('lname') }}" placeholder="Last Name" class="form-control">
                     @if ($errors->has('lname'))
                     <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                      </button>
                      <strong>Warning!</strong> {{$errors->first('lname')}}
                    </div>
                    @endif
                  </div><!--form-group ends-->

                </div>


                <div class="row" style="margin: 10px 0px;">

                  <div class="col-md-6">
                    <label class="control-label">Email</label>
                    <input name="email" type="email" value="{{ old('email') }}" class="form-control" placeholder="abc123@example.com">
                    @if ($errors->has('email'))
                    <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                      </button>
                      <strong>Warning!</strong> {{$errors->first('email')}}
                    </div>
                    @endif
                  </div><!--form-group ends-->

                  <div class="col-md-6">
                    <label class="control-label">phone</label>
                    <div class="input-group">
                     <span class="input-group-addon" id="basic-addon">+92</span>
                     <input type="text" name="phone" value="{{ old('phone') }}" placeholder="Mobile No" class="form-control">

                   </div>
                   @if ($errors->has('phone'))
                   <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">Close</span>
                    </button>
                    <strong>Warning!</strong> {{$errors->first('phone')}}
                  </div>
                  @endif


                </div><!--form-group ends-->

              </div> 


              <div class="row" style="margin: 10px 0px;">

                <div class="col-md-6">
                  <label class="control-label">Password</label>
                  <input type="password" placeholder="******" class="form-control" name="password">
                  @if ($errors->has('password'))
                  <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">Close</span>
                    </button>
                    <strong>Warning!</strong> {{$errors->first('password')}}
                  </div>
                  @endif
                </div><!--form-group ends-->

                <div class="col-md-6">
                  <label class="control-label">Confirm Password</label>
                  <input name="password_confirmation" type="password" placeholder="******" class="form-control">
                </div><!--form-group ends-->

              </div> 

              <div class="row" style="margin: 10px 0px;">

                <div class="col-md-6">
                 <label class="control-label">City</label>
                 <input name="city" value="{{ old('city') }}" type="text" placeholder="City" class="form-control">
               </div><!--form-group ends-->

               <div class="col-md-6">
                 <label class="control-label">Captain Cnic</label>
                 <input name="captain_cnic" value="{{ old('captain_cnic') }}" type="text" placeholder="captain_cnic" class="form-control">
               </div>

             </div> 

             <div class="row" style="margin: 10px 0px;">

              <div class="col-md-6">
                <label class="control-label">Remarks of Captain</label>
                <input name="remarks_captain" value="{{ old('remarks_captain') }}" type="text" placeholder="mole on neck" class="form-control">
              </div>

            </div> 

            <div class="row" style="margin: 10px 0px;">


              <div class="col-md-6">

              </div><!--form-group ends-->

            </div> 
          </div>

        </div>

      </div>

    </div>

    <div class="panel panel-primary basic_info_panel">
     <div class="panel-heading" style="padding: 20px 0px;background-color:#f26727;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-car" style="color: #fff;"> </i> <b style="color: #fff!important;
     text-transform: uppercase;">Vehicle Info</b></h4></div>
     <div class="panel-body">
      <div class="info_form_box"> 
       <div class="col-md-12">
         <div class="row">
          <div class="form-group col-md-4">
            <label class="control-label">Vehicle Category</label>
            <select class="form-control" id="vehicle" name="vehical_cat_id">
              <option >Choose Vehicle </option>
              @foreach ($vehicals as $i)
              <option value="{{$i->id}}">{{$i->name}} </option>
              @endforeach

            </select>
            @if ($errors->has('vehical_cat_id'))
            <div class="alert alert-danger" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                <span class="sr-only">Close</span>
              </button>
              <strong>Warning!</strong> {{$errors->first('vehical_cat_id')}}
            </div>
            @endif
          </div>

          {{-- <div class="form-group col-md-4">
            <label class="control-label">Vehicle Sub Categories</label>
            <select class="form-control" name="vehical_sub_id" >
                  <!-- <option >Choose Vehicle </option>
                    @foreach ($vehicals_categories as $i)
                  <option value="{{$i->id}}">{{$i->name}} </option>
                  @endforeach-->
                </select> 
                @if ($errors->has('vehical_sub_id'))
                <div class="alert alert-danger" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                  </button>
                  <strong>Warning!</strong> {{$errors->first('vehical_sub_id')}}
                </div>
                @endif
              </div> --}}
              <div class="form-group col-md-4">
                <label class="control-label">Vehicle No</label>
                <input type="text" name="vehicle_no" value="{{ old('vehicle_no') }}" placeholder="Vehicle No" class="form-control">
              </div>

            </div>    
            <div class="row">                                     
              <div class="form-group col-md-4">
                <label class="control-label">License No</label>
                <input type="text" value="{{ old('licence_no') }}" name="licence_no" placeholder="License No" class="form-control">
              </div>                          
              <div class="form-group col-md-4">
                <label class="control-label">Chesis No</label>
                <input type="text" value="{{ old('chesi_no') }}" placeholder="Chesis No" class="form-control" name="chesi_no">
              </div>
              <div class="form-group col-md-4">
                <label class="control-label">Engine No</label>
                <input name="engin_no" value="{{ old('engin_no') }}" type="text" placeholder="Engine No" class="form-control"> 
              </div>
            </div>
            <div class="row"> 
              <div class="form-group col-md-4">
                <label class="control-label">Vehicle Registration No</label>
                <input name="vehicle_registration" value="{{ old('vehicle_registration') }}" type="text" class="form-control" placeholder="Registration No">
              </div> 

              <div class="form-group col-md-4">
                <label class="control-label">Vehicle Name</label>
                <input name="vehicle_name" value="{{ old('vehicle_name') }}" type="text" class="form-control" placeholder="vehicle Name">
              </div> 

              <div class="form-group col-md-4">
                <label class="control-label">Remarks Vehicle</label>
                <input name="remarks_vehicle" value="{{ old('remarks_vehicle') }}" type="text" placeholder="white color" class="form-control"> 
              </div>

            </div>           
          </div> 
        </div>  
      </div>
    </div>


    <div class="panel panel-primary basic_info_panel">
     <div class="panel-heading" style="padding: 20px 0px;background-color:#f26727;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-user" style="color: #fff;"> </i> <b style="color: #fff!important;
     text-transform: uppercase;">Owner Info</b></h4></div>
     <div class="panel-body">

      <div class="form-group col-md-12">



        <div class="row">

          <div class="col-md-3" style="margin-left: 10px;">
           <div class="fileinput fileinput-new" data-provides="fileinput">
            <div class="fileinput-new thumbnail" style="width: 250px;">
              <!-- <div style="text-align: center;font-size:20px;margin:80px;">Profile<br>Photo -->
                <img src="{{asset('images/captain/profiles/no-image.png')}}" id="ownrprofile" height="150" width="150">
                <!-- </div> -->
              </div>
            </div>
            <div class="form-group">
             <label for="owner" class="btn btn-primary">Upload Profile</label>
             <input type="file"  id="owner" name="owner_image" class="hide" style="opacity: 0;">
           </div>
           <!-- cnic image upload here -->
           <div class="col-md-4">
            <div class="form-group" style="padding: 0px; ">
              <div class="owner_image"></div>
            </div>
          </div>
        </div>


        <!-- end the cnic image upload here -->

        <div>
          <div class="col-md-12">
            <div class="row">
              <div class="form-group col-md-4">
                <label class="control-label">Owner Name</label>
                <input name="owner_name" value="{{ old('owner_name') }}" type="text" placeholder="Vehicle Name" class="form-control">
              </div>
              <div class="form-group col-md-4">
               <label class="control-label">Owner Father Name</label>
               <input name="owner_fname" value="{{ old('owner_fname') }}" type="text" placeholder="Vehicle Name" class="form-control">
             </div>
             <div class="form-group col-md-4">
              <label class="control-label">Cnic No#</label>
              <input name="owner_cnic" value="{{ old('owner_cnic') }}" type="text" placeholder="35101-65088345-0" class="form-control">
            </div>   
          </div> 
          <div class="row">                       
            <div class="form-group col-md-4">

              <label class="control-label">Bank </label>
              <input name="bank" value="{{ old('bank') }}" type="text" placeholder="bank" class="form-control">
            </div>
            <div class="form-group col-md-4">
              <label class="control-label">Branch Code </label>
              <input name="branch_code" value="{{ old('branch_code') }}" type="text" placeholder="branch_code " class="form-control">
            </div>
            <div class="form-group col-md-4">
              <label class="control-label">Account No </label>
              <input name="account_no" value="{{ old('account_no') }}" type="text" placeholder="account no " class="form-control"> 
            </div>
          </div>
          <div class="row">
           <div class="form-group col-md-4">
            <label class="control-label">Remarks Owner</label>
            <input name="remarks_owner" value="{{ old('remarks_owner') }}" type="text" placeholder="mole on chin" class="form-control"> 
          </div>
        </div>
      </div>          


      <div class="form-group col-md-12">
        <div class="margiv-top-10 pull-right">
                      <!--  <button class="btn green" type="Submit" name="submit">Save</button>
                       <button class="btn red" type="reset">Cancel</button> -->
                     </div>

                   </div>
                 </div>
               </div>
             </div>






             <div class="panel panel-primary basic_info_panel">
              <div class="panel-heading" style="padding: 20px 0px;background-color:#f26727;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-car" style="color: #fff;"> </i> <b style="color: #fff!important;
              text-transform: uppercase;">All Documents</b></h4></div>
              
              <div class="panel-body">
                <div class="row" style="margin-top: 15px;">
                  <div class="col-md-4">
                    <div class="clsbox-1" runat="server">
                      <div class="dropzone clsbox" id="license_file_dropzone">
                        <div class="dz-message" data-dz-message><span>Click here or drag/drop License to upload.</span></div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="clsbox-1" runat="server">
                      <div class="dropzone clsbox" id="cnic_file_dropzone">
                        <div class="dz-message" data-dz-message><span>Click here or drag/drop CNIC to upload.</span></div>
                      </div>
                    </div>

                  </div>
                  <div class="col-md-4">
                    <div class="clsbox-1" runat="server">
                      <div class="dropzone clsbox" id="vehicle_reg_file_dropzone">
                        <div class="dz-message" data-dz-message><span>Click here or drag/drop Vehicle Registration to upload.</span></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row" style="margin-top: 15px; margin-bottom: 15px;">
                 <div class="col-md-4"></div>
                 <div class="col-md-4">
                  <div class="clsbox-1" runat="server">
                    <div class="dropzone clsbox" id="vehicle_file_dropzone">
                      <div class="dz-message" data-dz-message><span>Click here or drag/drop Vehicle Registration to upload.</span></div>
                    </div>
                  </div>
                </div>
                
               <!-- vehicle_image upload here -->
               <div class="col-md-4">
                <div class="form-group" style="padding: 0px; ">
                  <div class="vehicle_image"></div>
                </div>
              </div>
            </div>


            <!-- end the vehicle_image upload here -->
          </div> 
        </div> 
      </div>
    </div>

    <div class="panel panel-primary basic_info_panel">
      <div class="panel-heading" style="padding: 20px 0px;background-color:#f26727;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-car" style="color: #fff;"> </i> <b style="color: #fff!important;
      text-transform: uppercase;">Status</b></h4></div>

      <div class="panel-body">
        <div class="row">


          <div class="col-md-6" style="padding-top: 8px">
            <label class="control-label" style="padding-top: 0px !important">Status: <span class="req">*</span></label>
            <div style="padding-top: 20px">
              <label style="display:inline; padding-right: 35px">
                <input name="status" id="facility_used_for_active"  class="icheck facility_status" type="radio" value="1"  checked> Active </label>
                <label style="display:inline;">
                  <input name="status" id="facility_used_for_inactive" class="icheck facility_status" type="radio" value="0"  >  Inactive </label>
                </div>
              </div>

              <div class="col-md-6">
                <label class="control-label">Description: <span class="req"></span></label>
                <textarea  class="form-control" name="description" id="document_description" maxlength="150"></textarea>

              </div><!--form-group ends-->
            </div>
          </div>
        </div>


      </div><!-- end of panel group -->

      <div class="form-actions">
        <div class="row">
          <div class="col-md-offset-3 col-md-9">

           <button class="btn btn-success" type="Submit" name="submit">Save</button>
         </form>
         <a href="{{ route('captainResource.index')}}"class="btn  btn-primary" type="reset">Cancel</a>
                    <!-- <button class="btn blue" type="submit" id="submit_form">Add</button>
                    <a class="btn default"
                    href="admin/documents/index/">Cancel</a> -->
                  </div>
                </div>
              </div><!--form-actions ends-->
            </div><!--form-body ends-->
            <!--form ends-->
          </div><!--portlet-body form ends-->
        </div><!--portlet light ends-->



        @endsection
        @stack('post-styles')
        <link href="{{asset('assets/global/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css" />

        <style type="text/css">
        .form-actions {
          padding: 20px 20px;
          margin: 0;
          background-color: #f5f5f5;
          border-top: 1px solid #e7ecf1;
        }
      </style>
      @push('post-scripts')



      <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.js"></script>

      <script type="text/javascript">



  // start cnic


  CustomDropZone("license_file_dropzone","licenses","driving_licenes_images");
  CustomDropZone("cnic_file_dropzone","cnics","driving_cnic_images");
  CustomDropZone("vehicle_reg_file_dropzone","registration_image","vehicle_registration_image");
  CustomDropZone("vehicle_file_dropzone","vehicle_image","vehicle_image");


  function  CustomDropZone(drop_zone_id,folder_name,field_name) {
    Dropzone.autoDiscover = false;
    var temp_files=[];
    var uploaded_files = [];
    var myDropzone = new Dropzone('#'+drop_zone_id, {
      url: "{{ route('upload_document_file') }}",
      acceptedFiles: "image/*,application/pdf",
      maxFilesize: 50,
      uploadMultiple: true,
      createImageThumbnails: true,
      addRemoveLinks: true,
      maxFiles: null,
      processQueue: true,
      autoProcessQueue: true,
      removedfile: function(file){
       var file_name = file.previewElement.querySelector("[data-dz-name]").innerHTML;
     //var file_name = file.name;
     $.ajax({
       type: 'POST',
       url: "{{ route('remove_document_file') }}",
       data: {'folder_name':folder_name,file: file_name,'_token': $('meta[name="csrf-token"]').attr('content')},
       success: function(response){
        file.previewElement.remove();
        uploaded_files.pop(file_name);
        $('#'+field_name).val(uploaded_files);
        //$('#license_file').val('');
      }
    });

   },
   success: function(file,response){
    if(response.status){
      temp_files.push(file);
      if(temp_files.length==response.files_names.length){
        temp_files.forEach(function(row,index){
          var fileuploded = row.previewElement.querySelector("[data-dz-name]");
          fileuploded.innerHTML = response.files_names[index];
          uploaded_files.push(response.files_names[index]);
          $('#'+field_name).val(uploaded_files);
        });
      }
        //file.name = response.files_name;
        //var fileuploded = file.previewElement.querySelector("[data-dz-name]");
        //fileuploded.innerHTML = response.files_name;
       //console.log(file,response);
     }
   },
   init: function () {

     this.on('addedfile', function(file) {
      var ext = file.name.split('.').pop();
      if (ext == "pdf") {
        $(file.previewElement).find(".dz-image img").attr("src", "{{asset('images/pdf.png')}}");
        $('#file_ext').hide();
      }
      else if(ext == "doc" || ext == "docx") {
        $(file.previewElement).find(".dz-image img").attr("src", "{{asset('images/docx.png')}}");
        $('#file_ext').hide();
      }
      else{
        $('#file_ext').show();
      } 
    });
     this.on("thumbnail", function(file, dataUrl) {
      $('.dz-image').last().find('img').attr({width: '100%', height: '100%'});
    });
     this.on('sending', function(file, xhr, formData){
      temp_files = [];
      formData.append('folder_name',folder_name);
      formData.append('_token',$('meta[name="csrf-token"]').attr('content'));
    });
   }

 });

    return myDropzone;
  }


</script>


<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#profile-img-tag').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#images").change(function(){
    readURL(this);
  }); 

</script>


<script type="text/javascript">
  $('#owner').change(function(){
    var curElement = $(this).parent().parent().find('#ownrprofile');
    console.log(curElement);
    var reader = new FileReader();

    reader.onload = function (e) {
        // get loaded data and render thumbnail.
        curElement.attr('src', e.target.result);
      };

    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
  });
</script>
<script type="text/javascript">
  $('#vehicle_image1').change(function(){
    var curElement = $(this).parent().parent().find('#vehicle_image111');
    console.log(curElement);
    var reader = new FileReader();

    reader.onload = function (e) {
        // get loaded data and render thumbnail.
        curElement.attr('src', e.target.result);
      };

    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
  });
</script>
<script type="text/javascript">
  $("[name='vehical_cat_id']").on("change",function(){
    // $("#city").show();
    $("[name='vehical_sub_id']").html(` <option > Choose the Sub Category  </option>`);
    var vehicle  = $("#vehicle").val();

    $.ajax({
      method:"get",
      url:"{{route('vehicle-categories_data')}}",
      data : {id:vehicle},
      dataType:"json",
      success:function(data){
                            // console.log(data);
                            data.forEach(function(val,ind){

                             var id = val.id;
                             var name = val.name;
                             var option = `<option value="${id}">${name}</option>`;

                             $("[name='vehical_sub_id']").append(option);
                           });



                          }
                        });

  })
</script>

@endpush  