<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title></title>
<!-- Bootstrap Css -->
<!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->
<link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="{{asset('assets/public/css/style.css')}}" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

</head>
<body>
<div class="_style_VRmHw" data-reactid="81">
   <div data-reactid="82">
      <div class="_style_6xFKm" data-reactid="83">
         <nav title="" style="position:static;" role="navigation" dir="ltr" aria-label="Primary" aria-hidden="false" class="_style_4iw4w9" data-reactid="84">
           <a href="/" aria-label="Loader Home Page" class="_style_EWifa" data-reactid="85" style="padding-top: 5px!important;
    width: 115px;"><img src="{{asset('assets/pages/img/tod_logo.png')}}"></a>
            <div class="_style_sCxac _style_1eIon _style_1zoMpH" dir="ltr" data-reactid="93">
               <div class=" _style_1eIon" data-reactid="94">
                  <div role="navigation" aria-label="Ride" class="_style_3MQ8b" data-reactid="95">
                     <a class="_style_qlhRf" href="{{route('ride')}}">
                        <!-- react-text: 1087 -->Ride<!-- /react-text -->
                     </a>
                     <div data-reactid="98"></div>
                  </div>
                  <div role="navigation" aria-label="Drive" class="_style_3MQ8b" data-reactid="99">
                     <a class="_style_qlhRf" href="{{route('drive')}}">
                        <!-- react-text: 1089 -->Drive<!-- /react-text -->
                     </a>
                     <div></div>
                  </div>
               </div>
            </div>
            <div class="_style_2eHurn _style_1DpYnF" dir="ltr" data-reactid="103" style="">
               <div class="_style_1eIon" data-reactid="104">
                  <div class="" data-reactid="105">
                     <div role="" aria-label="Help" class="_style_2dGwNP" data-reactid="106">
                        <a href="#" class="_style_1C74C7">
                           <!-- react-text: 1091 -->Help<!-- /react-text -->
                        </a>
                        <div data-reactid="109"></div>
                     </div>
                     <div role="" aria-label="Sign In" class="_style_2dGwNP" data-reactid="110">
                        <a href="{{route('sign-in')}}" class="_style_1C74C7">
                           <!-- react-text: 1093 -->Sign In<!-- /react-text -->
                        </a>
                        <div data-reactid="113"></div>
                     </div>
                  </div>
               </div>
            </div>
            <div data-reactid="114">
               <div class="_style_3BKzMt _style_2om04a" data-reactid="115">
                  <div class="_style_24hvbj" data-reactid="116">
                     <div class="_style_kNTDA" role="button" tabindex="0" data-reactid="117">
                     <svg viewBox="0 0 64 64" width="20px" height="20px" class=" _style_4wJp4e" data-reactid="118"><path d="M32.0000114,4c-11.0449219,0-20,8.8027344-20,20c0,6.0996094,2,11.03125,6,16.9375l13.1807709,18.6359062 C31.3796368,59.8575249,31.6893368,59.9997215,31.9991455,60c0.3104134,0.0002785,0.6209373-0.141922,0.820179-0.4265938 L46.0000114,40.9375c4-5.90625,6-10.8378906,6-16.9375C52.0000114,12.8027344,43.0449333,4,32.0000114,4z M32.0000114,36.25 c-6.7655029,0-12.25-5.484375-12.25-12.25s5.4844971-12.25,12.25-12.25s12.25,5.484375,12.25,12.25 S38.7655144,36.25,32.0000114,36.25z" data-reactid="119" fill="#f16622"></path></svg>
                        <span class="_style_17XNsa" data-reactid="120">Lahore</span>
                     </div>
                  </div>
                  <div class="_style_24hvbj">
                     <a class="btn btn--primary _style_PkWok dotcom-cta-button btn--small _style_4AsDBA" href="#">
                        <!-- react-text: 1096 -->Become a driver<!-- /react-text -->
                     </a>
                  </div>
               </div>
            </div>
            <a href="#" dir="ltr" aria-expanded="false" aria-label="Expand Side Navigation" class="_style_2A5F76" data-reactid="121">
             <!--   <div class="_style_3jefWi" data-reactid="122">
                  <div class="_style_1iGbRV" data-reactid="123">
                     <div class="_style_1VXCWP" data-reactid="124"></div>
                  </div>
                  <div class="_style_4eg2rR" data-reactid="125">
                     <div class="_style_3hzhVq" data-reactid="126"></div>
                  </div>
                  <div class="_style_3AFpNc" data-reactid="127">
                     <div class="_style_3hzhVq" data-reactid="128"></div>
                  </div>
               </div> -->
                <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                <span class="hamb-top"></span>
            <span class="hamb-middle"></span>
            <span class="hamb-bottom"></span>
            </button>
            </a>
         </nav>
      </div>
   </div>
   <div id="wrapper">
   <div class="overlay"></div>
    
        <!-- Sidebar -->
        <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
            <ul class="nav sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">
                      <img width="60%" src="{{asset('assets/pages/img/tod_logo.png')}}">
                    </a>
                </li>
                <li class="dropdown">
                  <a href="http://loader.aktechzone.com/ride" class="dropdown-toggle" data-toggle="dropdown">Ride <span class="caret"></span></a>
                  <ul class="dropdown-menu show" role="menu">
                    <li><a href="http://loader.aktechzone.com/ride">Overview</a></li>
                    <li><a href="{{route('fare-estimate.index')}}">Fare Estimator</a></li>
                    <!-- <li><a href="#">Another action</a></li>
                    <li><a href="#">Cities</a></li> -->
                    <li><a href="{{route('how-it-work')}}">How it works</a></li>
                    <li><a href="{{route('ride-safety')}}">Safety</a></li>
                  </ul>
                </li>
                 <li class="dropdown">
                  <a href="http://loader.aktechzone.com/drive" class="dropdown-toggle" data-toggle="dropdown">Drive <span class="caret"></span></a>
                  <ul class="dropdown-menu show" role="menu">
                    <li><a href="http://loader.aktechzone.com/drive">Overview</a></li>
                    <li><a href="{{route('drive-app')}}">Driver App</a></li>
                    <!-- <li><a href="#">Driving in your City</a></li> -->
                    <li><a href="{{route('drive-requirement')}}">Requirements</a></li>
                    <li><a href="{{route('drive_safety')}}">Safety</a></li>
                  </ul>
                </li>
            </ul>
        </nav>
     </div>
   <div aria-hidden="true" style="transform:scaleY(0);" class="_style_2xq251" data-reactid="129">
      <div class="_style_4BPFJ1" data-reactid="130">
         <div class="_style_1TC7Ps" data-reactid="131">
            <div class="_style_VRmHw" data-reactid="132">
               <div class="_style_1fRwDe" data-reactid="133">
                  <div class="_style_3ZWbRB" data-reactid="134">
                     <div class="_style_3YC6q7" data-reactid="135">
                        <div data-reactid="136">
                           <a href="#" class="_style_3uftU1" data-reactid="137">
                              <h6 class="_style_3WtBKm _style_6TpIw" data-reactid="138">Ride</h6>
                              <span class="_style_xLfxc" data-reactid="139">
                                 <svg viewBox="0 0 64 64" width="16px" height="16px" class=" _style_4wJp4e" data-reactid="140">
                                    <path d="M53.151 24.575L36.888 40.84l-4.243 4.242a1 1 0 0 1-1.414 0l-4.243-4.242-16.263-16.264a1 1 0 0 1 0-1.414l4.243-4.242a1 1 0 0 1 1.414 0l15.556 15.556 15.557-15.556a1 1 0 0 1 1.414 0l4.242 4.242a1 1 0 0 1 0 1.414z" data-reactid="141"></path>
                                 </svg>
                              </span>
                           </a>
                           <div class="_style_3O2TtY" data-reactid="142">
                              <div class="_style_31RGeY" aria-hidden="true" data-reactid="143">
                                 <div class="_style_2SK1zv" data-reactid="144">
                                    <div class="_style_2gJSG2" data-reactid="145">
                                       <a class="_style_4jjUVj" href="#">
                                          <!-- react-text: 1098 -->Overview<!-- /react-text -->
                                       </a>
                                    </div>
                                    <div class="_style_2gJSG2" data-reactid="148">
                                       <a class="_style_4jjUVj" href="#">
                                          <!-- react-text: 1100 -->How it Works<!-- /react-text -->
                                       </a>
                                    </div>
                                    <div class="_style_2gJSG2" data-reactid="151">
                                       <a class="_style_4jjUVj" href="{{route('fare-estimate.index')}}
                                       ">
                                          <!-- react-text: 1102 -->Fare Estimator<!-- /react-text -->
                                       </a>
                                    </div>
                                    <div class="_style_2gJSG2" data-reactid="154">
                                       <a class="_style_4jjUVj" href="#">
                                          <!-- react-text: 1104 -->Safety<!-- /react-text -->
                                       </a>
                                    </div>
                                    <div class="_style_2gJSG2" data-reactid="157">
                                       <a class="_style_4jjUVj" href="#">
                                          <!-- react-text: 1106 -->Cities<!-- /react-text -->
                                       </a>
                                    </div>
                                    <div class="_style_2gJSG2" data-reactid="160">
                                       <a class="_style_4jjUVj" href="#">
                                          <!-- react-text: 1108 -->Airports<!-- /react-text -->
                                       </a>
                                    </div>
                                    <div class="_style_2gJSG2" data-reactid="163">
                                       <a target="_blank" href="#" class="_style_4jjUVj">
                                          <!-- react-text: 1110 -->Business<!-- /react-text -->
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="_style_3YC6q7" data-reactid="166">
                        <div data-reactid="167">
                           <a href="#" class="_style_3uftU1" data-reactid="168">
                              <h6 class="_style_3WtBKm _style_6TpIw" data-reactid="169">Drive</h6>
                              <span class="_style_xLfxc" data-reactid="170">
                                 <svg viewBox="0 0 64 64" width="16px" height="16px" class=" _style_4wJp4e" data-reactid="171">
                                    <path d="M53.151 24.575L36.888 40.84l-4.243 4.242a1 1 0 0 1-1.414 0l-4.243-4.242-16.263-16.264a1 1 0 0 1 0-1.414l4.243-4.242a1 1 0 0 1 1.414 0l15.556 15.556 15.557-15.556a1 1 0 0 1 1.414 0l4.242 4.242a1 1 0 0 1 0 1.414z" data-reactid="172"></path>
                                 </svg>
                              </span>
                           </a>
                           <div class="_style_3O2TtY" data-reactid="173">
                              <div class="_style_31RGeY" aria-hidden="true" data-reactid="174">
                                 <div class="_style_2SK1zv" data-reactid="175">
                                    <div class="_style_2gJSG2" data-reactid="176">
                                       <a class="_style_4jjUVj" href="#">
                                          <!-- react-text: 1112 -->Overview<!-- /react-text -->
                                       </a>
                                    </div>
                                    <div class="_style_2gJSG2" data-reactid="179">
                                       <a class="_style_4jjUVj" href="#">
                                          <!-- react-text: 1114 -->Requirements<!-- /react-text -->
                                       </a>
                                    </div>
                                    <div class="_style_2gJSG2" data-reactid="182">
                                       <a class="_style_4jjUVj" href="#">
                                          <!-- react-text: 1116 -->Driver App<!-- /react-text -->
                                       </a>
                                    </div>
                                    <div class="_style_2gJSG2" data-reactid="185">
                                       <a class="_style_4jjUVj" href="#">
                                          <!-- react-text: 1118 -->Safety<!-- /react-text -->
                                       </a>
                                    </div>
                                    <div class="_style_2gJSG2" data-reactid="188">
                                       <a class="_style_4jjUVj" href="#">
                                          <!-- react-text: 1120 -->Local Driver Guide<!-- /react-text -->
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="_style_1BcZgH" data-reactid="191">
                        <div data-reactid="192">
                           <a href="#" class="_style_3uftU1" data-reactid="193">
                              <h6 class="_style_3WtBKm _style_6TpIw" data-reactid="194">Eats and more</h6>
                              <span class="_style_xLfxc" data-reactid="195">
                                 <svg viewBox="0 0 64 64" width="16px" height="16px" class=" _style_4wJp4e" data-reactid="196">
                                    <path d="M53.151 24.575L36.888 40.84l-4.243 4.242a1 1 0 0 1-1.414 0l-4.243-4.242-16.263-16.264a1 1 0 0 1 0-1.414l4.243-4.242a1 1 0 0 1 1.414 0l15.556 15.556 15.557-15.556a1 1 0 0 1 1.414 0l4.242 4.242a1 1 0 0 1 0 1.414z" data-reactid="197"></path>
                                 </svg>
                              </span>
                           </a>
                           <div class="_style_3O2TtY" data-reactid="198">
                              <div class="_style_31RGeY" aria-hidden="true" data-reactid="199">
                                 <div class="_style_2SK1zv" data-reactid="200">
                                    <div class="_style_4bumBK" data-reactid="201">
                                       <a target="_blank" href="#" class="_style_4jjUVj">
                                          <!-- react-text: 1122 -->Uber Eats<!-- /react-text -->
                                       </a>
                                    </div>
                                    <div class="_style_4bumBK" data-reactid="204">
                                       <a target="_blank" href="#" class="_style_4jjUVj">
                                          <!-- react-text: 1124 -->Uber for Business<!-- /react-text -->
                                       </a>
                                    </div>
                                    <div class="_style_4bumBK" data-reactid="207">
                                       <a target="_blank" href="#" class="_style_4jjUVj">
                                          <!-- react-text: 1126 -->Uber Freight<!-- /react-text -->
                                       </a>
                                    </div>
                                    <div class="_style_4bumBK" data-reactid="210">
                                       <a target="_blank" href="#" class="_style_4jjUVj">
                                          <!-- react-text: 1128 -->Uber Health<!-- /react-text -->
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="_style_4fTbsV" data-reactid="213">
                     <div class="_style_1PVYU0" data-reactid="214">
                        <div class="_style_EcSkz" data-reactid="215">
                           <div class="_style_bJmtY" data-reactid="216">
                              <div class="_style_2f192X" data-reactid="217">
                                 <a class="_style_qYAtG" href="#" data-reactid="218">
                                    <svg viewBox="0 0 64 64" width="16px" height="16px" class="_style_30FFBp _style_4wJp4e" data-reactid="219">
                                       <path d="M32.0000114,4c-11.0449219,0-20,8.8027344-20,20c0,6.0996094,2,11.03125,6,16.9375l13.1807709,18.6359062 C31.3796368,59.8575249,31.6893368,59.9997215,31.9991455,60c0.3104134,0.0002785,0.6209373-0.141922,0.820179-0.4265938 L46.0000114,40.9375c4-5.90625,6-10.8378906,6-16.9375C52.0000114,12.8027344,43.0449333,4,32.0000114,4z M32.0000114,36.25 c-6.7655029,0-12.25-5.484375-12.25-12.25s5.4844971-12.25,12.25-12.25s12.25,5.484375,12.25,12.25 S38.7655144,36.25,32.0000114,36.25z" data-reactid="220"></path>
                                    </svg>
                                    <!-- react-text: 221 -->Lahore<!-- /react-text -->
                                 </a>
                              </div>
                           </div>
                           <div class="_style_bJmtY" data-reactid="222">
                              <div class="_style_2f192X _style_2oqcgf" data-reactid="223">
                                 <a class="_style_qYAtG" href="#" data-reactid="224">
                                    <!-- react-text: 227 -->English<!-- /react-text -->
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="_style_1PVYU0" data-reactid="228">
                        <div class="_style_EcSkz" data-reactid="229">
                           <div class="_style_26k48m" data-reactid="230">
                              <div class="_style_2f192X" data-reactid="231">
                                 <span data-reactid="232">Follow Us</span>
                                 <div class="_style_1l7iHD" data-reactid="233"><span class="_style_414tT0" data-reactid="234"><a class="_style_1YDFo9" href="#" data-reactid="235"><span class="_style_2vszYG" data-reactid="236">Facebook</span><span style="display:inline-block;" data-reactid="237"></span></a></span><span class="_style_414tT0" data-reactid="240"><a class="_style_1YDFo9" href="#" data-reactid="241"><span class="_style_2vszYG" data-reactid="242">Twitter</span><span style="display:inline-block;" data-reactid="243"></span></a></span><span class="_style_414tT0" data-reactid="246"><a class="_style_1YDFo9" href="#" data-reactid="247"><span class="_style_2vszYG" data-reactid="248">LinkedIn</span><span style="display:inline-block;" data-reactid="249"></span></a></span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

@yield('content')



 @section('javascripts')


        <footer class="dotcom-footer dotcom-footer-cities _style_1oTOob" data-reactid="473">
   <!-- react-empty: 687 -->
   <div data-reactid="474">
      <div class="dotcom-footer-city-picker">
         <div class="_style_VRmHw">
            <div class="_style_eeI8o">
               <div class="soft--bottom">
                  <h3 class="flush">
                     <div>Loader is in Lahore</div>
                     <div class="primary-font--thin">and 3 more cities of Pakistan
</div>
                  </h3>
               </div>
               <div class="_style_11O4RF app-footer__city-picker two-fifths palm-one-whole">
                  <div class="autocomplete-container">
                     <div class="autocomplete position--relative">
                        <div class="autocomplete__input hard flush--bottom autocomplete__input--icon">
                           <div><input value="" placeholder="Find a City" autocomplete="off" aria-label="Find a city by entering a city, city and state, or city and country"></div>
                        </div>
                     </div>
                  </div>
                  <button class="_style_3ZS4hm _style_16HNh4" kind="primary" style="line-height: 0px; padding: 13px; background-color: rgb(26, 26, 26); border-color: rgb(26, 26, 26); height: 44px;">
                     <svg viewBox="0 0 64 64" width="16px" height="16px" class=" _style_4wJp4e">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M59.9270592,31.9847012L60,32.061058L43.7665291,49.1333275l-3.2469215-3.5932007 L51.3236885,34H4v-4h47.3943481L40.5196075,18.4069672l3.2469215-3.4938126L60,31.946312L59.9270592,31.9847012z"></path>
                     </svg>
                     <span class="visually-hidden">City Search</span>
                  </button>
               </div>
            </div>
         </div>
      </div>
      <div class="dotcom-footer-city-illustration">
         <svg class="_style_1l1WBH" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1560 197" height="197" width="1560" preserveAspectRatio="xMinYMax meet">
            <defs>
               <pattern id="a" data-name="2 3 - black" width="5" height="5" patternTransform="translate(726 27)" patternUnits="userSpaceOnUse" viewBox="0 0 5 5">
                  <path fill="none" d="M0 0h5v5H0z"></path>
                  <path fill="none" d="M0 0h5v5H0z"></path>
                  <path fill="none" d="M0 0h5v5H0V0z"></path>
                  <path d="M2 2H0V0h2v2z"></path>
               </pattern>
               <pattern id="b" data-name="SVGID 1" width="5" height="5" patternTransform="translate(841.4 310.4)" patternUnits="userSpaceOnUse" viewBox="0 0 5 5">
                  <path fill="none" d="M0 0h5v5H0z"></path>
                  <path fill="none" d="M0 0h5v5H0z"></path>
                  <path fill="none" d="M0 0h5v5H0V0z"></path>
                  <path d="M2 2H0V0h2v2z"></path>
               </pattern>
               <pattern id="c" data-name="2 3 - black" width="5" height="5" patternTransform="translate(726 30)" patternUnits="userSpaceOnUse" viewBox="0 0 5 5">
                  <path fill="none" d="M0 0h5v5H0z"></path>
                  <path fill="none" d="M0 0h5v5H0z"></path>
                  <path fill="none" d="M0 0h5v5H0V0z"></path>
                  <path d="M2 2H0V0h2v2z"></path>
               </pattern>
            </defs>
            <path d="M1498.22 54H1361.5a5.5 5.5 0 0 1 0-11h50.6a23.92 23.92 0 0 0 16.9-7 23.92 23.92 0 0 1 16.9-7h19.33a194.76 194.76 0 0 0-100.59-28H1137c-11.9 0-23.2 3.9-32 12a49.47 49.47 0 0 1-32.8 13H969.9c-22.3 0-43.1 7.5-58.9 22s-37.7 23-60 23H720a137.73 137.73 0 0 0-94 37c-26.1 24.4-61.8 38-98.9 38H510c-26.2 0-50.7 10.2-70 28l-26 23h1146a195.13 195.13 0 0 0-61.78-143zM1297 66a17 17 0 1 1 17-17 17 17 0 0 1-17 17zm162 34h-112a5 5 0 0 1 0-10h15.3a30.48 30.48 0 0 0 21.7-9 30.66 30.66 0 0 1 21.7-9h53.3a14 14 0 0 1 0 28z" class="_style_2f8elF"></path>
            <path d="M1536 173a148 148 0 0 0-104-43h-111v67h239zm-613-21h-17v-38a73.89 73.89 0 0 0 17-2v40zm271-80a125.64 125.64 0 0 1 90 37.2V153h-90V72zm-172-1v14l-14-14h14zM574 197h-45c16.4 0 31.9-5.6 45-15v15z"></path>
            <path fill="#fff" d="M840 32v9h-10v156h48V42h5V32h-43zm115 120h-67l45 45h22v-45zm-32-61h32v61h-32z"></path>
            <path fill="#d6d6d6" d="M928 98h27v2h-27zm0 6h27v2h-27zm0 6h27v2h-27zm0 6h27v2h-27zm0 6h27v2h-27zm0 6h27v2h-27zm0 6h27v2h-27zm0 6h27v2h-27z"></path>
            <path fill="#fff" d="M932 85h36v6h-36z"></path>
            <path class="_style_4bynJ1" d="M932 85l19-19h17v19h-36z"></path>
            <path class="_style_3uGAF5" d="M1022 85l-19-19h-35v19h54z"></path>
            <path class="_style_4bynJ1" d="M979 79v-6h-6l6 6zm13 0v-6h-6l6 6zm13 0v-6h-6l6 6z"></path>
            <path fill="#fff" d="M951 63h17v3h-17zm104 107l-55-55h55v55z"></path>
            <path fill="#d6d6d6" d="M1045 121h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm16 13h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm16 13h2v7h-2zm-14 1l-1-1h1v1zm8 6h-1l-1-1v-6h2v7z"></path>
            <path class="_style_3uGAF5" d="M679 140h21v8h-21zm0-14h21v8h-21zm0-14h21v8h-21zm0-14v8h24l-8-8h-16z"></path>
            <path fill="#d6d6d6" d="M679 134h21v6h-21zm0-14h21v6h-21zm0-14h17v6h-17z"></path>
            <path class="_style_4bynJ1" d="M679 140v8h-55l8-8h47zm0-14v8h-44l8-8h36zm0-14v8h-35l8-8h27zm0-14v8h-25l8-8h17z"></path>
            <path fill="#fff" d="M643 134h36v6h-36zm9-14h27v6h-27zm10-14h17v6h-17zm634 87l-40-40h-10v-38h50v78z"></path>
            <path class="_style_3uGAF5" d="M1296 107v8h26l-8-8h-18z"></path>
            <path class="_style_4bynJ1" d="M1296 107v8h-50l8.4-8h41.6z"></path>
            <path fill="#d6d6d6" d="M1290 120h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm40 13h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm40 13h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm-494 16h74v35h-74z"></path>
            <path fill="url(#a)" d="M756 162h74v35h-74z"></path>
            <path d="M756 32v48h-17v6h-25a14 14 0 0 0-14 14v97h56V86h32V0zm212 17h2v14h-2z" fill="#fff"></path>
            <path class="_style_3uGAF5" d="M970 49h6v2h-6z"></path>
            <path fill="#fff" d="M985 49h2v14h-2z"></path>
            <path class="_style_3uGAF5" d="M987 49h6v2h-6z"></path>
            <path fill="#fff" d="M1001 49h2v14h-2z"></path>
            <path class="_style_3uGAF5" d="M1003 49h6v2h-6z"></path>
            <path fill="#d6d6d6" d="M749 167h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm16 13h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm155-28h18V42h-5V32h-18v10h-5v155h55l-45-45z"></path>
            <path fill="url(#b)" d="M603 148h41v49h-41z"></path>
            <path fill="#d6d6d6" d="M603 148v49h97v-49h-97zM840 18h2v14h-2zm6 0h2v14h-2z"></path>
            <path d="M1045 13a13.38 13.38 0 0 0-13 13h26a13.38 13.38 0 0 0-13-13z" class="_style_4bynJ1"></path>
            <path d="M1045 13v13h13a13.38 13.38 0 0 0-13-13z" class="_style_3uGAF5"></path>
            <path fill="#fff" d="M1047 111V29h-2v-3h-20v3h-3v82h25z"></path>
            <path fill="#d6d6d6" d="M1041 34h2v7h-2zm0 11h2v7h-2zm0 11h2v7h-2zm0 11h2v7h-2zm0 11h2v7h-2zm-5-44h2v7h-2zm0 11h2v7h-2zm0 11h2v7h-2zm0 11h2v7h-2zm0 11h2v7h-2zm-5-44h2v7h-2zm0 11h2v7h-2zm0 11h2v7h-2zm0 11h2v7h-2zm0 11h2v7h-2zm-5-44h2v7h-2zm0 11h2v7h-2zm0 11h2v7h-2zm0 11h2v7h-2zm0 11h2v7h-2z"></path>
            <path fill="#fff" d="M1142 38h26v100h-26z"></path>
            <path d="M1194 138h-26V38a26 26 0 0 1 26 26v74z" fill="#d6d6d6"></path>
            <path fill="#fff" d="M1123 153h61v44h-61z"></path>
            <path fill="#d6d6d6" d="M1184 153h72v44h-72z"></path>
            <path d="M1199 166a3 3 0 0 0-3 3v28h6v-28a3 3 0 0 0-3-3zm14 0a3 3 0 0 0-3 3v28h6v-28a3 3 0 0 0-3-3zm14 0a3 3 0 0 0-3 3v28h6v-28a3 3 0 0 0-3-3zm14 0a3 3 0 0 0-3 3v28h6v-28a3 3 0 0 0-3-3z" class="_style_3uGAF5"></path>
            <path d="M1145 166a3 3 0 0 0-3 3v28h6v-28a3 3 0 0 0-3-3zm14 0a3 3 0 0 0-3 3v28h6v-28a3 3 0 0 0-3-3zm14 0a3 3 0 0 0-3 3v28h6v-28a3 3 0 0 0-3-3z" fill="#d6d6d6"></path>
            <path d="M727 162v-4h5a17.39 17.39 0 0 0 12-5l2-2h10v11h-29z" class="_style_4bynJ1"></path>
            <path d="M830 162v-4h-4c-4.6 0-9.8-1.8-13-5l-2-2h-55v11h74zm354-9h72a50.17 50.17 0 0 0-36-15 50.83 50.83 0 0 0-36 15z" class="_style_3uGAF5"></path>
            <path fill="#d6d6d6" d="M1300 197l-44-44v44h44zM1142 68h26v2h-26zm0 6h26v2h-26zm0 6h26v2h-26zm0 6h26v2h-26zm0 6h26v2h-26zm0 6h26v2h-26zm0 6h26v2h-26zm0 6h26v2h-26z"></path>
            <path class="_style_3uGAF5" d="M1168 68h26v2h-26zm0 6h26v2h-26zm0 6h26v2h-26zm0 6h26v2h-26zm0 6h26v2h-26zm0 6h26v2h-26zm0 6h26v2h-26zm0 6h26v2h-26z"></path>
            <path d="M1168 41v26h26a26.08 26.08 0 0 0-26-26z" transform="translate(0 -3)" fill="url(#c)"></path>
            <path fill="#d6d6d6" d="M1335 162h35v35h-35z"></path>
            <path fill="url(#a)" d="M1335 162h35v35h-35z"></path>
            <path class="_style_4bynJ1" d="M1335 162h-26l13-13 13 13z"></path>
            <path class="_style_3uGAF5" d="M1357 149h-35l13 13h35l-13-13zm-493 30v2a5.06 5.06 0 0 1-1.7 3.3l-2 2-.3.3V179h-2v18h2v-5.5a6.29 6.29 0 0 1 1.7-3.8l2-2a7 7 0 0 0 2.3-4.7v-2h-2zm77 7h2v11h-2zm-227 0h2v11h-2z"></path>
            <path fill="#d6d6d6" d="M968 63h35v3h-35z"></path>
            <path d="M490 197a42.44 42.44 0 0 1-30.5-12.9C449.6 174.2 436 168 422 168h-38c-8 0-15.3-2.3-21-8a31.79 31.79 0 0 0-22-9h-80a41.11 41.11 0 0 1-29-12 41.11 41.11 0 0 0-29-12H100a72.29 72.29 0 0 0-51 21L0 197h490z"></path>
            <path d="M1297 27a22 22 0 1 0 22 22 22 22 0 0 0-22-22zm0 39a17 17 0 1 1 17-17 17 17 0 0 1-17 17z" class="_style_2f8elF"></path>
            <path class="_style_3uGAF5" d="M647 151v32h2v-30l-2-2zm8 8v24h2v-22l-2-2zm8 8v16h2v-14l-2-2zm8 8v8h2v-6l-2-2z"></path>
            <path d="M603 148h-17.8a11.18 11.18 0 0 0-11.2 11.16V197h29v-49z" fill="#fff"></path>
            <path class="_style_3uGAF5" d="M685 151h2v32h-2zm7 0h2v32h-2zM883 48h7v2h-7zm11 0h7v2h-7zm-11 6h7v2h-7zm11 0h7v2h-7zm-11 6h7v2h-7zm11 0h7v2h-7zm-11 6h7v2h-7zm11 0h7v2h-7zm-11 6h7v2h-7zm11 0h7v2h-7zm-11 6h7v2h-7zm11 0h7v2h-7zm-11 6h7v2h-7zm11 0h7v2h-7zm-11 6h7v2h-7zm11 0h7v2h-7zm-11 6h7v2h-7zm11 0h7v2h-7zm-11 6h7v2h-7zm11 0h7v2h-7zm-11 6h7v2h-7zm11 0h7v2h-7zm-11 6h7v2h-7zm11 0h7v2h-7zm-11 6h7v2h-7zm11 0h7v2h-7zm-11 6h7v2h-7zm11 0h7v2h-7zm-11 6h7v2h-7zm11 0h7v2h-7zm-11 6h7v2h-7zm11 0h7v2h-7zm-11 6h7v2h-7zm11 0h7v2h-7zm229-6h19v15h-19z"></path>
            <path d="M1220 138h-78v15h42a50.83 50.83 0 0 1 36-15z" class="_style_4bynJ1"></path>
            <path fill="#d6d6d6" d="M1335 173h-13v-58h-26v82h39v-24z"></path>
            <path class="_style_3uGAF5" d="M1316 120h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm16 13h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm16 13h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2z"></path>
            <path d="M826 158h4v-44h-12v42.3a20.53 20.53 0 0 0 8 1.7z"></path>
            <path d="M813 153a16.56 16.56 0 0 0 4.3 3h.7V0h-30v86h-32v65h55zm-99-60h42v2h-42zm0 6h42v2h-42zm0 6h42v2h-42zm0 6h42v2h-42zm0 6h42v2h-42zm0 6h42v2h-42zm0 6h42v2h-42zm0 6h42v2h-42z" fill="#d6d6d6"></path>
            <path class="_style_3uGAF5" d="M756 93h34v2h-34zm0 6h34v2h-34zm0 6h34v2h-34zm0 6h34v2h-34zm0 6h34v2h-34zm0 6h34v2h-34zm0 6h34v2h-34zm0 6h34v2h-34zM796 0h2v137h-2zm6 0h2v137h-2zm6 0h2v137h-2z"></path>
            <path fill="#fff" d="M644 148h35v35l-35-35z"></path>
            <path fill="#d6d6d6" d="M649 153v-2h-2l2 2zm8 8v-10h-2v8l2 2zm8 8v-18h-2v16l2 2zm8 8v-26h-2v24l2 2z"></path>
            <path d="M1123 72h19v66h-19z"></path>
            <path fill="#d6d6d6" d="M1062 29v-3h-17v3h2v80l-18-18h-7v-6h-54v6h-13v106h100v-27l-55-55h65V29h-3z"></path>
            <path class="_style_3uGAF5" d="M1060 34h2v7h-2zm0 11h2v7h-2zm-5-11h2v7h-2zm0 11h2v7h-2zm-5-11h2v7h-2zm0 11h2v7h-2zm-95 53h74v2h-74zm0 6h74v2h-74zm0 6h74v2h-74zm0 6h45v2h-45zm0 6h45v2h-45zm0 6h45v2h-45zm0 6h45v2h-45zm0 6h45v2h-45z"></path>
            <path d="M1024.3 158.6a2 2 0 0 0-3.3 1.4v11l-12.7-12.4a2 2 0 0 0-3.3 1.4v11l-12.7-12.4a2 2 0 0 0-3.3 1.4v11l-12.7-12.4a2 2 0 0 0-3.3 1.4v37h64v-26z" fill="#fff"></path>
            <path fill="#d6d6d6" d="M977 177h7v2h-7zm11 0h7v2h-7zm11 0h7v2h-7zm11 0h7v2h-7zm-33 6h7v2h-7zm11 0h7v2h-7zm11 0h7v2h-7zm11 0h7v2h-7zm-33 6h7v2h-7zm11 0h7v2h-7zm11 0h7v2h-7zm11 0h7v2h-7z"></path>
            <path class="_style_3uGAF5" d="M1043 177h7v2h-7zm0 6h7v2h-7zm0 6h7v2h-7z"></path>
            <path fill="#d6d6d6" d="M1021 147h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2z"></path>
            <path class="_style_3uGAF5" d="M1029 147h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2z"></path>
            <path fill="#d6d6d6" d="M1013 134h2v7h-2zm-8 0h2v7h-2z"></path>
            <path class="_style_3uGAF5" d="M1013 134h2v7h-2zm-8 0h2v7h-2zm0-13v7h2v-6l-1-1h-1z"></path>
            <path fill="#d6d6d6" d="M1021 121h2v7h-2zm-8 0h2v7h-2zm10 20l-2-2v-5h2v7z"></path>
            <path class="_style_3uGAF5" d="M1039 154h-2v-2l2 2z"></path>
            <path fill="url(#a)" d="M603 148h41v49h-41z"></path>
            <path fill="#fff" d="M1335 162h-26l26 26v-26z"></path>
            <path class="_style_4bynJ1" d="M1335 162h-26l13-13 13 13z"></path>
            <path fill="#d6d6d6" d="M1329 166h2v7h-2zm-8 0h2v7h-2z"></path>
            <path d="M1293 197c6.2 0 13.6-2.6 18-7a28 28 0 0 1 20-8h20a17.84 17.84 0 0 0 12-5c2-2 4.4-4.2 7-5v25h-77z"></path>
            <path class="_style_3uGAF5" d="M1023 141h-2v-5l2 2v3z"></path>
            <path d="M718.5 186h-10a3.54 3.54 0 0 1-3.5-3.5 3.54 3.54 0 0 1 3.5-3.5h1.1a3.49 3.49 0 0 0 2.4-1 3.49 3.49 0 0 1 2.4-1h4.1a4.48 4.48 0 0 1 4.5 4.46 4.48 4.48 0 0 1-4.46 4.5zM848 175.5a3.54 3.54 0 0 1 3.5-3.5 6 6 0 0 0 4.3-1.8l.2-.2a6.73 6.73 0 0 1 4.8-2h7.7a5.55 5.55 0 0 1 5.5 5.5 5.55 5.55 0 0 1-5.5 5.5h-17a3.54 3.54 0 0 1-3.5-3.5zm88.5 10.5H948a3 3 0 0 0 3-3 3 3 0 0 0-3-3h-.9a4.91 4.91 0 0 1-3.6-1.5 5.07 5.07 0 0 0-3.6-1.5h-3.4a4.4 4.4 0 0 0-4.4 4.4v.1a4.34 4.34 0 0 0 4.18 4.5h.22z" class="_style_4bynJ1"></path>
            <path fill="#d6d6d6" d="M1007 122l-1-1h1v1z"></path>
            <path d="M1099 50h-24a19.93 19.93 0 0 0-20 19.86V197h44V50z" fill="#fff"></path>
            <path fill="#d6d6d6" d="M1072 58h2v57h-2zm7 0h2v57h-2zm9 63l-2-2V58h2v63zm7 7l-2-2V58h2v70z"></path>
            <path fill="#d6d6d6" d="M1123 153V50h-24v81l-16-16h-28v82h112l-44-44z"></path>
            <path class="_style_3uGAF5" d="M1088 151h-2v-33l2 2v31zm7 0h-2v-26l2 2v24zm-17-30h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm16 13h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm16 13h2v7h-2zm-8 0h2v7h-2zm-8 0h2v7h-2zm20 32v2a5.06 5.06 0 0 1-1.7 3.3l-2 2-.3.3V179h-2v18h2v-5a7.43 7.43 0 0 1 1.7-4.3l2-2a7 7 0 0 0 2.3-4.7v-2h-2z"></path>
            <path d="M1066 175.6a3.46 3.46 0 0 1 3.5-3.4h1.2a4 4 0 0 0 2.5-1l2.2-2.1a3.74 3.74 0 0 1 2.5-1h8.6a5.57 5.57 0 0 1 5.6 5.5 5.57 5.57 0 0 1-5.6 5.5h-16.9a3.63 3.63 0 0 1-3.6-3.5z" fill="#fff"></path>
            <path class="_style_3uGAF5" d="M1106 186h2v11h-2z"></path>
            <path d="M1101.5 186h11.5a3 3 0 0 0 3-3 3 3 0 0 0-3-3h-.9a4.91 4.91 0 0 1-3.6-1.5 5.08 5.08 0 0 0-3.6-1.5h-3.4a4.4 4.4 0 0 0-4.4 4.4v.1a4.34 4.34 0 0 0 4.18 4.5h.22z" fill="#fff"></path>
            <path class="_style_3uGAF5" d="M1106 58h2v93h-2zm7 0h2v93h-2z"></path>
            <path d="M1297 20a32 32 0 1 0 32 32 32 32 0 0 0-32-32zm0 49a17 17 0 1 1 17-17 17 17 0 0 1-17 17z" transform="translate(0 -3)" fill="url(#c)"></path>
         </svg>
      </div>
      <div class="dotcom-footer-core _style_1PGbC7">
         <div class="dotcom-footer-core-ctas _style_nV88m">
        <a href="/" aria-label="Loader Home Page" class="_style_EWifa" data-reactid="85" style="padding-top: 5px!important;
    width: 115px;"><img src="{{asset('assets/pages/img/tod_logo.png')}}"></a>
            <div class="_style_1A4dDH">
               <!-- react-empty: 821 -->
               <div class="">
                  <a class="dotcom-cta-button _style_57W2Y _style_2d5yhM _style_2uJak7 _style_31KQtL" href="#">
                     <!-- react-text: 824 -->SIGN UP TO RIDE<!-- /react-text -->
                  </a>
               </div>
            </div>
            <div class="_style_4v9JMl">
               <div class="">
                  <a href="#" class="dotcom-cta-button _style_2uJak7 _style_1eEKuu _style_57W2Y _style_39V5zj">
                     <!-- react-text: 828 -->Become a driver<!-- /react-text -->
                  </a>
               </div>
               <!-- react-empty: 829 -->
            </div>
         </div>
         <div class="dotcom-footer-core-nav _style_34loCG">
            <div class="dotcom-footer-core-util _style_4hSx9v _style_3wm63y">
               <a class="_style_3i3PdM" href="#">
                  <svg viewBox="0 0 64 64" width="16px" height="16px" class=" _style_1BKBec">
                     <path d="M32.0000114,4c-11.0449219,0-20,8.8027344-20,20c0,6.0996094,2,11.03125,6,16.9375l13.1807709,18.6359062 C31.3796368,59.8575249,31.6893368,59.9997215,31.9991455,60c0.3104134,0.0002785,0.6209373-0.141922,0.820179-0.4265938 L46.0000114,40.9375c4-5.90625,6-10.8378906,6-16.9375C52.0000114,12.8027344,43.0449333,4,32.0000114,4z M32.0000114,36.25 c-6.7655029,0-12.25-5.484375-12.25-12.25s5.4844971-12.25,12.25-12.25s12.25,5.484375,12.25,12.25 S38.7655144,36.25,32.0000114,36.25z"></path>
                  </svg>
                  <span style="padding-left: 12px; text-transform: capitalize;">Enter your location</span>
               </a>
               <a class="_style_3i3PdM _style_3jILTx" href="#">
                  <svg viewBox="0 0 64 64" width="16px" height="16px" class=" _style_1BKBec">
                     <path d="M33.7021484,4.5913086c-0.3100586-0.0356445-0.6191406-0.0869141-0.934082-0.0869141 c-0.0917969,0-0.1821289,0.0224609-0.2734375,0.0258789c-0.1640625-0.003418-0.3251953-0.0258789-0.4907227-0.0258789 c-15.4384766,0-28,12.5615234-28,28c0,15.4404297,12.5615234,28,28,28c0.1655273,0,0.3266602-0.0224609,0.4907227-0.0258789 c0.0913086,0.003418,0.1816406,0.0258789,0.2734375,0.0258789c0.3149414,0,0.6240234-0.0512695,0.934082-0.0869141 c14.6474609-0.8842773,26.3017578-13.046875,26.3017578-27.9130859S48.3496094,5.4755859,33.7021484,4.5913086z M23.6225586,9.4985352c-2.8369141,3.2744141-5.1401367,7.9755859-6.4057617,13.527832 c-3.934082-0.4277344-6.3081055-0.9248047-7.2651367-1.1547852C12.7148438,16.1616211,17.6171875,11.6938477,23.6225586,9.4985352z M7.4907227,32.5043945c0-2.5683594,0.4008789-5.0439453,1.1357422-7.3710938 c0.6674805,0.1816406,3.2211914,0.8120117,7.9482422,1.3364258c-0.2714844,1.9399414-0.4262695,3.9550781-0.4262695,6.034668 c0,2.6425781,0.2529297,5.1772461,0.6821289,7.5864258c-4.0546875,0.4394531-6.5522461,0.9589844-7.6733398,1.2304688 C8.0957031,38.5810547,7.4907227,35.6152344,7.4907227,32.5043945z M10.6801758,44.5527344 c1.269043-0.2705078,3.5415039-0.6787109,6.8984375-1.0292969c1.3217773,4.8876953,3.4707031,9.0180664,6.0439453,11.9868164 C18.1206055,53.5,13.5366211,49.5869141,10.6801758,44.5527344z M30,56.6972656 c-3-1.2558594-7.4179688-6.4589844-9.2285156-13.4731445C23.375,43.03125,27,42.8925781,30,42.8574219V56.6972656z M30,39.3706055 c-3,0.0385742-7.0830078,0.1928711-9.8671875,0.4086914C19.75,37.4692383,19.640625,35.0249023,19.640625,32.5043945 c0-1.9604492,0.0385742-3.8681641,0.2749023-5.7114258C22.7431641,27.0166016,27,27.1777344,30,27.2177734V39.3706055z M30,23.730957c-3-0.0371094-6.9223633-0.1884766-9.6035156-0.394043C22.0708008,15.5424805,27,9.6606445,30,8.3115234V23.730957z M53.9663086,21.6801758c-0.546875,0.1689453-2.7631836,0.7866211-7.1459961,1.3061523 c-0.9892578-5.2597656-2.7744141-9.7421875-5.1020508-12.9780273 C47.0551758,12.3212891,51.3945312,16.4853516,53.9663086,21.6801758z M34,8.2597656 c3,1.2451172,7.9438477,7.1796875,9.5874023,15.0722656C40.9482422,23.5424805,37,23.6953125,34,23.7324219V8.2597656z M34,27.2177734c3-0.0385742,7.2045898-0.2041016,9.996582-0.4360352c0.2299805,1.8466797,0.2885742,3.7573242,0.2885742,5.7226562 c0,2.5234375-0.0893555,4.9711914-0.4624023,7.2841797C41.0742188,39.5668945,37,39.4072266,34,39.3706055V27.2177734z M34,56.7490234V42.8574219c3,0.0351562,6.6523438,0.1767578,9.222168,0.375C41.4418945,50.3393555,37,55.5878906,34,56.7490234z M41.7182617,55.0019531c2.0976562-2.9155273,3.7685547-6.8291016,4.7993164-11.4272461 c3.4487305,0.3974609,5.6196289,0.8588867,6.715332,1.1357422C50.6064453,49.2602539,46.5742188,52.8969727,41.7182617,55.0019531z M47.1386719,40.1313477c0.3535156-2.4189453,0.5566406-4.9682617,0.5566406-7.6269531 c0-2.0942383-0.1254883-4.1206055-0.3476562-6.0732422c5.3911133-0.6464844,7.7763672-1.4570312,7.934082-1.5131836 L54.71875,23.3271484c1.1518555,2.8388672,1.7998047,5.9316406,1.7998047,9.1772461 c0,3.1831055-0.6289062,6.2177734-1.7387695,9.0102539C53.8164062,41.2329102,51.3945312,40.6313477,47.1386719,40.1313477z"></path>
                  </svg>
                  <span style="padding-left: 12px; text-transform: uppercase;">English</span>
               </a>
               <a class="_style_3i3PdM" href="#">
                  <span class="_style_2vszYG">Help</span>
                  <svg viewBox="0 0 64 64" width="16px" height="16px" class=" _style_1BKBec">
                     <path d="M47.0932617,55.578125c9.9132729-6.3570824,14.8717461-18.5635834,12.1553192-30.0354328 C56.5887451,14.3098202,46.9536171,5.641645,35.4912834,4.2155991C23.8733921,2.7701998,12.3084412,8.9110298,7.0133862,19.3519459 C1.74448,29.7413044,3.6268132,42.6627846,11.5766268,51.1558914c8.0701036,8.6216164,21.1002998,11.2331543,31.8757286,6.395668 C44.713295,56.9854774,45.9299431,56.3239632,47.0932617,55.578125z M39.6118164,41.2753906 c-4.0401917,3.3219337-9.9890003,3.6132507-14.3237305,0.6733398c-4.0170879-2.7244797-6.0503502-7.7514725-5.0132713-12.5050163 c1.0312157-4.7266808,4.9380665-8.4576778,9.7054729-9.2716293c4.9097271-0.8382511,9.9618359,1.5428219,12.4273663,5.8673439 C45.2672005,31.0550518,44.1023178,37.6143417,39.6118164,41.2753906 C39.4360352,41.4199219,39.6884766,41.2128906,39.6118164,41.2753906z M8,32c0-3.1442432,0.6224785-6.2795258,1.8295898-9.1831055 c1.2624741,0.5228825,2.5249491,1.0457668,3.7874231,1.5686493c0.828126,0.342989,1.656251,0.685976,2.484376,1.0289631 c0.3992462,0.1653576,1.0261478,0.2732658,1.0574913,0.622488c0.0245056,0.2730446-0.3147736,0.8365955-0.4069748,1.1256828 c-0.1397781,0.4382725-0.2607269,0.8829498-0.3620167,1.3316765c-0.2065983,0.9152508-0.331707,1.8495617-0.3737926,2.7868748 c-0.0817184,1.8199825,0.1505184,3.6503544,0.6853428,5.3917789c0.0930996,0.3031387,0.5946331,1.2562408,0.4693317,1.469574 c-0.0581551,0.0990143-0.482151,0.1996956-0.5826359,0.2413139c-0.7499542,0.3106117-1.4999084,0.6212234-2.2498627,0.9318314 c-1.5028944,0.6224594-3.0057878,1.2449188-4.5086823,1.8673782C8.6224785,38.2795258,8,35.144249,8,32z M56,32 c0,3.1442451-0.6224785,6.2795258-1.8295898,9.1831055c-1.2623901-0.5228844-2.5247841-1.0457649-3.7871742-1.5686493 c-0.8280678-0.3429871-1.6561394-0.6859741-2.484211-1.028965c-0.3992386-0.1653633-1.0260696-0.2732697-1.0574188-0.6224861 c-0.0245094-0.2730446,0.3147354-0.836586,0.4069138-1.1256828c0.1397438-0.4382706,0.2606392-0.8829498,0.3618774-1.3316765 c0.2064934-0.9152603,0.3314781-1.849575,0.3735237-2.7868729c0.0816383-1.8199425-0.15028-3.6503353-0.684948-5.3917809 c-0.0930748-0.3031387-0.5945549-1.2562542-0.4692574-1.4695759c0.0581551-0.0990105,0.4821129-0.1996918,0.5825958-0.241312 c0.7499046-0.3106117,1.4998093-0.6212215,2.2497139-0.9318333c1.5027962-0.6224594,3.0055885-1.2449169,4.5083847-1.8673763 C55.3775215,25.7204742,56,28.8557529,56,32z M41.1831055,9.8295898c-0.5228844,1.2623911-1.0457649,2.5247822-1.5686493,3.7871733 c-0.3429871,0.8280706-0.6859741,1.6561413-1.028965,2.4842119c-0.1653633,0.3992405-0.2732697,1.0260696-0.6224861,1.0574188 c-0.2730446,0.0245113-0.836586-0.3147354-1.1256828-0.4069138c-0.4382706-0.1397419-0.8829498-0.2606392-1.3316765-0.3618774 c-0.9152603-0.2064934-1.849575-0.3314781-2.7868729-0.3735237c-1.8199425-0.0816374-3.6503353,0.1502781-5.3917809,0.6849499 c-0.3031387,0.093071-1.2562542,0.5945511-1.4695759,0.4692574c-0.0990105-0.0581551-0.1996918-0.4821148-0.241312-0.5825977 c-0.3106117-0.7499046-0.6212215-1.4998093-0.9318333-2.2497139c-0.6224594-1.5027952-1.2449169-3.0055895-1.8673763-4.5083847 C28.6499081,7.4046178,35.3500824,7.4046144,41.1831055,9.8295898z M22.8168945,54.1699219 c0.5228825-1.2623901,1.0457668-2.5247841,1.5686493-3.7871742c0.342989-0.8280678,0.685976-1.6561394,1.0289631-2.484211 c0.1653652-0.3992386,0.2732697-1.0260735,0.622488-1.057415c0.2730446-0.0245094,0.8365974,0.3147736,1.1256828,0.406971 c0.4382725,0.139782,0.8829498,0.2607269,1.3316765,0.3620186c0.9152508,0.2066002,1.8495617,0.331707,2.7868748,0.3737907 c1.8199825,0.0817184,3.6503544-0.1505165,5.3917789-0.6853409c0.3031387-0.0931015,1.2562408-0.594635,1.469574-0.4693298 c0.0990143,0.0581551,0.1996956,0.4821472,0.2413139,0.582634c0.3106117,0.7499542,0.6212234,1.4999084,0.9318314,2.2498627 c0.6224594,1.5028954,1.2449188,3.0057869,1.8673782,4.5086823C35.3502312,56.5953255,28.6495628,56.5957565,22.8168945,54.1699219z "></path>
                  </svg>
                  <span style="padding-left: 12px;">Help</span>
               </a>
            </div>
            <div class="dotcom-footer-core-social _style_3x6Z4b">
               <span class="_style_414tT0">
                  <a class="_style_2fN6V8" href="#">
                     <span class="_style_2vszYG">Facebook</span>
                     <span style="display: inline-block;">
                        <svg viewBox="0 0 64 64" width="16px" height="16px" class=" _style_4tFZcc">
                           <path d="M36.4625053,60H25.9625034V32h-7v-9.625h7v-5.6000004C25.9625053,8.7250004,28.0625057,4,37.3375053,4h7.6999969v9.625 h-4.8999977c-3.6749992,0-3.8500023,1.4000006-3.8500023,3.8500004V22.375h8.75l-0.875,9.625h-7.6999969V60z"></path>
                        </svg>
                     </span>
                  </a>
               </span>
               <span class="_style_414tT0">
                  <a class="_style_2fN6V8" href="#">
                     <span class="_style_2vszYG">Twitter</span>
                     <span style="display: inline-block;">
                        <svg viewBox="0 0 64 64" width="16px" height="16px" class=" _style_4tFZcc">
                           <path d="M58,15.8769531c-1.939846,0.8125-3.889843,1.4624996-6.1546898,1.625 c2.264843-1.3000011,3.889843-3.4023438,4.702343-5.8296881c-2.1023445,1.3000011-4.3773422,2.1023445-6.8046875,2.5898438 c-1.7773438-2.1023436-4.7023392-3.4023438-7.6171875-3.4023438c-5.8296852,0-10.6945305,4.7023439-10.6945305,10.6945333 c0,0.8125,0.1625023,1.625,0.3250008,2.4273415c-8.9070301-0.4874992-16.686718-4.702343-22.028904-11.1820297 c-0.9750004,1.625-1.4625006,3.4023438-1.4625006,5.342186c0,3.7273445,1.9398441,6.9671879,4.7023439,8.907032 c-1.7773438,0-3.4023438-0.4875011-4.8648434-1.3000011v0.1625004c0,5.1796875,3.7273436,9.3945312,8.5820303,10.3695335 c-0.9749994,0.1624985-1.7773428,0.3250008-2.7523432,0.3250008c-0.6499996,0-1.3000002,0-1.9398432-0.1625023 c1.3000002,4.2148438,5.342186,7.2921867,9.8820305,7.454689c-3.5648422,2.9148445-8.2570305,4.5398445-13.2843742,4.5398445 c-0.8125,0-1.7773438,0-2.5898438-0.1625023c4.702343,3.077343,10.3695316,4.8648453,16.3617191,4.8648453 c19.6015606,0,30.2960911-16.1992188,30.2960911-30.296093c0-0.4875011,0-0.9750004,0-1.3000011 C54.7601585,19.7566395,56.547657,17.9792976,58,15.8769531z"></path>
                        </svg>
                     </span>
                  </a>
               </span>
               <span class="_style_414tT0">
                  <a class="_style_2fN6V8" href="#">
                     <span class="_style_2vszYG">LinkedIn</span>
                     <span style="display: inline-block;">
                        <svg viewBox="0 0 64 64" width="16px" height="16px" class=" _style_4tFZcc">
                           <path d="M48.1717491,7.9984102H15.9741688c-4.363224,0-7.9741678,3.6109438-7.9741678,7.9741678v31.9012089 c0,4.5136795,3.6109438,8.1246223,7.9741678,8.1246223h32.0471268c4.363224,0,7.9741707-3.6109428,7.9741707-7.9741669V15.972578 C56.145916,11.609354,52.5349731,7.9984102,48.1717491,7.9984102z M22.6329365,48.0150032h-7.1671524V26.21492h7.1671524V48.0150032 z M19.0493584,23.2286072L19.0493584,23.2286072c-2.5383644,0-4.0315218-1.6424732-4.0315218-3.7328911 s1.6424723-3.732892,4.0315218-3.732892c2.5383663,0,4.0315228,1.6424723,4.0315228,3.732892 C23.2301979,21.5861359,21.5877247,23.2286072,19.0493584,23.2286072z M48.763176,48.0150032h-7.1671524V36.3683853 c0-2.9863129-1.0452118-4.9274178-3.7328911-4.9274178c-1.9411049,0-3.1356316,1.3438416-3.7328911,2.6876831 c-0.1493187,0.4479446-0.2986336,1.0452118-0.2986336,1.79179V48.015007h-7.1671524c0,0,0.1493168-19.7096653,0-21.8000851 h7.1671524v3.1356297c0.8958931-1.4931583,2.6876793-3.5835762,6.5698891-3.5835762 c4.7780991,0,8.3616753,3.1356297,8.3616753,9.8548355L48.763176,48.0150032L48.763176,48.0150032z"></path>
                        </svg>
                     </span>
                  </a>
               </span>
               <span class="_style_414tT0">
                  <a class="_style_2fN6V8" href="#">
                     <span class="_style_2vszYG">Instagram</span>
                     <span style="display: inline-block;">
                        <svg viewBox="0 0 64 64" width="16px" height="16px" class=" _style_4tFZcc">
                           <path d="M58.02191,21.27509c-0.1781-2.67126-0.53424-4.63013-1.24658-6.23291c-0.71234-1.78082-1.60272-3.20544-3.0274-4.63013
                              c-1.42462-1.42468-2.8493-2.31506-4.63013-3.0274c-1.60272-0.71234-3.56165-1.06848-6.23285-1.24658
                              c-2.8493-0.17804-3.73975-0.17804-10.68494-0.17804s-7.83563,0-10.68494,0.17804c-2.6712,0.1781-4.63013,0.53424-6.23285,1.24658
                              c-1.78082,0.71234-3.20551,1.60272-4.63013,3.0274c-1.42468,1.42468-2.31512,2.8493-3.0274,4.63013
                              c-0.71234,1.60278-1.06854,3.56165-1.24658,6.23291c-0.1781,2.6712-0.1781,3.56165-0.1781,10.68494s0,7.83557,0.1781,10.68494
                              c0.17804,2.6712,0.53424,4.63013,1.24658,6.23285c0.71228,1.78082,1.60272,3.20544,3.02734,4.63013
                              c1.42468,1.42468,2.84937,2.31506,4.63019,3.0274c1.60272,0.71234,3.56165,1.06848,6.23285,1.24658
                              c2.6712,0.17804,3.56165,0.17804,10.68494,0.17804s7.83563,0,10.68494-0.17804c2.6712-0.1781,4.63013-0.53424,6.23285-1.24658
                              c1.78082-0.71234,3.20551-1.60272,4.63013-3.0274c1.42468-1.42468,2.31506-2.8493,3.0274-4.63013
                              c0.71234-1.60272,1.06848-3.56165,1.24658-6.23285c0.1781-2.67126,0.1781-3.56165,0.1781-10.68494
                              S58.20001,24.12439,58.02191,21.27509z M53.39178,42.46686c-0.1781,2.49316-0.53424,3.91779-0.89044,4.80823
                              c-0.53424,1.24658-1.06848,2.13696-1.95886,3.0274c-0.89044,0.89038-1.78082,1.42462-3.0274,1.95886
                              c-0.89044,0.3562-2.31506,0.71234-4.80823,0.89044c-2.67126,0.17804-3.56165,0.17804-10.50684,0.17804s-7.65753,0-10.50684-0.1781
                              c-2.49316-0.17804-3.91785-0.53424-4.80823-0.89038c-1.24658-0.53424-2.13696-1.06848-3.0274-1.95886
                              c-0.89038-0.89044-1.42468-1.78082-1.95892-3.0274c-0.35614-0.89044-0.71228-2.31506-0.89038-4.80823
                              c-0.1781-2.67126-0.1781-3.56165-0.1781-10.50684c0-6.94525,0-7.65759,0.1781-10.5069
                              c0.1781-2.4931,0.53424-3.91779,0.89038-4.80823c0.53424-1.24652,1.06854-2.13696,1.95892-3.02734
                              c0.89044-0.89044,1.78082-1.42468,3.0274-1.95892c0.89038-0.3562,2.31506-0.71234,4.80823-0.89044
                              c2.8493-0.17804,3.56165-0.17804,10.50684-0.17804s7.65753,0,10.50684,0.1781c2.49316,0.17804,3.91779,0.53424,4.80823,0.89038
                              c1.24658,0.53424,2.13696,1.06848,3.0274,1.95892c0.89038,0.89038,1.42462,1.78082,1.95892,3.0274
                              c0.35614,0.89038,0.71228,2.31506,0.89038,4.80817c0.1781,2.67126,0.1781,3.56165,0.1781,10.5069
                              C53.56989,38.90521,53.56989,39.61755,53.39178,42.46686z"></path>
                           <path d="M32.20001,18.60382c-7.30139,0-13.1781,5.87671-13.1781,13.1781s5.87671,13.1781,13.1781,13.1781
                              c7.30133,0,13.1781-5.87671,13.1781-13.1781S39.50134,18.60382,32.20001,18.60382z M32.20001,40.32983
                              c-4.80823,0-8.54791-3.91779-8.54791-8.54791s3.91779-8.54797,8.54791-8.54797s8.54791,3.91785,8.54791,8.54797
                              S37.00824,40.32983,32.20001,40.32983z"></path>
                           <path d="M46.09039,15.04218c-1.60272,0-3.0274,1.42468-3.0274,3.0274s1.24658,3.0274,3.0274,3.0274
                              c1.60278,0,3.0274-1.24658,3.0274-3.0274S47.69318,14.86414,46.09039,15.04218z"></path>
                        </svg>
                     </span>
                  </a>
               </span>
            </div>
            <ul class="_style_15JKd3 _style_13n97z">
               <li class="_style_18VCY6">
                  <a class="_style_OgcgY" href="#">
                     <!-- react-text: 875 -->Ride<!-- /react-text -->
                  </a>
               </li>
               <li class="_style_18VCY6">
                  <a class="_style_OgcgY" href="#">
                     <!-- react-text: 878 -->Drive<!-- /react-text -->
                  </a>
               </li>
               <li class="_style_18VCY6">
                  <a class="_style_OgcgY" href="#">
                     <!-- react-text: 881 -->Cities<!-- /react-text -->
                  </a>
               </li>
            </ul>
            <ul class="_style_15JKd3 _style_1koTOV">
               <li class="_style_18VCY6">
                  <a class="_style_OgcgY" href="#">
                     <!-- react-text: 906 -->Safety<!-- /react-text -->
                  </a>
               </li>
               <li class="_style_18VCY6">
                  <a class="_style_OgcgY" href="#">
                     <!-- react-text: 909 -->Careers<!-- /react-text -->
                  </a>
               </li>
               <li class="_style_18VCY6">
                  <a class="_style_OgcgY" href="#">
                     <!-- react-text: 915 -->About Us<!-- /react-text -->
                  </a>
               </li>
            </ul>
         </div>
         <div class="dotcom-footer-core-legal _style_4e9tLk">
            <span class="_style_3t1hiE">© 2018 Loader Technologies Inc.</span>
            <div class="_style_9m4aL">
               <a href="#" class="_style_1gunlB">
                  <!-- react-text: 975 -->Privacy<!-- /react-text -->
               </a>
               <a href="#" class="_style_1gunlB">
                  <!-- react-text: 977 -->Accessibility<!-- /react-text -->
               </a>
               <a class="_style_1gunlB" href="#">
                  <!-- react-text: 979 -->Terms<!-- /react-text -->
               </a>
            </div>
         </div>
      </div>
   </div>
</footer>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script>
   $(document).ready(function () {
  var trigger = $('.hamburger'),
      overlay = $('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    function hamburger_cross() {

      if (isClosed == true) {          
        overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
      } else {   
        overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
      }
  }
  
  $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
  });  
});
</script>
<script>
     $(document).ready(function() {
    $('.carousel').carousel({
      interval: 6000
    })
  });
</script>
<script type="text/javascript">
  
  $(".cap").first().addClass("active");

  console.log($(".cap").first());
</script>