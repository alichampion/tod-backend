<?php
/**
* Project: hajjtrack.
* User: naveed
* Date: 12/07/2018
* Time: 10:53 AM
*/
?>
<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />

    <title>
        @yield('title','Dashboard') - TOD
    </title>
    <base href="{{ url('/')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @section('metas')
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    @show
    <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},            active: function() {                sessionStorage.fonts = true;            }          });
    </script>
    <!--end::Web font -->

    @stack('pre-styles')

    @section('styles')

    <!--begin::Base Styles -->                           <!--begin::Page Vendors -->
    <link href="{{asset('new/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors -->
    <link href="{{asset('new/assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('new/assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="{{asset('new/assets/js/sweetalert2/sweetalert2.css')}}"/>
    <!--end::Base Styles -->
    <link rel="stylesheet" type="text/css" href="{{asset('new/assets/css/custom.css')}}">


    @show

    @stack('post-styles')

    <link rel="shortcut icon" href="{{asset('new/assets/demo/default/media/img/logo/favicon.ico')}}" />

    <script type="text/javascript">
        var BASE_URL = "{{ url('/')}}";
    </script>

</head>
<!-- end::Head -->        <!-- end::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light  m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

    <!--[html-partial:include:{"file":"_layout.html"}]/-->
    @section('layout')
    @include('_layouts.admin._layout')
    @show

    @stack('pre-scripts')

    @section('scripts')
    <!--begin::Base Scripts -->
    <script src="{{asset('new/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('new/assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
    <!--end::Base Scripts -->                    <!--begin::Page Vendors -->
    <script src="{{asset('new/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
    <!--end::Page Vendors -->                                                        <!--begin::Page Snippets -->
    <script src="{{asset('new/assets/app/js/dashboard.js')}}" type="text/javascript"></script>
    <!--end::Page Snippets -->

    <script type="text/javascript" src="{{asset('new/assets/js/validate/formValidation.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('new/assets/js/validate/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('new/assets/js/validate/validation.js')}}"></script>
    <!-- <script src="{{asset('new/assets/js/sweetalert2/sweetalert2.min.js')}}"></script> -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>

    <script src="{{asset('new/assets/js/custom.js')}}"></script>
    <script>
        $(document).ready(function(){
            setInterval(function(){
                var date = new Date();
                var hr = date.getHours(); 
                var min = date.getMinutes();
                var sec = date.getSeconds();
                
                var time = `${hr}:${min}:${sec}`;
                $("#datatime").html(time);
            },1000);
        });
    </script>
    @show



    @stack('post-scripts')

</body>
<!-- end::Body -->
</html>
