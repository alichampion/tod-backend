
<!-- BEGIN: Brand -->
<div class="m-stack__item m-brand  m-brand--skin-light ">
	<div class="m-stack m-stack--ver m-stack--general">
		<div class="m-stack__item m-stack__item--middle m-brand__logo">
			<a href="{{route('admin.dashboard')}}" class="m-brand__logo-wrapper">
				<img alt="" style="width: 60px; height: 60px; margin-top: 15px;" src="{{ asset('assets/pages/img/tod_logo.png')}}"/>
			</a>
		</div>
		<div class="m-stack__item m-stack__item--middle m-brand__tools">
			<!-- BEGIN: Left Aside Minimize Toggle -->
			<a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block 					 ">
				<span></span>
			</a>
			<!-- END -->										<!-- BEGIN: Responsive Aside Left Menu Toggler -->
			<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
				<span></span>
			</a>
			<!-- END -->																		<!-- BEGIN: Topbar Toggler -->
			<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
				<i class="flaticon-more"></i>
			</a>
			<!-- BEGIN: Topbar Toggler -->
		</div>
	</div>
</div>
<!-- END: Brand -->
