
<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')


 <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
     
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                              <i class="fa fa-key" style="color: #fff;"></i>
                                <span class="caption-subject  sbold uppercase" style="color: #fff; font-size: 18px!important;"> Available Permissions</span>

                            </div>
                            <a href="{{ route('roles.index') }}" class="btn btn-default pull-right">Roles</a>
                        </div>

                 
          
                  <div class="row">
          <div class="col-md-11" style="margin: 0 auto;">
    <div class="table-responsive">
        <table class="table table-bordered table-striped">

            <thead>
                <tr>
                    <th>Permissions</th>
                    <th>Operation</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($permissions as $permission)
                <tr>
                    <td>{{ $permission->name }}</td> 
                    <td>
                    <a href="{{ URL::to('admin/permissions/'.$permission->id.'/edit') }}" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>

                    {!! Form::open(['method' => 'DELETE', 'route' => ['permissions.destroy', $permission->id] ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}

                    </td>
                </tr>
                @endforeach
            </tbody>
              <a href="{{ URL::to('admin/permissions/create') }}" class="btn btn-success" style="float: right; margin-right: 4%; margin-bottom: 10px;">Add Permission</a>
              <br>
        </table>
    </div>
</div>
</div>               
@endsection
@stack('post-styles')
<link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />

@push('post-scripts')



                    

 @endpush
