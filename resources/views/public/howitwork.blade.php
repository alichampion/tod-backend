@extends('layout.public.publicheader')
@section('content')
<div class="_style_3jZond" data-reactid="260"><div class="gutter" data-reactid="261"><div class="dotcom-body app-body" data-reactid="262"><div><div class="position--relative pull-app-gutter--sides clearfix"><div><div class="_style_34KYWo wcb-billboard-text"><div class="_style_49F8gy"><div class="_style_2AtI1L" style="width: 75%;"><div class="_style_1mG8d3" style="margin-bottom: 24px;"><h1 id="wcb-headline" class="_style_12KMB8">How Loader works</h1><p class="_style_3hjxI4"> A guide for new riders</p></div><p class="_style_1mG8d3 _style_TM58B" style="margin-bottom: 24px;">The Loader app matches you with a nearby driver to take you wherever you want to go.</p><a class="wcb-button _style_1m6gck" href="#"><!-- react-text: 8027 -->Sign up to ride<!-- /react-text --><span class="_style_28zWyA"><svg viewBox="0 0 64 64" width="20px" height="20px" class=" _style_4wJp4e"><path fill-rule="evenodd" clip-rule="evenodd" d="M59.927 31.985l.073.076-16.233 17.072-3.247-3.593L51.324 34H4v-4h47.394L40.52 18.407l3.247-3.494L60 31.946l-.073.039z"></path></svg></span></a></div></div></div><div class="wcb-image">
   <div class="_style_1SDmu3" role="img" title="Billboard Hero Image" aria-label="Billboard Hero Image"></div>
</div><div class="_style_34KYWo wcb-steps"><div class="_style_27rK6s"><div class="_style_VxPAE"><p class="_style_bsxwn">Overview</p><h2 class="_style_3zaJwR">Requesting a ride</h2></div><div class="_style_3IMwtk"><div class="layout layout--flush"><div class="_style_3i9euD _style_3nW6zn _style_sVJx"><div class="_style_2S02WF"><div class="layout"><div class="layout__item _style_4jP3Mu"><div class="_style_1Vr2Vl"><div class="_style_46VFZ1">1</div></div></div><div class="layout__item _style_1bTCU7"><h4 class="_style_41IcSp">Download</h4><div class="cmln__markdown"><p class="cmln__paragraph">Get the free Loader app from the <a class="cmln__link" href="#">App Store</a> or <a class="cmln__link" href="#">Google Play</a> on your smartphone. Open the app to create your account.  </p>
</div></div></div></div></div><div class="_style_3i9euD _style_3nW6zn _style_sVJx"><div class="_style_2S02WF"><div class="layout"><div class="layout__item _style_4jP3Mu"><div class="_style_1Vr2Vl"><div class="_style_46VFZ1">2</div></div></div><div class="layout__item _style_1bTCU7"><h4 class="_style_41IcSp">Request</h4><div class="cmln__markdown"><p class="cmln__paragraph">Enter your destination and choose a ride option. You’ll always see the price up front.</p>
</div></div></div></div></div><div class="_style_3i9euD"><div class="_style_2S02WF"><div class="layout"><div class="layout__item _style_4jP3Mu"><div class="_style_1Vr2Vl"><div class="_style_46VFZ1">3</div></div></div><div class="layout__item _style_1bTCU7"><h4 class="_style_41IcSp">Ride</h4><div class="cmln__markdown"><p class="cmln__paragraph">You'll see your driver's picture and vehicle details, and can track their arrival on the map.</p>
</div></div></div></div></div></div></div></div></div><div class="_style_1OT7Uy wcb-gallery"><div class="_style_4jsgrJ"><div class="_style_VRmHw"><div class="_style_Ma5L" style="padding-left:40px!important;"><div class="_style_VxPAE"><p class="_style_bsxwn">App features</p><h2 class="_style_3zaJwR">Safe, easy rides</h2></div><div class="_style_4eMc1T"><div class="_style_3XjDEC"><div class="cmln__markdown"><p class="cmln__paragraph">We built these features to make it easy to get wherever you’re going. </p>
</div></div></div></div></div></div><div class="_style_VRmHw"><div><div>
<div>
    <div class="carousel slide" id="myCarousel">
        <div class="carousel-inner">
            <div class="item active">
                    <ul class="thumbnails">
                        <li class="col-sm-3">
                     <div class="fff">
                        <div class="thumbnail">
                           <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                        </div>
                        <div class="caption">
                           <h4>Praesent commodo</h4>
                           <p>Nullam Condimentum Nibh Etiam Sem</p>
                           <a class="btn btn-mini" href="#">» Read More</a>
                        </div>
                            </div>
                        </li>
                        <li class="col-sm-3">
                     <div class="fff">
                        <div class="thumbnail">
                           <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                        </div>
                        <div class="caption">
                           <h4>Praesent commodo</h4>
                           <p>Nullam Condimentum Nibh Etiam Sem</p>
                           <a class="btn btn-mini" href="#">» Read More</a>
                        </div>
                            </div>
                        </li>
                        <li class="col-sm-3">
                     <div class="fff">
                        <div class="thumbnail">
                           <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                        </div>
                        <div class="caption">
                           <h4>Praesent commodo</h4>
                           <p>Nullam Condimentum Nibh Etiam Sem</p>
                           <a class="btn btn-mini" href="#">» Read More</a>
                        </div>
                            </div>
                        </li>
                        <li class="col-sm-3">
                     <div class="fff">
                        <div class="thumbnail">
                           <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                        </div>
                        <div class="caption">
                           <h4>Praesent commodo</h4>
                           <p>Nullam Condimentum Nibh Etiam Sem</p>
                           <a class="btn btn-mini" href="#">» Read More</a>
                        </div>
                            </div>
                        </li>
                    </ul>
              </div><!-- /Slide1 --> 
            <div class="item">
                    <ul class="thumbnails">
                        <li class="col-sm-3">
                     <div class="fff">
                        <div class="thumbnail">
                           <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                        </div>
                        <div class="caption">
                           <h4>Praesent commodo</h4>
                           <p>Nullam Condimentum Nibh Etiam Sem</p>
                           <a class="btn btn-mini" href="#">» Read More</a>
                        </div>
                            </div>
                        </li>
                        <li class="col-sm-3">
                     <div class="fff">
                        <div class="thumbnail">
                           <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                        </div>
                        <div class="caption">
                           <h4>Praesent commodo</h4>
                           <p>Nullam Condimentum Nibh Etiam Sem</p>
                           <a class="btn btn-mini" href="#">» Read More</a>
                        </div>
                            </div>
                        </li>
                        <li class="col-sm-3">
                     <div class="fff">
                        <div class="thumbnail">
                           <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                        </div>
                        <div class="caption">
                           <h4>Praesent commodo</h4>
                           <p>Nullam Condimentum Nibh Etiam Sem</p>
                           <a class="btn btn-mini" href="#">» Read More</a>
                        </div>
                            </div>
                        </li>
                        <li class="col-sm-3">
                     <div class="fff">
                        <div class="thumbnail">
                           <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                        </div>
                        <div class="caption">
                           <h4>Praesent commodo</h4>
                           <p>Nullam Condimentum Nibh Etiam Sem</p>
                           <a class="btn btn-mini" href="#">» Read More</a>
                        </div>
                            </div>
                        </li>
                    </ul>
              </div><!-- /Slide2 --> 
            <div class="item">
                    <ul class="thumbnails">
                        <li class="col-sm-3">   
                     <div class="fff">
                        <div class="thumbnail">
                           <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                        </div>
                        <div class="caption">
                           <h4>Praesent commodo</h4>
                           <p>Nullam Condimentum Nibh Etiam Sem</p>
                           <a class="btn btn-mini" href="#">» Read More</a>
                        </div>
                            </div>
                        </li>
                        <li class="col-sm-3">
                     <div class="fff">
                        <div class="thumbnail">
                           <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                        </div>
                        <div class="caption">
                           <h4>Praesent commodo</h4>
                           <p>Nullam Condimentum Nibh Etiam Sem</p>
                           <a class="btn btn-mini" href="#">» Read More</a>
                        </div>
                            </div>
                        </li>
                        <li class="col-sm-3">
                     <div class="fff">
                        <div class="thumbnail">
                           <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                        </div>
                        <div class="caption">
                           <h4>Praesent commodo</h4>
                           <p>Nullam Condimentum Nibh Etiam Sem</p>
                           <a class="btn btn-mini" href="#">» Read More</a>
                        </div>
                            </div>
                        </li>
                        <li class="col-sm-3">
                     <div class="fff">
                        <div class="thumbnail">
                           <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                        </div>
                        <div class="caption">
                           <h4>Praesent commodo</h4>
                           <p>Nullam Condimentum Nibh Etiam Sem</p>
                           <a class="btn btn-mini" href="#">» Read More</a>
                        </div>
                            </div>
                        </li>
                    </ul>
              </div><!-- /Slide3 --> 
        </div>
        
       
      <nav>
         <ul class="control-box pager">
            <li><a data-slide="prev" href="#myCarousel" class=""><i class="glyphicon glyphicon-chevron-left"></i></a></li>
            <li><a data-slide="next" href="#myCarousel" class=""><i class="glyphicon glyphicon-chevron-right"></i></li>
         </ul>
      </nav>
      <!-- /.control-box -->   
                              
    </div><!-- /#myCarousel -->
        
</div><!-- /.col-xs-12 -->          

</div></div></div></div><div class="_style_3ye16P wcb-vp-illustrated"><div class="_style_eeI8o"><div class="_style_4jfdOQ"><p class="_style_bsxwn">More features</p><h2 class="_style_3zaJwR">The ride you want</h2></div><div class="layout _style_4ykyzI"><div class="layout__item _style_1YSqxy"><div class="_style_VxPAE"><div class="_style_2ehyHb"><div class="_style_2XpHEy"><div class="_style_1iLMt8"><div aria-label="3c_message_2" class="_style_4EfqLw"><div class="isvg loaded" style="height: 100%;"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88 88" height="88" width="88" style="display: block; height: 100%; width: 100%;"><title>Decorative Illustration</title><desc>Decorative Illustration</desc>
    <defs>
        <clipPath id="a___1569785765">
            <circle cx="44.04" cy="44" r="43.65" fill="none"></circle>
        </clipPath>
        <pattern id="b___1569785765" data-name="2x2 - Black" width="4" height="4" patternTransform="matrix(.8 0 0 .8 5.7 4.5)" patternUnits="userSpaceOnUse" viewBox="0 0 4 4">
            <path fill="none" d="M0 0h4v4H0z"></path>
            <path d="M0 0h2v2H0z"></path>
        </pattern>
    </defs>
    <path d="M17.07 74.68h10.25v8.95H17.07z" style="fill:#fcb666;"></path>
    <g clip-path="url(#a___1569785765)">
        <path fill="#d6d6d5" d="M90.75 90.31H-1.6v-93.5h92.35z"></path>
        <path d="M71.01 87.65H17.08V61.3a14.8 14.8 0 0 1 14.8-14.8h24.33a14.8 14.8 0 0 1 14.8 14.8v26.35z" style="fill:rgb(241, 102, 34)"></path>
        <circle cx="40.02" cy="22.61" r="11.58" fill="#f8f8f9"></circle>
        <path d="M35.06 46.5h8.32v-4.3a12.59 12.59 0 0 0-3.69-8.9l-1-1A12.59 12.59 0 0 1 35 23.42h-2.29V46.5h2.35z"></path>
        <path fill="#f8f8f9" d="M51.6 46.5H36.26V28.61H51.6z"></path>
        <path fill="#f16622" d="M17.07 63.76h53.94v23.89H17.07z"></path>
        <path d="M40 38.83h-9.41a2.16 2.16 0 0 1-2.16-2.16V22.61H40v16.22z" fill="#f8f8f9"></path>
        <path d="M28.44 25v4.61h-1.16a.54.54 0 0 1-.38-.92A5.23 5.23 0 0 0 28.44 25z" fill="#f8f8f9"></path>
        <path d="M40 11a11.24 11.24 0 0 1 11.6 11.29v17.06l-4-4a5.07 5.07 0 0 1-1.48-3.58v-5.93a1.79 1.79 0 0 0-1.8-1.84 1.79 1.79 0 0 0-1.79 1.79v.27h-1A3.14 3.14 0 0 1 38.36 23v-1.5a2.28 2.28 0 0 0-2.28-2.28h-7.14A11.59 11.59 0 0 1 40 11z"></path>
        <path d="M39.77 21.97H25.18v-.48a6.57 6.57 0 0 1 6.56-6.6h8v7.08z"></path>
        <path d="M36.27 46.5h15.34v41.03H36.27z" style="fill: rgb(137, 218, 193);"></path>
        <path d="M43.97 54.13h-.1a7.63 7.63 0 0 1-7.63-7.63H51.6a7.63 7.63 0 0 1-7.63 7.63z" fill="#f8f8f9"></path>
        <path d="M51.6 46.5h8.32v-4.3a12.59 12.59 0 0 0-3.69-8.9l-1-1a12.59 12.59 0 0 1-3.69-8.9h-5.42V41a5.49 5.49 0 0 0 5.48 5.5z"></path>
    </g>
    <ellipse cx="67.07" cy="10.13" rx="10.32" ry="9.78" fill="#fff"></ellipse>
    <path d="M57 12.35l7.84 23.31a2.32 2.32 0 0 0 4.41 0l7.84-23.31H57z" fill="#fff"></path>
    <ellipse cx="67.07" cy="10.13" rx="4.75" ry="4.5" style="fill: rgb(137, 218, 193);"></ellipse>
    <path fill="#f8f8f9" d="M7.16 54.98h6.76v3.27H7.16z"></path>
    <path d="M5.77 58.25h11.3v25.38h-3.3a8 8 0 0 1-8-8V58.25z" style="fill: rgb(241, 102, 34);"></path>
    <rect x="8.84" y="38.38" width="8.23" height="17.2" rx="1.18" ry="1.18" style="fill: rgb(137, 218, 193);"></rect>
    <rect x="7.66" y="38.38" width="8.23" height="17.2" rx="1.18" ry="1.18"></rect>
    <path d="M13.92 52.59v-4.25H12a1.93 1.93 0 0 0 1.93-1.93H6.54a2.21 2.21 0 0 0-2.21 2.21v2.47a3.09 3.09 0 0 0 .9 2.18l1.87 1.88a4 4 0 0 0 2.83 1.17h.25a3.74 3.74 0 0 0 3.74-3.73z" fill="#f8f8f9"></path>
</svg>
</div></div></div></div></div><div class="_style_3ZBSIh"><h4 class="_style_4okShP">Requests made easy</h4><div class="_style_31qDzC"><div class="cmln__markdown"><p class="cmln__paragraph">Get a fast, convenient ride wherever you want to go.</p>
<p class="cmln__paragraph"><a class="cmln__link" href="#" target="_blank">Saved places</a></p>
</div></div></div></div></div><div class="layout__item _style_1YSqxy"><div class="_style_VxPAE"><div class="_style_2ehyHb"><div class="_style_2XpHEy"><div class="_style_1iLMt8"><div aria-label="3c_money_payment_3" class="_style_4EfqLw"><div class="isvg loaded" style="height: 100%;"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88 88" height="88" width="88" style="display: block; height: 100%; width: 100%;"><title>Decorative Illustration</title><desc>Decorative Illustration</desc>
    <defs>
        <clipPath id="a___-1853328213">
            <path d="M46.26 14H19.39a22.32 22.32 0 0 0 22.32 22.35h26.86A22.32 22.32 0 0 1 46.26 14z" fill="none"></path>
        </clipPath>
        <pattern id="b___-1853328213" data-name="2x2 - Black" width="4" height="4" patternTransform="matrix(.7 0 0 .7 4 7.8)" patternUnits="userSpaceOnUse" viewBox="0 0 4 4">
            <path fill="none" d="M0 0h4v4H0z"></path>
            <path d="M0 0h2v2H0z"></path>
        </pattern>
    </defs>
    <path fill="#f16622" d="M7.14 36.35h52.83V88H7.14z"></path>
    <path fill="#d6d6d5" d="M87.98 88h-28V36.35h28zM7.14 38.12h52.83v1.41H7.14z"></path>
    <path d="M59.97 38.12h28v1.41h-28z" style="fill: rgb(241, 102, 34);"></path>
    <path fill="#d6d6d5" d="M7.14 42.03h52.83v1.41H7.14z"></path>
    <path d="M59.97 42.03h28v1.41h-28z" style="fill:rgb(241, 102, 34);"></path>
    <path fill="#d6d6d5" d="M7.14 45.95h52.83v1.41H7.14z"></path>
    <path d="M59.97 45.95h28v1.41h-28z" style="fill: rgb(241, 102, 34);"></path>
    <path fill="#d6d6d5" d="M7.14 49.86h52.83v1.41H7.14z"></path>
    <path d="M59.97 49.86h28v1.41h-28z" style="fill: rgb(241, 102, 34);"></path>
    <path fill="#d6d6d5" d="M7.14 53.78h52.83v1.41H7.14z"></path>
    <path d="M59.97 53.78h28v1.41h-28z" style="fill: rgb(241, 102, 34);"></path>
    <path fill="#d6d6d5" d="M7.14 57.69h52.83v1.41H7.14z"></path>
    <path d="M59.97 57.69h28v1.41h-28z" style="fill: rgb(241, 102, 34);"></path>
    <path fill="#d6d6d5" d="M7.14 61.6h52.83v1.41H7.14z"></path>
    <path d="M59.97 61.6h28v1.41h-28z" style="fill: rgb(241, 102, 34);"></path>
    <path fill="#d6d6d5" d="M7.14 65.52h52.83v1.41H7.14z"></path>
    <path d="M59.97 65.52h28v1.41h-28z" style="fill: rgb(241, 102, 34);"></path>
    <path fill="#d6d6d5" d="M7.14 65.52h52.83v1.41H7.14z"></path>
    <path d="M59.97 65.52h28v1.41h-28z" style="fill: rgb(241, 102, 34);"></path>
    <path fill="#d6d6d5" d="M7.14 69.43h52.83v1.41H7.14z"></path>
    <path d="M59.97 69.43h28v1.41h-28z" style="fill: rgb(241, 102, 34);"></path>
    <path fill="#d6d6d5" d="M7.14 73.35h52.83v1.41H7.14z"></path>
    <path d="M59.97 73.35h28v1.41h-28z" style="fill: rgb(241, 102, 34);"></path>
    <path fill="#d6d6d5" d="M7.14 77.26h52.83v1.41H7.14z"></path>
    <path d="M59.97 77.26h28v1.41h-28z" style="fill: rgb(241, 102, 34);"></path>
    <path fill="#d6d6d5" d="M7.14 81.18h52.83v1.41H7.14z"></path>
    <path d="M59.97 81.18h28v1.41h-28z" style="fill: rgb(241, 102, 34);"></path>
    <path fill="#d6d6d5" d="M7.14 85.09h52.83v1.41H7.14z"></path>
    <path d="M59.97 85.09h28v1.41h-28z" style="fill: rgb(241, 102, 34);"></path>
    <path d="M46.26 14H19.39a22.32 22.32 0 0 0 22.32 22.35h26.86A22.32 22.32 0 0 1 46.26 14z" style="fill: rgb(137, 218, 193);"></path>
    <g clip-path="url(#a___-1853328213)">
        <path d="M65.48 38.6h-12c-13.55 0-26-8.77-29.1-17l-.38-.78.87-.12a3 3 0 0 0 2.55-2.93v-.7h12.8v.7a3 3 0 0 0 2.92 3h.49l.16.46c3.15 9 12.66 16 21.65 16v1.37zM26 21.87c4 9.07 17.5 15.33 27.49 15.33h4.62a26.9 26.9 0 0 1-15.45-15.1 4.41 4.41 0 0 1-3.75-3.63h-10.1a4.41 4.41 0 0 1-2.81 3.4z" style="fill: rgb(241, 102, 34);"></path>
    </g>
    <path d="M0 88V61.13h26.94V88zm26.95 0V61.13h26.94V88z" style="fill: rgb(137, 218, 193);"></path>
    <path d="M45.2 65.57a5.17 5.17 0 0 0 3.54 3.55v11.54a5.17 5.17 0 0 0-3.54 3.54H7.43a5.17 5.17 0 0 0-3.54-3.55V69.11a5.17 5.17 0 0 0 3.54-3.55H45.2m1.21-1.41H6.22a3.74 3.74 0 0 1-3.74 3.75v14a3.74 3.74 0 0 1 3.74 3.74h40.2a3.74 3.74 0 0 1 3.74-3.74v-14a3.74 3.74 0 0 1-3.74-3.74z" style="fill: rgb(241, 102, 34);"></path>
    <path d="M26.94 82.84a8.27 8.27 0 1 1 8.27-8.27 8.28 8.28 0 0 1-8.27 8.27zm0-15.14a6.87 6.87 0 1 0 6.87 6.87 6.87 6.87 0 0 0-6.87-6.87zM8.47 78.89h6.54v1.41H8.47z" style="fill: rgb(241, 102, 34);"></path>
    <circle cx="11.74" cy="74.83" r="2.27" style="fill:rgb(241, 102, 34);"></circle>
    <circle cx="26.94" cy="74.57" r="5.4" fill="#fff"></circle>
    <path d="M29.83 75.27h-3.59v-4.5h1.41v3.09h2.18v1.41"></path>
    <path d="M18.07 82.14h17.75v2.71H18.07z" style="fill: rgb(137, 218, 193);"></path>
    <path d="M36.52 85.55H17.36v-4.12h19.16v4.12zm-17.75-1.41h16.34v-1.3H18.77v1.3z" style="fill: rgb(241, 102, 34);"></path>
    <path d="M51.31 28.17H24.44a22.27 22.27 0 0 0 17.27 8.18h26.86a22.27 22.27 0 0 1-17.26-8.18z" fill="url(#b___-1853328213)"></path>
</svg>
</div></div></div></div></div><div class="_style_3ZBSIh"><h4 class="_style_4okShP">Seamless payments</h4><div class="_style_31qDzC"><div class="cmln__markdown"><p class="cmln__paragraph">Know the cost before you ride and pay the way you want. </p>
<p class="cmln__paragraph"><a class="cmln__link" href="#">LoaderPOOL commuter benefits</a></p>
<p class="cmln__paragraph"><a class="cmln__link" href="#">Family profiles</a></p>
<p class="cmln__paragraph"><a class="cmln__link" href="#">Split fare</a></p>
<p class="cmln__paragraph"><a class="cmln__link" href="#">Upfront pricing</a></p>
</div></div></div></div></div><div class="layout__item _style_1YSqxy"><div class="_style_VxPAE"><div class="_style_2ehyHb"><div class="_style_2XpHEy"><div class="_style_1iLMt8"><div aria-label="3c_app_location" class="_style_4EfqLw"><div class="isvg loaded" style="height: 100%;">
   <img src="{{asset('assets/public/images/transport.png')}}">
</div></div></div></div></div><div class="_style_3ZBSIh"><h4 class="_style_4okShP">Quick pickups</h4><div class="_style_31qDzC"><div class="cmln__markdown"><p class="cmln__paragraph">Find your driver faster, so you can get where you need to go.</p>
<p class="cmln__paragraph"><a class="cmln__link" href="#">Adjust your location</a></p>
<p class="cmln__paragraph"><a class="cmln__link" href="#">Driver Profiles</a></p>
</div></div></div></div></div><div class="layout__item _style_1YSqxy"><div class="_style_VxPAE"><div class="_style_2ehyHb"><div class="_style_2XpHEy"><div class="_style_1iLMt8"><div aria-label="3c_point_A_to_B" class="_style_4EfqLw"><div class="isvg loaded" style="height: 100%;"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 119 119" height="119" width="119" style="display: block; height: 100%; width: 100%;"><title>3c_point_A_to_B</title><desc>3c_point_A_to_B</desc>
    <defs>
        <clipPath id="a___-375265330">
            <path fill="none" d="M24.03 34.88h70.94V95H24.03z"></path>
        </clipPath>
    </defs>
    <path d="M.5 59.5a59 59 0 1 0 59-59 59 59 0 0 0-59 59" fill="#fff" stroke="#d6d6d5" stroke-miterlimit="10"></path>
    <path d="M24.03 34.88h70.94V95H24.03z" style="fill: rgb(242, 102, 29);"></path>
    <path d="M35.85 24.06a7.5 7.5 0 0 0-7.32 9.22 7.48 7.48 0 0 0 .37 1.16l5.1 16a1.89 1.89 0 0 0 1.84 1.35 1.89 1.89 0 0 0 1.84-1.35l5.08-16A7.52 7.52 0 0 0 35.83 24z" style="fill: rgb(0, 0, 0)"></path>
    <circle cx="35.85" cy="31.5" r="2.89" fill="#fff"></circle>
    <path d="M83.15 50a7.5 7.5 0 0 0-7.31 9.22 7.48 7.48 0 0 0 .37 1.16l5.1 16a1.89 1.89 0 0 0 1.84 1.35A1.89 1.89 0 0 0 85 76.38l5.08-16a7.52 7.52 0 0 0-6.93-10.48z" style="fill: rgb(0, 0, 0)"></path>
    <circle cx="83.15" cy="57.44" r="2.89" fill="#fff"></circle>
    <g clip-path="url(#a___-375265330)" fill="#fff">
        <path d="M71.33 45.29h23.65v3.22H71.33z"></path>
        <path d="M81.53 19.94h3.22v26.97h-3.22zM55.788 33.546l2.285-2.268 14.398 14.508-2.286 2.268zM57.8 58.93h3.22v37.59H57.8z"></path>
        <path d="M20.15 57.22H59.5v3.22H20.15z"></path>
        <path d="M20.99 46.159l2.288-2.265L60.741 81.75l-2.289 2.265zm37.58 12.022l11.453-12.235 2.35 2.2L60.92 60.382z"></path>
        <path d="M24.03 81.36h70.94v3.22H24.03z"></path>
        <path d="M81.53 82.97h3.22v16.94h-3.22zm-11.93 0h3.22v27.22H69.6z"></path>
    </g>
    <path d="M83.15 84.58H59.5a1.61 1.61 0 0 1-1.15-.48l-23.65-24a1.62 1.62 0 1 1 2.3-2.31l23.18 23.57h23a1.61 1.61 0 0 1 0 3.22z"></path>
</svg>
</div></div></div></div></div><div class="_style_3ZBSIh"><h4 class="_style_4okShP">Plan your trip</h4><div class="_style_31qDzC"><div class="cmln__markdown"><p class="cmln__paragraph">Get a ride that fits your schedule, not the other way around.</p>
<p class="cmln__paragraph"><a class="cmln__link" href="#">Multiple destinations</a></p>
<p class="cmln__paragraph"><a class="cmln__link" href="#">Schedule a ride</a></p>
<p class="cmln__paragraph"><a class="cmln__link" href="#">Calendar sync</a></p>
</div></div></div></div></div><div class="layout__item _style_1YSqxy"><div class="_style_VxPAE"><div class="_style_2ehyHb"><div class="_style_2XpHEy"><div class="_style_1iLMt8"><div aria-label="3c_Driver" class="_style_4EfqLw"><div class="isvg loaded" style="height: 100%;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88 88" height="88" width="88" style="display: block; height: 100%; width: 100%;"><title>Driver hands on steering wheel</title><desc>Driver hands on steering wheel</desc>
    <defs>
        <clipPath id="a___649188101">
            <path d="M25.76 24.25v2.65l-.86 1a.81.81 0 0 1-1.44-.51V16.72A1.73 1.73 0 0 0 21.74 15h-.35a1.72 1.72 0 0 0-1.64 1.22h-2.3a1.66 1.66 0 0 0-1.6 1.22h-2a2.3 2.3 0 0 0-2.3 2.3v-.07h-1.17a2.26 2.26 0 0 0-2.26 2.26v7.28a18.25 18.25 0 0 0 1.62 7.52L11 39.45a5.75 5.75 0 0 1 .34 3.78l-.67 2.38h12.49v-2.24a3.89 3.89 0 0 1 1.38-3l1.86-1.57a7 7 0 0 0 2.41-4.23l.78-4.77v-7.91a31.38 31.38 0 0 0-3.83 2.36z" fill="none"></path>
        </clipPath>
        <clipPath id="b___649188101">
            <path d="M62.24 24.25v2.65l.86 1a.81.81 0 0 0 1.44-.51V16.72A1.73 1.73 0 0 1 66.26 15h.35a1.72 1.72 0 0 1 1.64 1.22h2.3a1.66 1.66 0 0 1 1.6 1.22h2a2.3 2.3 0 0 1 2.3 2.3v-.07h1.19a2.26 2.26 0 0 1 2.26 2.26v7.28a18.25 18.25 0 0 1-1.62 7.52L77 39.45a5.75 5.75 0 0 0-.34 3.78l.67 2.38H64.84v-2.24a3.89 3.89 0 0 0-1.38-3l-1.86-1.54a7 7 0 0 1-2.41-4.23l-.78-4.77v-7.94a31.38 31.38 0 0 1 3.83 2.36z" fill="none"></path>
        </clipPath>
    </defs>
    <path d="M44 18.37a31.33 31.33 0 1 1-31.33 31.34A31.37 31.37 0 0 1 44 18.37m0-7a38.29 38.29 0 1 0 38.3 38.34A38.29 38.29 0 0 0 44 11.41z"></path>
    <path d="M37.51 80.47q-1-.21-2-.49a31.17 31.17 0 0 0 17 0q-1 .28-2 .49a2.36 2.36 0 0 1-2.85-2.3v-6.1a15.65 15.65 0 0 1 3.84-10.31c2.88-3.31 5-5.4 5-5.4h15.04a2.35 2.35 0 0 1 2.24 3.07 31.2 31.2 0 0 0 1.55-9.72c0-.91 0-1.81-.12-2.7a1.74 1.74 0 0 1-2.36 1.73l-8.93-3.46H24.09l-8.93 3.46A1.74 1.74 0 0 1 12.79 47c-.08.89-.12 1.79-.12 2.7a31.2 31.2 0 0 0 1.55 9.72 2.35 2.35 0 0 1 2.24-3.07h15.07s2.1 2.09 5 5.4a15.65 15.65 0 0 1 3.86 10.31v6.1a2.36 2.36 0 0 1-2.88 2.31z" style="fill: rgb(242, 102, 29)"></path>
    <path d="M28.3 43.46c1.19-.51 2.41-1 3.63-1.35a39.67 39.67 0 0 1 24.14 0c1.23.39 2.44.84 3.63 1.35l4.21 1.81-5.71 15.81a6.35 6.35 0 0 1-6 4.19H35.48a6.35 6.35 0 0 1-6-4.29l-5.38-15.7z" style="fill: rgb(242, 102, 29)"></path>
    <circle cx="44" cy="52.32" r="5.22" fill="#fff"></circle>
    <path d="M25.76 24.25v2.65l-.86 1a.81.81 0 0 1-1.44-.51V16.72A1.73 1.73 0 0 0 21.74 15h-.35a1.72 1.72 0 0 0-1.64 1.22h-2.3a1.66 1.66 0 0 0-1.6 1.22h-2a2.3 2.3 0 0 0-2.3 2.3v-.07h-1.17a2.26 2.26 0 0 0-2.26 2.26v7.28a18.25 18.25 0 0 0 1.62 7.52L11 39.45a5.75 5.75 0 0 1 .34 3.78l-.67 2.38h12.49v-2.24a3.89 3.89 0 0 1 1.38-3l1.86-1.57a7 7 0 0 0 2.41-4.23l.78-4.77v-7.91a31.38 31.38 0 0 0-3.83 2.36z" fill="#d6d6d5"></path>
    <path fill="#fff" d="M23.2 48.37H10.01l.62-2.77h12.53l.04 2.77z"></path>
    <path d="M24.74 88H0l10-39.63h13.19l.87 2.46a11.84 11.84 0 0 1 .68 4z" style="fill: rgb(242, 102, 29)"></path>
    <g clip-path="url(#a___649188101)" fill="#a5a5a5">
        <path d="M19.56 22.6a.5.5 0 0 1-.5-.5v-7.73a.5.5 0 1 1 1 0v7.73a.5.5 0 0 1-.5.5zm-3.88 0a.5.5 0 0 1-.5-.5v-7.73a.5.5 0 1 1 1 0v7.73a.5.5 0 0 1-.5.5zm-4.1 0a.5.5 0 0 1-.5-.5v-7.73a.5.5 0 0 1 1 0v7.73a.5.5 0 0 1-.5.5z"></path>
    </g>
    <path d="M62.24 24.25v2.65l.86 1a.81.81 0 0 0 1.44-.51V16.72A1.73 1.73 0 0 1 66.26 15h.35a1.72 1.72 0 0 1 1.64 1.22h2.3a1.66 1.66 0 0 1 1.6 1.22h2a2.3 2.3 0 0 1 2.3 2.3v-.07h1.19a2.26 2.26 0 0 1 2.26 2.26v7.28a18.25 18.25 0 0 1-1.62 7.52L77 39.45a5.75 5.75 0 0 0-.34 3.78l.67 2.38H64.84v-2.24a3.89 3.89 0 0 0-1.38-3l-1.86-1.54a7 7 0 0 1-2.41-4.23l-.78-4.77v-7.94a31.38 31.38 0 0 1 3.83 2.36z" fill="#d6d6d5"></path>
    <path fill="#fff" d="M64.8 48.37h13.19l-.62-2.77H64.84l-.04 2.77z"></path>
    <path d="M63.26 88H88L78 48.37H64.81l-.87 2.46a11.84 11.84 0 0 0-.68 4z" style="fill: rgb(242, 102, 29)"></path>
    <g clip-path="url(#b___649188101)" fill="#a5a5a5">
        <path d="M68.44 22.6a.5.5 0 0 1-.5-.5v-7.73a.5.5 0 1 1 1 0v7.73a.5.5 0 0 1-.5.5zm3.88 0a.5.5 0 0 1-.5-.5v-7.73a.5.5 0 1 1 1 0v7.73a.5.5 0 0 1-.5.5zm4.1 0a.5.5 0 0 1-.5-.5v-7.73a.5.5 0 1 1 1 0v7.73a.5.5 0 0 1-.5.5z"></path>
    </g>
</svg>
</div></div></div></div></div><div class="_style_3ZBSIh"><h4 class="_style_4okShP">Enjoy the ride</h4><div class="_style_31qDzC"><div class="cmln__markdown"><p class="cmln__paragraph">Make the most of your time in the backseat.</p>
<p class="cmln__paragraph"><a class="cmln__link" href="#">Share your status</a></p>
<p class="cmln__paragraph"><a class="cmln__link" href="#">Visa Local Offers</a></p>
<p class="cmln__paragraph"><a class="cmln__link" href="#">American Express</a></p>
</div></div></div></div></div><div class="layout__item _style_1YSqxy"><div class="_style_VxPAE"><div class="_style_2ehyHb"><div class="_style_2XpHEy"><div class="_style_1iLMt8"><div aria-label="3c_stars" class="_style_4EfqLw"><div class="isvg loaded" style="height: 100%;"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88 88" height="88" width="88" style="display: block; height: 100%; width: 100%;"><title>Decorative Illustration</title><desc>Decorative Illustration</desc>
    <defs>
        <clipPath id="a___1778161231">
            <circle cx="44" cy="44" r="44" fill="none"></circle>
        </clipPath>
        <pattern id="b___1778161231" data-name="2/2 - black" width="4" height="4" patternTransform="translate(8 17.1)" patternUnits="userSpaceOnUse" viewBox="0 0 4 4">
            <path fill="none" d="M0 0h4v4H0z"></path>
            <path d="M0 0h2v2H0z"></path>
        </pattern>
    </defs>
    <path d="M24.05 0h46.07a5.23 5.23 0 0 1 5.23 5.23v36.39l-3.81-3.88H24.05a5.23 5.23 0 0 1-5.23-5.23V5.23A5.23 5.23 0 0 1 24.05 0z" style="fill:rgb(241, 102, 34)"></path>
    <g clip-path="url(#a___1778161231)">
        <path d="M6.38 80h57.17l-3.37-31.48A8.42 8.42 0 0 0 51.81 41H11.16a8 8 0 0 0-8 8.9z" style="fill:#000;"></path>
        <path d="M32.8 43.08h17.27a3.79 3.79 0 0 1 3.79 3.79H29a3.79 3.79 0 0 1 3.8-3.79z" fill="#d6d6d6"></path>
        <path d="M62 46.87H22.46A12.51 12.51 0 0 0 10.05 61l2.35 30.67h63.92L73.45 57A11.58 11.58 0 0 0 62 46.87z" fill="#f16622"></path>
        <path fill="#777" d="M20.26 40.99h-6.7l50.67 50.67h6.7L20.26 40.99M46.59 6.65a12.07 12.07 0 0 1 12.06 12.06v13.68a2.41 2.41 0 0 1-2.41 2.41H34.52V18.71A12.07 12.07 0 0 1 46.59 6.65z"></path>
        <path fill="#777" d="M34.52 18.71h16.09v28.16H34.52z"></path>
        <path d="M50.61 46.87H34.52v2h.69a13.45 13.45 0 0 1 9.51 3.94l.15.15a3.36 3.36 0 0 0 5.74-2.38v-3.71z" fill="#fff"></path>
        <path d="M59.95 24a.4.4 0 0 1-.28.69h-1.75v-4h.73v.18a4.42 4.42 0 0 0 1.3 3.13z" fill="#777"></path>
        <path d="M50.94 11.11h3.71a1 1 0 0 0 .68-1.63l-.93-.93a7 7 0 0 0-5-2.06H43a8.44 8.44 0 0 0-8.44 8.44V33.6l6.16-6.16a6.44 6.44 0 0 0 1.88-4.55v-2.08a2.07 2.07 0 0 1 1.81-2.09 2 2 0 0 1 2.21 2v2h2v-2.28a3.22 3.22 0 0 1 3.22-3.22h.8v-.14a3.22 3.22 0 0 0-.94-2.28l-1.6-1.6a1.23 1.23 0 0 1 .84-2.09z"></path>
        <path fill="#d6d6d6" d="M32.1 81.47l18.287-18.286 8.513 8.514-18.285 18.286z"></path>
        <path d="M54.22 63.12l-1.95 1.95a3.18 3.18 0 0 0-.93 2.25 3.18 3.18 0 0 0 5.55 2.12l3.18-3.55a.16.16 0 0 0 0-.11v-2.54a.16.16 0 0 0-.07-.16h-5.67a.16.16 0 0 0-.11.04zm14.41-4.8h3.46v.39a1.82 1.82 0 0 1-1.82 1.82h-1.64v-2.21z" fill="#777"></path>
        <path d="M89.25 70.34a5.19 5.19 0 0 1-3.67-1.52 5.19 5.19 0 0 0-3.67-1.52H70.09a5.19 5.19 0 0 0-3.67 1.52 5.19 5.19 0 0 1-3.67 1.52h-5.24l.18 4.47h2.87a6.46 6.46 0 0 1 4.57 1.89L71 82.63a6.46 6.46 0 0 1 1.89 4.57v4h6.13v-4A6.46 6.46 0 0 1 81 82.63l5.93-5.93a6.46 6.46 0 0 1 4.57-1.89h2.87l.21-4.47h-5.33z"></path>
        <path d="M92.27 74.13A16.25 16.25 0 0 0 76 57.9v-3.7a19.93 19.93 0 1 1 0 39.85v-3.7a16.25 16.25 0 0 0 16.27-16.22zm-32.45 0A16.25 16.25 0 0 1 76 57.9v-3.7a19.93 19.93 0 0 0 0 39.85v-3.7a16.25 16.25 0 0 1-16.18-16.22z"></path>
        <path d="M61.94 55.41l-8.77 8.77 3.11 3.11h6.49a8.5 8.5 0 0 0 6-2.49L71 62.61a1 1 0 0 0 0-1.42l-5.78-5.78a2.3 2.3 0 0 0-3.28 0z" fill="#777"></path>
        <path d="M5.38 74.69h94.55v22.24H5.38z" style="fill: rgb(241, 102, 34)"></path>
        <path d="M45.33 79.14h29.18v18.65H35.29v-8.61a10 10 0 0 1 10.04-10.04z" fill="url(#b___1778161231)"></path>
    </g>
</svg>
</div></div></div></div></div><div class="_style_3ZBSIh"><h4 class="_style_4okShP">Rate your ride</h4><div class="_style_31qDzC"><div class="cmln__markdown"><p class="cmln__paragraph">Ratings help keep Loader safe, reliable, and enjoyable for everyone. You can even add a tip for your driver.</p>
<p class="cmln__paragraph"><a class="cmln__link" href="#">Driver ratings</a></p>
<p class="cmln__paragraph"><a class="cmln__link" href="#">Driver compliments</a></p>
<p class="cmln__paragraph"><a class="cmln__link" href="#">In-app tipping</a></p>
<p class="cmln__paragraph"><a class="cmln__link" href="#">Lost and found</a></p>
</div></div></div></div></div></div></div></div><div class="_style_34KYWo wcb-accordion"><div class="_style_4pIkRo"><div class="_style_eeI8o"><div class="_style_VxPAE"><p class="_style_bsxwn">FAQs</p><h2 class="_style_3zaJwR">Common questions about Loader</h2></div>
 <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">When and where is Loader available in my city?</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse">
        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">How do I create an Loader account?</a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">How are fares calculated?</a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
      </div>
    </div>
     <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">More FAQs</a>
        </h4>
      </div>
      <div id="collapse4" class="panel-collapse collapse">
        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
      </div>
    </div>
  </div> 
</div></div></div><div class="_style_3ye16P wcb-messaging-2up"><div class="_style_eeI8o"><div class="layout"><div class="layout__item _style_4vDxhh"><div class="_style_4clpDB"><div class="_style_9Ls0z"><div class="_style_2lqZEN"><p class="_style_bsxwn"> </p><h2 class="_style_3zaJwR">Create your account</h2></div><div class="cmln__markdown"><p class="cmln__paragraph">Download the app so you’re ready the next time you need a ride.</p>
</div><a class="wcb-button _style_46i2TE _style_1m6gck" href="#"><!-- react-text: 8309 -->Sign up to ride<!-- /react-text --><span class="_style_28zWyA"><svg viewBox="0 0 64 64" width="20px" height="20px" class=" _style_4wJp4e"><path fill-rule="evenodd" clip-rule="evenodd" d="M59.927 31.985l.073.076-16.233 17.072-3.247-3.593L51.324 34H4v-4h47.394L40.52 18.407l3.247-3.494L60 31.946l-.073.039z"></path></svg></span></a></div></div></div><div class="layout__item _style_4vDxhh"><div><div class="_style_9Ls0z"><div class="_style_2lqZEN"><p class="_style_bsxwn"> </p><h2 class="_style_3zaJwR">Share with friends </h2></div><div class="cmln__markdown"><p class="cmln__paragraph">Refer your friends to Loader and they’ll get $15 off their first ride.</p>
</div><a class="wcb-button _style_1JtngI" href="#"><!-- react-text: 8321 -->INVITE FRIENDS<!-- /react-text --><span class="_style_1gVw1O"><svg viewBox="0 0 64 64" width="16px" height="16px" class=" _style_4wJp4e"><path d="M23.161 53.213l-4.242-4.242a1 1 0 0 1 0-1.415L34.475 32 18.919 16.444a1 1 0 0 1 0-1.415l4.242-4.242a1 1 0 0 1 1.414 0L40.84 27.05l4.242 4.243a1 1 0 0 1 0 1.414L40.84 36.95 24.575 53.213a1 1 0 0 1-1.414 0z"></path></svg></span></a></div></div></div></div></div></div></div></div></div></div></div></div>
@endsection